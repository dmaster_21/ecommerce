<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compras_cart extends Model
{
    //
    const PROCESADO = 1;//COMPRA GENERADA 
    const REPORTADO = 2;//PAGO REPORTADO
    const PAGADO = 3;//PAGO REPORTADO
    const ANULADO = 9;//COMPRA ANULADA 
    

      protected $fillable = [
      	'nro_orden',
      	'annio_fiscal',
      	'nro_factura',
      	'nro_control',
      	'fecha_compra',
      	'monto_compra',
      	'user_id',
      	'status',
      	'fecha_pago',
      ];


      public function detalle(){
        return $this->hasMany('App\Compras_cart_detalle','compras_carts_id','id');  
      }
      public function banco(){
       //return $this->hasOne('App\Banco','id','banco_id');
        return $this->belongsto('App\Banco','banco_id');
      }

      public function pago(){

       // return $this->hasOne('App\Tipo_pago','id','tipo_id');
        return $this->belongsto('App\Tipo_pago','tipo_id');
      }

        public function cliente(){

       return $this->hasOne('App\User','id','user_id');
      }
       public function agency(){

       return $this->belongsto('App\Envio','envios_id');
      }

       public function user(){

       return $this->hasOne('App\User','id','user_id_despacho');
      }

     
}
