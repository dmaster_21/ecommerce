<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Banco;
use App\Compras_cart;
use App\Log;
class BancosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        $bco=Banco::all();
        return view('banco.index',compact('bco'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(['nombre' => 'required|min:3|max:200']);

        $bco=new Banco();
        $bco->name=$request->nombre;
        $bco->save();

        Session::flash('message', 'El Banco '.$request->nombre.' Se Ha Creado De Manera Satisfactoria');
        Session::flash('class', 'success');
        //return back();
         Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Creación del Banco'.$request->nombre,
        'accion1'=>'Crear'
         ]);

        return redirect()->intended(url('/banco'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(['nombre' => 'required|min:3|max:200']);
        $bco=Banco::find($id);
        $bco->name=$request->nombre;
        $bco->save();

        Session::flash('message', 'El Banco '.$request->nombre.' Se Ha Modificado De Manera Satisfactoria');
         Session::flash('class', 'success');

          Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Modificación  del Banco'.$request->nombre,
        'accion1'=>'Editar'
         ]);

        //return back();
        return redirect()->intended(url('/banco'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $bco=Banco::find($id);

         $pags=Compras_cart::where('banco_id',$id)->count();

         if($pags>0){

            Session::flash('message', 'El Banco' .$bco->name. 'Se Encuentra Asociado a Pagos de clientes No Puede ser Eliminado');
            Session::flash('class', 'danger');

           

         }else{
             $bco->delete();
             Session::flash('message', 'El Banco' .$bco->name. ' Ha Sido Eliminado');
             Session::flash('class', 'success');
             $bco->delete();
                Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Eliminación  del Banco'.$bco->name,
        'accion1'=>'Eliminar'
         ]);
         }
          return redirect()->intended(url('/banco'));
    }

    public function status($id,$st){

         $bco=Banco::find($id);
         $bco->status=$st;
         $bco->save();

         if($st==9){
            $mes='Inactivado';
         }else{
            $mes='Activado';
         }

          Session::flash('message', 'El Banco '.$bco->name.' Se Ha '.$mes.' De Manera Satisfactoria');
           Session::flash('class', 'success');

              Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'El Usuario ha'. $mes . 'El Banco '.$bco->name,
        'accion1'=>'Editar'
         ]);
        //return back();
        return redirect()->intended(url('/banco'));
    }
}
