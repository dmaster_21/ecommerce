<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;



use App\Product;
use App\Compras_Cart;
use App\Compras_Cart_detalle;
use App\Iva;
use App\Envio;
use App\Log;
use App\day;
class CartController extends Controller
{
   
public function __construct(){

	//verifico si no existe la variable de session y si no existe la creo

	if(!\Session::has('cart')){
		\Session::put('cart',array());
	} 

}
//show cart
public function show(){

	$cart=\Session::get('cart');//identifico mi array
	$total=$this->total();
	$iva=Iva::find(1);
	//return $cart;
	return view('cart.cart')->with(['cart'=>$cart,'total'=>$total,'iva'=>$iva]);
}

//add items
public function add(Product $product){

	$cart=\Session::get('cart');//declaro session local
	$product->qty=1;

	$cart[$product->id]=$product;
	\Session::put('cart',$cart);//actualizo session global

	return redirect()->route('cart.show');


}


public function delete(Product $product){

	$cart=\Session::get('cart');//declaro session local
	unset($cart[$product->id]);///inteccion de dependencias
	\Session::put('cart',$cart);//cambio sesion global

	return redirect()->route('cart.show');


}

//delete item

//update item
public function update(Product $product,$qty){
    $cart=\Session::get('cart');//declaro session local
	$cart[$product->id]->qty=$qty;
	\Session::put('cart',$cart);//cambio sesion global
	return redirect()->route('cart.show');


}

//trash cart
public function trash(){
	\Session::forget('cart');
    return redirect()->route('cart.show');

	}

//Total

	private function total(){

			$cart=\Session::get('cart');//declaro sesseion local
			$total=0;
			foreach ($cart as $item) {
				$total+= $item->price*$item->qty;
			}

			return $total;

	}
//detalle del pedido antes de procesar 
	public function Detail(){
		if(count(\Session::get('cart'))<=0){
			return redirect()->route('/');
		}else{
    $iva=Iva::find(1);
    $envio=Envio::where('status',1)->get();
    $cart=\Session::get('cart');//identifico mi array
	$total=$this->total();
	return view('cart.detail')->with(['cart'=>$cart,'total'=>$total,'iva'=>$iva,'envio'=>$envio]);




		}


	}

	public function proccess(Request $request){


	 $request->validate(['agente_envio' => 'required']);
	 
	 $now = new \DateTime();
      $compra= new Compras_Cart();
      $iva=Iva::find(1);
      $env=Envio::find($request->agente_envio);
      $orden=Compras_Cart::orderBy('nro_orden','desc')->first();
      
      
       $compra->nro_orden=$orden->nro_orden+1;
       $compra->annio_fiscal= $now->format('Y');
       $compra->nro_factura=$orden->nro_factura+1;
       $compra->nro_control=$orden->nro_control+1;
       $compra->fecha_compra= $now->format('Y-m-d H:i:s');
       $compra->monto_compra=$this->total();
       $compra->iva=$iva->porc;//valor de iva
       $compra->monto_envio=$env->price;//costo de envio
       $compra->envios_id=$env->id;
       $compra->user_id=auth()->id();
       $compra->cajero="CLIENTE";
       $compra->status='1';
       $compra->plataforma="PLATAFORMA EN LINEA";
       if($compra->save()){
            
            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: '.$compra->nro_orden,
        'accion1'=>'Crear'
         ]);

       $cart=\Session::get('cart');//declaro sesseion local
		
			foreach ($cart as $item) {

		    $prod=Product::find($item->id);

		    $prod->qty=$prod->qty-$item->id;
            $Cdetalle=new Compras_Cart_detalle;
            $Cdetalle->compras_carts_id=$compra->id;
            $Cdetalle->products_id=$item->id;
            $Cdetalle->qty=$item->qty;
            $Cdetalle->price=$item->price;
            $Cdetalle->iva=$iva->porc;
            $Cdetalle->save();
            $prod->save();

         }

         $total=$this->total();
           $day=day::find(1);
          Session::flash('message','Estimado Usuario Su Compra Ha Sido Completada Bajo el Número de Orden ->'.$compra->nro_orden. 'Su compra Sera Valida '.$day->nroday.' dias Posterior a su compra, Si no Reporta el Pago será Anulada');
          Session::flash('class','success');
          \Session::forget('cart');
         return view('cart.success')->with(['cart'=>$cart,'compra'=>$compra,'total'=>$total]);

        



     }else{
     	Session::flash('message','Estimado Usuario Su Compra No Ha Podido Ser Completada Por Favor Espere...');
          Session::flash('class','danger');
     	return view('cart.detail');


     }






      



			
	}

}
