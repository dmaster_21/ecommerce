<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Log;


class CategorysController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Cat=DB::table('categories')->paginate(10);        //
        return view('categorias.index')->with('Category',$Cat);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
     /*   $validation = Validator::make($request->all(), [
        'name' => 'required|min:3|max:200',
   ]);*/

        $request->validate(['Categoria' => 'required|min:3|max:200']);

        $Cat=new Category();

        $Cat->name=$request->Categoria;       
        $Cat->user_id=auth()->id();         
        $Cat->save();

        Session::flash('message', 'La Categoria '.$request->Categoria.' Se Ha Creado De Manera Satisfactoria');
        //return back();
        

         Log::create([
       
        'user_id' => auth()->id(),
        'accion' => 'Creación de  Una Nueva Categoria Llamada '.$request->Categoria,
        'accion1'=>'Crear'
    ]);
         return redirect()->intended(url('/categorias'));

         //$Log=new Log();
         //$Log->user_id=auth()->id();

       
  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)

    {
        dd('dd');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $Cat=Category::find($id);      
       if($Cat->status===9){
        $sts='1';
        $menssage="La Categoria ".$Cat->name." Ha Sido Habilitada Satisfactoriamente";
       }else{
        $sts='9';
        $menssage="La Categoria ".$Cat->name." Ha Sido Inhabilitada Satisfactoriamente";
       } 
       $Cat->status=$sts;
       $Cat->save();
       Session::flash('message', $menssage);


         Log::create([       
        'user_id' => auth()->id(),
        'accion' => $menssage,
        'accion1'=>'Editar'
         ]);
      
      return redirect()->intended(url('/categorias'));
      
      
    }

      
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $request->validate(['Categoria' => 'required|min:3|max:200']);
        $cat=Category::find($id);
        $cat->name=$request->Categoria;
        $cat->save();

         Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Modificación de la Categoria '.$request->Categoria,
        'accion1'=>'Editar'
         ]);

         Session::flash('message', 'La Categoria Ha Sido Actualizada Satisfactoriamente');
         return redirect()->intended(url('/categorias'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     
        $Cat=Category::find($id); 
        $pr=Product::where('category_id',$id)->count();

        if($pr==0){
            $Cat->delete();
            Session::flash('message', 'La Categoria Ha Sido Eliminada Satisfactoriamente');
            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Eliminación de Categoria '.$Cat->name,
        'accion1'=>'Eliminar'
         ]);

        }else{

            Session::flash('message', 'Disculpe Las Categoria Tiene Productos Asociados Por Lo Tanto no Se Puede Eliminar,Opte Puede Inactivar');
        }
       
       
         return redirect()->intended(url('/categorias'));
       
    }


  


}
