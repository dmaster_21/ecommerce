<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use Barryvdh\DomPDF\Facade as PDF;
use App\Compras_Cart;
use App\Compras_Cart_detalle;
use App\Banco;
use App\Tipo_pago;
use App\Iva;
use App\User;
use App\Envio;
use App\Product;
use App\Log;
use App\Day;

class ComprascartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
           $ver=Compras_cart::find(12); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


   
    public function create()
    {
        //
        dd();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $request->validate([
            'forma_pago' => 'required',
            'banco' => 'required',         
            'comprobante' => 'required|min:4|max:6|regex:/^[0-9 ,.\'-]+$/i',
            'monto'=>'required|regex:/^[0-9 ,.\'-]+$/i',
           
        ]);

       $comp=Compras_Cart::find($id);
        $comp->status='2';
        $comp->fecha_pago=now();
        $comp->tipo_id=$request->forma_pago;
        $comp->banco_id=$request->banco;
        $comp->ref_pago=$request->comprobante;
        
        if($comp->save()){
        Session::flash('mensaje', 'Pago Reportado Exitosamente, Estaremos Validando su Información y Contactaremos Lo más Breve Posible!');
        Session::flash('class', 'success');

            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:'.$comp->ref_pago,
        'accion1'=>'Editar'
         ]);

        }else{

         Session::flash('mensaje', 'Disculpe ha Ocurrido un Error!');
        Session::flash('class', 'danger');
        }

         return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       $comp=Compras_Cart::find($id);
       $det=Compras_Cart_detalle::where('compras_cart_id',$id);
       $comp->status='9';
       $comp->save();

       Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'La Compra Número:'. $comp->nro_orden. 'Ha Fue Eliminada',
        'accion1'=>'Eliminar'
         ]);

      foreach($det as $detail){

      $prod=Product::find($detail->products_id);
      $sum=$prod->qty+$detail->qty;
      $prod->qty=$sum;
      $prod->save();
     // $detail->delete();

      }

     
      
        Session::flash('mensaje', 'Estimado Usuario Tu Compra Fue Anulada de Manera Satisfactoria!');
        Session::flash('class', 'success');
           return back();

    }

     public function cuenta(){

        return view('cart.cuenta');        

    }

    public function cliente(){

        $comp=Compras_Cart::where('user_id',auth()->id())->get();
        $banco=Banco::where('status',1)->get();
        $tipo=Tipo_pago::where('status',1)->get();
        $iva=Iva::find(1);

       return view('cart.compras')->with(['compra'=>$comp,'banco'=>$banco,'tipo'=>$tipo,'iva'=>$iva]);
     



    }


    public function verificar(Request $request){
       
        $request->validate([
            'nombre' => 'required',      
           
        ]);

        $user=User::where('name','LIKE','%'.$request->nombre.'%')->get();
        $iva=Iva::find(1);
         $day=day::find(1);

        return view('ventas.verficar',compact('user','iva','day'));

//return redirect()->intended(url('/gestion_espacios/create'))->withInput();
    }

     public function confirmar(Request  $request,$id){


       $request->validate([
                      'nro'=>'required|regex:/^[0-9 ,.\'-]+$/i'
           
        ]);

      $comp=Compras_Cart::find($id);
        $comp->status='3';
        $comp->ref_pago=$request->nro;
        $comp->save();

        Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Confirmación de Pago de La Compra Número:'. $comp->nro_orden,
        'accion1'=>'Editar'
         ]);

          Session::flash('mensaje', 'Su Confirmación de Pago de Ejecuto de Manera Satisfactoria!');
          Session::flash('class', 'success');

        $user=User::where('name','LIKE','%'.$comp->cliente->name.'%')->get();
        $iva=Iva::find(1);
        
         return back();

    }


   public  function ingreso(Request $request){
    //buscamos rl clirnyr

    $request->validate([
            'cedula' => 'required',
            'name' => 'required|string|max:255',
            //'email' => 'string|email|max:255|unique:users',              
            'telefono' => 'required',                    
            'direccion' => 'required|string',
             'pago'  => 'required',
             'banco' => 'required',
             'nro_pago'=>'required',
             'agente_envio'=>'required',
             'nac'=>'required',
        ]);



    if(empty($request->Prod)){

      Session::flash('message','Por favor Cargar Al menos un Items dentro de la Compra');
      Session::flash('class','danger');
    
     return redirect()->intended(url()->previous())->withInput();
    }


     $usr=User::where('ced_user','=',$request->cedula)->where('nac_usr',$request->nac)->first();

     if($usr){
  $usr->ced_user=$request->cedula;
  $usr->nac_usr=$request->nac;
  $usr->name=$request->name;
  $usr->telefono=$request->telefono;
  $usr->direccion=$request->direccion;
  $usr->email=$request->email;
  $usr->save();

     }else{

  $usr=new User();
  $usr->name=$request->name;
  $usr->nac_usr=$request->nac;
  $usr->ced_user=$request->cedula;
  $usr->telefono=$request->telefono;
  $usr->direccion=$request->direccion;
  $usr->email=$request->email;
  $usr->password=Hash::make($request->cedula);
  $usr->admin='1';
  $usr->save();


   Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Creción de un Nuevo Cliente de Nombre:'. $request->name,
        'accion1'=>'Crear'
         ]);

     }
   
      $compra= new Compras_Cart();
      $iva=Iva::find(1);      
      $orden=Compras_Cart::orderBy('nro_orden','desc')->first();
      $envio=Envio::find($request->agente_envio);

      
       $compra->nro_orden=$orden->nro_orden+1;
       $compra->annio_fiscal= date('Y');
       $compra->nro_factura=$orden->nro_factura+1;
       $compra->nro_control=$orden->nro_control+1;
       $compra->fecha_compra= now();
       $compra->monto_compra=$request->monto_compra;
       $compra->iva=$iva->porc;//valor de iva
       $compra->monto_envio=$envio->price;//costo de envio
       $compra->envios_id=$envio->id;
       $compra->fecha_pago= now();
       $compra->tipo_id=$request->pago;
       $compra->ref_pago=$request->nro_pago;
       $compra->banco_id=$request->banco;
       $compra->user_id=$usr->id;
       $compra->cajero=$request->cajero;
       $compra->status='3';
       $compra->plataforma="TIENDA FISICA";

      

       if($compra->save()){

          Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Procesamiento De Compra en Tienda Fisíca, Bajo El Número'. $compra->nro_orden,
       'accion1'=>'Crear'
         ]);

         // count($request->Prod)
   

        for($i=0;$i<count($request->Prod);$i++){
         

            $Cdetalle=new Compras_Cart_detalle();
           $prod=Product::find($request->Prod['id'][$i]);

            $prod->qty=$prod->qty-$request->Prod['qty'][$i]; 
            
            $Cdetalle->compras_carts_id=$compra->id;
            $Cdetalle->products_id=$request->Prod['id'][$i];
            $Cdetalle->qty=$request->Prod['qty'][$i];
            $Cdetalle->price=$prod->price;
            $Cdetalle->iva=$iva->porc;
             $Cdetalle->save();
            $prod->save();

        }

         Session::flash('message','La Compra Fue Ejecutada Bajo la  Orden ->'.$compra->nro_orden );
          Session::flash('class','success');

          return back();
     }else{

        Session::flash('message','Estimado Usuario Su Compra No se Proceso Intente Nuevamente');
          Session::flash('class','danger');
      
       return redirect()->intended(url()->previous())->withInput();

     }
}
     
     
     public function compraMes(Request $request){



     $request->validate([
            'desde' => 'required',
            'hasta' => 'required',
        

        ]);

     
       $f1=$request->desde;//date("Y/d/m", strtotime($request->desde));
          $f2=$request->hasta;//date("Y/d/m", strtotime($request->hasta));
    
          
            $ventas=Compras_cart::where('fecha_compra','>=',$request->desde)->where('fecha_compra','<=',$request->hasta)->get();
            $iva=Iva::find(1);

            
  
           return view('ventas.mensual',compact('ventas','iva','f1','f2'));


     
    }

    public function TopMes(Request $request){

       $request->validate([
            'desde' => 'required',
            'hasta' => 'required',
        

        ]);
     
          $f1=$request->desde;
          $f2=$request->hasta;
      
      

      $top = DB::table('compras_cart_detalles')
    ->join('products', 'products.id', '=', 'compras_cart_detalles.products_id')
    ->select('products.name','products.qty',DB::raw('sum(compras_cart_detalles.qty ) AS total_qty'))
    ->orderBy(DB::raw('sum(compras_cart_detalles.qty )'),'DESC')
    ->groupBy('products.id')

    ->whereBetween('compras_cart_detalles.created_at', [$request->desde, $request->hasta])  
    ->take(20)                 
    ->get();

   return view('ventas.toVendido',compact('top','f1','f2'));
    }



    public function pdfVentaC(){

      $ventas=Compras_cart::where('status','1')->get();
      $pdf = PDF::loadView('reports.ventasVer', compact('ventas'));
        return $pdf->stream('listado');
    }



   

    public function pdfTopMes(Request $request){

          $f1=date("Y/d/m h:m:s", strtotime($request->desde));
          $f2=date("Y/d/m h:m:s", strtotime($request->hasta));

      $top = DB::table('compras_cart_detalles')
    ->join('products', 'products.id', '=', 'compras_cart_detalles.products_id')
    ->select('products.name','products.qty',DB::raw('sum(compras_cart_detalles.qty ) AS total_qty'))
    ->orderBy(DB::raw('sum(compras_cart_detalles.qty )'),'DESC')
    ->groupBy('products.id')

    ->whereBetween('compras_cart_detalles.created_at', [$request->desde, $request->hasta])  
    ->take(20)                 
    ->get();

     $pdf = PDF::loadView('reports.ventasTop', compact('top'));
       return $pdf->stream('topVendido');

  
    }


    public function pdfcompraMes(Request $request){

          $f1=date("Y/d/m h:m:s", strtotime($request->desde));
          $f2=date("Y/d/m h:m:s", strtotime($request->hasta));
      

            $ventas=Compras_cart::where('status','=','3')->whereBetween('fecha_compra', [$request->desde,$request->hasta])->get();
            $iva=Iva::find(1);
  
          /// return view('ventas.mensual',compact('ventas','iva'));
             $pdf = PDF::loadView('reports.ventasMes', compact('ventas','iva'));
       return $pdf->stream('ventasMes');
     
    }



     public function voucher($id){
     $comp=Compras_cart::find($id);

      $pdf = PDF::loadView('reports.voucher', compact('comp'));
        return $pdf->stream('voucher');
     
    }

     public function listadespacho(){
       $ventas=Compras_cart::where('cond_despacho',1)->where('status',3)->get();

      $pdf = PDF::loadView('reports.listadespacho', compact('ventas'));
        return $pdf->stream('listadespacho');
     
    }

    public function historicodespacho(Request $request){
      $ventas=Compras_cart::whereBetween('fecha_despacho', array($request->desde1,$request->hasta1))->where('envios_id',$request->envio1)->where('cond_despacho','3')->get();

       $pdf = PDF::loadView('reports.historico', compact('ventas'));
        return $pdf->stream('historico');
      
        
    }

    public function verdespacho(){

      $ventas=Compras_cart::where('cond_despacho',1)->where('status',3)->get();
      $iva=Iva::find(1);

      return view('despacho.index',compact('ventas','iva'));


    }


    public function procesardespacho(Request $request){

       $request->validate([
            'fecha' => 'required',
            'nota' => 'required',
        

        ]);
      $compra=Compras_cart::find($request->id);
      $compra->fecha_despacho=$request->fecha;
      $compra->nota_despacho=$request->nota;
      $compra->cond_despacho='3';
      $compra->user_id_despacho=auth()->id();

      $compra->save();

        Session::flash('message','Despacho de Mercancía de manera Satisfactoria');
          Session::flash('class','success');

          return back();
    }

    public function buscadespacho(){
       $envio=Envio::where('status',1)->get();
      return view('despacho.buscar',compact('envio'));
    }


    public function procesabusqueda(Request $request ){
      $request->validate([
            'desde' => 'required',
            'hasta' => 'required',
            'envio' => 'required',      

        ]);
       $ventas=Compras_cart::whereBetween('fecha_despacho', array($request->desde,$request->hasta))->where('envios_id',$request->envio)->where('cond_despacho','3')->get();
        $iva=Iva::find(1);
         $envio=Envio::where('status',1)->get();
         $desde=$request->desde;
         $hasta=$request->hasta;
         $enselect=$request->envio;

      return view('despacho.historico',compact('ventas','iva','envio','desde','hasta','enselect'));

    }

public function graficos(){
  $concr=Compras_cart::where('status','3')->count();
  $xconcr=Compras_cart::where('status','1')->count();
  $vanul=Compras_cart::where('status','9')->count();
  
  $total=Compras_cart::all()->count();
  $tfisica=Compras_cart::where('plataforma','TIENDA FISICA')->count();
   $tlinea=Compras_cart::where('plataforma','PLATAFORMA EN LINEA')->count();

    $top = DB::table('compras_cart_detalles')
    ->join('products', 'products.id', '=', 'compras_cart_detalles.products_id')
    ->select('products.name','products.qty',DB::raw('sum(compras_cart_detalles.qty ) AS total_qty'))
   
    ->groupBy('products.id')
    ->where('compras_cart_detalles.created_at','>=','2019-01-01')  
    ->take(10)                 
    ->get();
   $meses = array();
    for($i=1;$i<=12;$i++){

       switch ($i) {
          case 1:
          $mes = 'Enero';
          break;
          case 2:
          $mes = 'Febrero';
          break;
          case 3:
          $mes= 'Marzo';
          break;
          case 4:
          $mes = 'Abril';
          break;

          case 5:
          $mes = 'Mayo';
          break;
          case 6:
          $mes = 'Junio';
          break;

          case 7:
          $mes = 'Julio';
          break;

          case 8:
          $mes = 'Agosto';
          break;

          case 9:
          $mes = 'Septiembre';
          break;

          case 10:
          $mes = 'Octubre';
          break;

          case 11:
          $mes = 'Noviembre';
          break;

          case 12:
          $mes = 'Diciembre';
          break;

        }

    $busca=Compras_cart::where('status','3')->whereMonth('created_at', '=',$i)->whereYear('created_at', '=', date('Y'))->count();
   array_push($meses,['mes'=>$mes,'qty'=>$busca]);
 }




  return view('graficos.index',compact('concr','xconcr','vanul','total','tfisica','tlinea','top','meses'));
}




}
