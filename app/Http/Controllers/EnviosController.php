<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;

use App\Envio;
use App\Compras_cart;
use App\Log;
class EnviosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $envio=Envio::all();
    return view('agencia.index',compact('envio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|min:3|max:200',
            'costo' => 'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);

        $envio=new Envio();
        $envio->name=$request->nombre;
        $envio->price=$request->costo;
        $envio->status='1';
        if($envio->save()){

        Session::flash('message', 'El Agende de envios  '.$request->nombre.' Se Ha Creado De Manera Satisfactoria');
        Session::flash('class', 'success');
        //return back();
            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Creacioón de Agencia de Envio Denóminada '.$request->nombre,
        'accion1'=>'Crear'
         ]);
    }else{
       Session::flash('message', 'El Agende de envios No pudo ser Agregado Intente Mas Tarde');
        Session::flash('class', 'danger');
    }
     return redirect()->intended(url('/envio'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

     $env=Envio::find($id);
     return response($env);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => 'required|min:3|max:200',
            'costo' => 'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);

        $envio=Envio::find($id);
        $envio->name=$request->nombre;
        $envio->price=$request->costo;
      ;
        if($envio->save()){

        Session::flash('message', 'El Agende de envios  '.$request->nombre.' Se Ha sido Modificado De Manera Satisfactoria');
        Session::flash('class', 'success');
        //return back();
           Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Modificación de Agencia de Envio '.$request->nombre,
         ]);
    }else{
       Session::flash('message', 'El Agende de envios No pudo ser Modificado Intente Mas Tarde');
        Session::flash('class', 'danger');
    }
     return redirect()->intended(url('/envio'));
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $envio=Envio::find($id);

         $compra=Compras_cart::where('envios_id',$id)->count();

         if($compra>0){

            Session::flash('message', 'El Agente' .$envio->name. 'Se Encuentra Asociado a Varias Compras  No Puede ser Eliminado Le Recordamos Inactivarlo');
            Session::flash('class', 'danger');

         }else{
             $envio->delete();
             Session::flash('message', 'El Agente' .$envio->name. ' Ha Sido Eliminado');
             Session::flash('class', 'success');
             $envio->delete();
             
                Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Eliminación  de Agencia de Envio'.$envio->name,
         ]);
         }
          return redirect()->intended(url('/envio'));
    }



    public function status($id,$st){

         $envio=Envio::find($id);
         $envio->status=$st;
         $envio->save();

         if($st==9){
            $mes='Inactivado';
         }else{
            $mes='Activado';
         }

          Session::flash('message', 'El Agente '.$envio->name.' Se Ha '.$mes.' De Manera Satisfactoria');
           Session::flash('class', 'success');
        //return back();
        return redirect()->intended(url('/envio'));
    }
}
