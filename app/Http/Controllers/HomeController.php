<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Compras_cart;
use App\Compras_cart_detalle;
use App\Product;
use App\Iva;
use App\Envio;
use App\day;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usr=User::find(auth()->id());
        /*PROCESO LOCAL para anular la gestion */
        $fecha_actual = date("Y-m-d");
        $day=day::find(1);
        $fechaA=date("Y-m-d",strtotime($fecha_actual."-".$day->nroday." days")); 
        
        $compra=Compras_cart::where('fecha_compra','<=',$fechaA)->where('status','1')->get();
        foreach($compra as $compras){
            $comp=Compras_cart::find($compras->id);
            $comp->status='9';
            $comp->save();

            $cdet=Compras_cart_detalle::where('compras_carts_id',$compras->id)->get();

            foreach($cdet as $cdets){

            $pr=Product::find($cdets->id);
            //dd($pr);
            /*devolvemos catidades*/
           //$sum=$pr->qty+$cdets->qty;           
           // $pr->qty=$sum;
           // $pr->save();
            }
        }
      
         if($usr->admin==1){
           return redirect()->intended('/');
            
         }else{

            $ventas_sn=Compras_cart::wherein('status',['1','2'])->count();
            $user=User::where('admin',1)->count();
            $ventas_vr=Compras_cart::where('status',3)->count();
            $prod=Product::where('status',1)->count();
            $ventas=Compras_cart::wherein('status',['1','2'])->get();
            $iva=Iva::find(1);
            $top=Product::where('qty','<=','10')->where('status','1')->count();
            $day=day::find(1);
         
           return view('home',compact('ventas_sn','user','ventas_vr','prod','ventas','iva','top','day'));
            
         }
    }
}
