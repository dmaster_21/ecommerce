<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;
use File;

use App\Category;
use App\Product;
use App\products_image;
use App\Log;
class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/*
     public function __construct()
    {
        $this->middleware('auth');
    }
*/
   

    public function index()

    {
        $barrido=Product::where('qty','<=',0)->get();
        foreach($barrido as $value){
        $cambio=Product::find($value->id);
        $cambio->qty=0;
        $cambio->save();

        }
        $Pdto=Product::where('status',1)->get();
       // return $pdto;
      /*$Pdto=DB::table('products')
       ->join('categories','categories.id','=','products.category_id')
       ->select('products.id','products.name','products.price','products.qty','products.destacado','products.status','products.image','categories.name as nameCategory')
       ->where('products.status',1)
       ->get();
      /**/
     // return $Pdto->category->name ;
      return view('productos.index')->with('Productos',$Pdto);
     


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     $Cat= DB::table('categories')->where('status','=',1)->get();
       return view('productos.create')->with('Category',$Cat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $producto=new Product();

         $request->validate([
            'categoria' => 'required',
            'nombre' => 'required',
            'descripcion' => 'required',
            'cantidad' => 'required|regex:/^[0-9 ,.\'-]+$/i',
            'precio' => 'required|regex:/^[0-9 ,.\'-]+$/i',
            'stock_min'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'stock_max'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);
         ////imagen destacada
        if ($request->hasFile('imagen')) {
        $file = $request->file('imagen');
    if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
        $path = public_path().'/images/dest';        
        $fileName =uniqid().'-'.$file->getClientOriginalName();
        $file->move($path, $fileName);

    }
        else{
             Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
             return back(); 
        }
       }else{

            Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
             return back();           
    }

    //valido existencia de galeria
         $files = $request->file('file');

          /* if(count($files)<=0){
            Session::flash('message', 'El Producto '.$request->nombre.' Debe Contener Al Menos Una Imgen');
         Session::flash('class', 'danger');

         return back(); 
     }*/


         $producto->category_id=$request->categoria; 
         $producto->name=$request->nombre; 
         $producto->description=$request->descripcion; 
         $producto->qty=$request->cantidad; 
         $producto->price=$request->precio; 
         $producto->image=$fileName; 
         $producto->stock_min=$request->stock_min; 
         $producto->stock_max=$request->stock_max; 
         $producto->user_id=auth()->id(); 

         
        
        $producto->save();
        
           Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Creación de Articulo|Producto Denominado:'. $request->nombre,
         ]);
         //gusrdo imagenes 
        
       
        $path = public_path().'/images/productos';      
       $active=1;//para que la primera imegen sea destacada del producto
         // recorremos cada archivo y lo subimos individualmente
         foreach ($files as $file) {         
         
        if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {

         $file->move($path, $fileName);

         $image=new products_image();
         $image->producto_id=$producto->id;//relaciono modelos 
         $image->image=$fileName;
         $image->active=$active;
         $image->save();

         $active=0;
     }else{

           Session::flash('message', 'Alguns archivos no fueron Cargados Por favor Revisar en Galeria');
         Session::flash('class', 'success');
          return redirect()->intended(url('/productos/'.$producto->id.'/edit'))->withInput();

     }
     }
     //mensaje de guardado
          Session::flash('message', 'El Producto '.$request->nombre.' Ha sido Listado de manera Satisfactoria');
         Session::flash('class', 'success');


        return back();
        


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\H ttp\Response
     */
    public function show($id)
    {
          $Pdto=Product::find($id);
          $img=products_image::where('producto_id',$id)->get();
       //return $Pdto;
    if($Pdto){
        
      return view('productos.show')->with(['img' => $img,'Prod' => $Pdto ]);
       }else{
        return redirect()->intended(url('/productos'));
   } 
//
    }
/*
    public function detalle($id){

        dd($id);
    }
*/
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {  
       $prod=Product::find($id);
       $gal=products_image::where('producto_id',$id)->get();
       $Cat= DB::table('categories')->where('status','=',1)->get();
       return view('productos.edit',compact('prod','gal','Cat'));
    }


     public function detalle($id) {
       
      $Pdto=Product::find($id);
      $img=products_image::where('producto_id',$id)->get();
      $rel=Product::where('category_id',$Pdto->category_id)->get();
       return view('detalle')->with(['img' => $img,'Pdto' => $Pdto ,'rel' => $rel]);
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $producto=Product::find($id);

      $request->validate([
            'categoria' => 'required',
            'nombre' => 'required',
            'descripcion' => 'required',
            'cantidad' => 'required|regex:/^[0-9 ,.\'-]+$/i',
            'precio' => 'required|regex:/^[0-9 ,.\'-]+$/i',
            'stock_min'=>'required|regex:/^[0-9 ,.\'-]+$/i',
            'stock_max'=>'required|regex:/^[0-9 ,.\'-]+$/i',
        ]);


       ////imagen destacada
      if ($request->hasFile('imagen')) {
        $file1 = $request->file('imagen');
          $img_ant=$producto->imagen;
     //elimino imagen anterior
    if ($file1->getClientOriginalExtension()=="jpg" || $file1->getClientOriginalExtension()=="png") {
        $path = public_path().'/images/dest';        
        $file1Name =uniqid().'-'.$file1->getClientOriginalName();
        $file1->move($path, $file1Name);
        $producto->image=$file1Name; 
        File::delete($path. $img_ant);
    }
        else{
             Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
             return back(); 
        }
       }

    //valido existencia de galeria
        



         $producto->category_id=$request->categoria; 
         $producto->name=$request->nombre; 
         $producto->description=$request->descripcion; 
         $producto->qty=$request->cantidad; 
         $producto->price=$request->precio; 
         
         $producto->stock_min=$request->stock_min; 
         $producto->stock_max=$request->stock_max; 
         

         
        
        $producto->save();

            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Modificación de Articulo|Producto Denominado:'. $request->nombre,
         ]);
        
      
         //gusrdo imagenes 
        
        $files = $request->file('file');
        $path = public_path().'/images/productos';      
         $active=0;//para que la primera imegen sea destacada del producto
         // recorremos cada archivo y lo subimos individualmente
        if ($request->hasFile('file')) {
         foreach ($files as $file) {     
         
        if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
         $fileName =uniqid().'-'.$file->getClientOriginalName();
         $file->move($path, $fileName);
         $image=new products_image();
         $image->producto_id=$producto->id;//relaciono modelos 
         $image->image=$fileName;
         $image->active=$active;
         $image->save();

         $active=0;
     }else{

         Session::flash('message', 'Algunos Archivos de Galeria Furon Invalidos Por favor Revisar');
         Session::flash('class', 'danger');


        return back();
        

         }
     }
      }
     //mensaje de guardado
          Session::flash('message', 'El Producto '.$request->nombre.' Ha sido Actualizado de manera Satisfactoria');
         Session::flash('class', 'success');


        return back();
        



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prod=Product::find($id);
        $path1 = public_path().'/images/productos/dest';
        File::delete($path1. $prod->image);
        $prod->status=9;


        $galery=products_image::where('producto_id',$id)->get();
        //elimino toda imagen
        foreach($galery as $gal){

        $gal->delete();
        $path = public_path().'/images/productos';   
        File::delete($path. $gal->image);


        }

        $prod->save();
            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Eliminacioón del Producto '.$prod->name,
         ]);

        Session::flash('message', 'El Producto '.$prod->name.' Ha Sido Anulado');
         Session::flash('class', 'success');

         return back();

    }




    public function destacar($id,$status){
        $prod=Product::find($id);
        $prod->destacado=$status;

        if($status==1){
            $var="Ha Sido Destacado";
        }else{
            $var="Ha Dejado de Estar Destacado";
        }
        
        if($prod->save()){


         Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'El Producto '.$prod->name.' '.$var,
         ]);
         
         Session::flash('message', 'El Producto '.$prod->name.' '.$var);
         Session::flash('class', 'success');
     }else{
         Session::flash('message', 'Disculpe Ha Ocurrido un Error Intente Luego');
         Session::flash('class', 'danger');

     }

        return redirect()->intended(url('/productos'));

    }


    public function deleteIm($id){
        $img=products_image::find($id);


        $img->delete();
        $path = public_path().'/images/productos';   
        File::delete($path. $img->imagen);

         Session::flash('message', 'La Imagen Se Ha Ejecutado Con éxito');
         Session::flash('class', 'success');

         return back();
    }

    public function buscar($name){
       $prod=Product::where('name','LIKE','%'.$name.'%')->get();

       // $prod=Product::all();
        return response($prod);


    }
    public function pdf()
    {   
        $products = Product::where('status','=','1')->where('qty','>','0')->orderBy('name')->get(); 
        $pdf = PDF::loadView('reports.products', compact('products'));
        return $pdf->stream('listado');
    }


    public function minpdf()
    {   
        $prod=Product::where('qty','<=','10')->orderBy('name')->get(); 
        $pdf = PDF::loadView('reports.min', compact('prod'));
        return $pdf->stream('listado');
    }

}
