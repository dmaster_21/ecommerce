<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

use App\User;
use App\Log;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $User=User::where('admin','>','1')->get();

        return view('user.index',compact('User'));
    }

   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

           $request->validate([
            'nac' => 'required|', 
            'cedula' => 'required|min:6|max:10', 
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',  
            'telefono' => 'required',  
            'rol' => 'required',         
            'direccion' => 'required|string']);
           
           

        $user=new User();
        $user->nac_usr = $request->nac;
        $user->ced_user = $request->cedula;
        $user->name = $request->name;
        $user->email =$request->email;
        $user->password = Hash::make($request->password);
        $user->telefono= $request->telefono;
        $user->admin= $request->rol;
        $user->direccion =$request->direccion;


         if($user->save()){

             Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Creación del Usuario ' .$request->name,
        'accion1'=>'Crear'
         ]);

          Session::flash('mensaje', 'Usuario de' .$request->name. 'Creado Exitosamente!.');
          Session::flash('class', 'success');

         }else{

          Session::flash('mensaje', 'Error de Acceso.');
          Session::flash('class', 'danger');


         }
         return back();

    }


    public function update(Request $request, $id)
    {
         $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|',            
            'direccion' => 'required|string']);
         

   $usr=User::find($id);
         if($request->hasFile('imagen')) {
        $file = $request->file('imagen');
     
    if ($file->getClientOriginalExtension()=="jpg" || $file->getClientOriginalExtension()=="png") {
        $path = public_path().'/images/avatar/';        
        $fileName =uniqid().'-'.$file->getClientOriginalName();
        $file->move($path, $fileName);
        $usr->photo=$fileName;

    }
        else{
             Session::flash('mensaje', 'El archivo no es una imagen valida.');
            Session::flash('class', 'danger');
             return back(); 
        }
    }
       

        
         $usr->name=$request->name;
         $usr->telefono=$request->telefono;    
         $usr->email=$request->email;     
         $usr->direccion=$request->direccion;
      

         if($usr->save()){
            Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Modificación del Usuario .'.$request->name ,
        'accion1'=>'Editar'
         ]);


          Session::flash('mensaje', 'Su información fue actualizada De Manera Exitosa.');
          Session::flash('class', 'success');

         }else{

          Session::flash('mensaje', 'Error de Acceso.');
          Session::flash('class', 'danger');


         }
         return back();






    }

    public function reset(Request $request, $id){

     $request->validate([
            'password' => 'required|string|min:6|confirmed']);

          $usr=User::find($id);
         $usr->password= Hash::make($request->password);


         if($usr->save()){

             Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Cambio de Clave del Usuario .'.$usr->name ,
        'accion1'=>'Eliminar'
         ]);

          Session::flash('mensaje', 'Estimado Usuario su Clave Fue Actualizada de Manera Exitosa.');
          Session::flash('class', 'success');

         }else{

          Session::flash('mensaje', 'Error de Acceso.');
          Session::flash('class', 'danger');


         }
         return back();

        
        

    }

     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
          $usr=User::find($id);


         if($usr->delete()){

          Session::flash('mensaje', 'El Usuario Ha Sido Eliminado');
          Session::flash('class', 'success');

              Log::create([       
        'user_id' => auth()->id(),
        'accion' => 'Ha Eliminado Al Usuario .'.$usr->name ,
        'accion1'=>'Eliminar'
         ]);

         }else{

          Session::flash('mensaje', 'Error de Acceso.');
          Session::flash('class', 'danger');


         }
         return back();

    }

    public function busc_cliente($cedula,$nac){

        $usr=User::where('ced_user','=',$cedula)->where('nac_usr',$nac)->get();
      //  if($usr){
        return $usr;
       /* }else{
            return response(['status' => 500]);
        }*/
    }


    public function logs(Request $request){



     $request->validate([
            'desde' => 'required',
            'hasta' => 'required',
            'user' => 'required',
            'accion'=>'required'

        ]);
     
$f1=date("Y/d/m h:m:s", strtotime($request->desde));
$f2=date("Y/d/m h:m:s", strtotime($request->hasta));


   if($request->user=='T'){
    $log=Log::whereBetween('created_at', [$request->desde, $request->hasta])->where('accion1',$request->accion)->get();

   }else{
      $log=Log::where('user_id',$request->user)->whereBetween('created_at', [$request->desde, $request->hasta])->where('accion1',$request->accion)->get();
   }


  
    $user=User::where('admin','<>','1')->get();
 
     return view('log.index',compact('log','user'));
    }

    
}
