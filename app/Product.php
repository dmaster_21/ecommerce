<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
      protected $fillable = [
        'name', 'description', 'qty','price','stock_min','stock_max',
    ];

     protected $hidden = [
        'destacado', 'status'
    ];


public function category()
    {
 
        return $this->hasOne('App\Category','id','category_id'); // Le indicamos que se va relacionar con el 
    }

    public function detalle(){
        return $this->hasMany('App\Compras_cart_detalle','productos_id','id');  
      }
}
