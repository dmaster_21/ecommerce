<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_carts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('nro_orden');
            $table->string('annio_fiscal');
            $table->integer('nro_factura');
            $table->integer('nro_control');
            $table->timestamp('fecha_compra');
            $table->double('monto_compra',15,2);
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users') ;            
             $table->enum('status',
                [
                    App\Compras_cart::PROCESADO,
                    App\Compras_cart::REPORTADO,
                    App\Compras_cart::PAGADO,
                    App\Compras_cart::ANULADO                    
                ])->default(App\Compras_cart::PROCESADO);

            $table->timestamp('fecha_pago')->nullable();
            //relacion con la tabla pagos 
            $table->unsignedInteger('tipo_id')->nullable();
            //$table->foreign('tipo_id')->references('id')->on('tipo_pagos'); 
             $table->unsignedInteger('envios_id')->nullable();
              $table->double('monto_envio',15,2)->nullable();


            //relacion con el banco 
             $table->unsignedInteger('banco_id')->nullable();
            //$table->foreign('banco_id')->references('id')->on('bancos') ;  

            $table->integer('ref_pago')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras_carts');
    }
}
