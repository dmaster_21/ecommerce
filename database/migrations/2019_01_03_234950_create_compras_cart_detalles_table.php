<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComprasCartDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compras_cart_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('compras_carts_id');
            $table->foreign('compras_carts_id')->references('id')->on('compras_carts') ; 

            $table->unsignedInteger('products_id');
            $table->foreign('products_id')->references('id')->on('products');

            $table->integer('qty');
            $table->double('price',15,2); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compras_cart_detalles');
    }
}
