<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFechaDespachoToComprasCarts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compras_carts', function (Blueprint $table) {
            
          $table->Integer('cond_despacho')->default('1');
          $table->datetime('fecha_despacho')->nullable();
          $table->string('nota_despacho')->nullable();
          $table->integer('user_id_despacho');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compras_carts', function (Blueprint $table) {
            
            $table->dropColumn('cond_despacho');            
            $table->dropColumn('fecha_despacho');
            $table->dropColumn('nota_despacho');
            $table->dropColumn('user_id_despacho');
        });
    }
}
