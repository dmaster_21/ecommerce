<?php

use Illuminate\Database\Seeder;
use App\User;
class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //--cero registro base

        User::create([
        		'name' =>'Yenderson Hernandez',
        		'email' => 'Yendersonhernandez@gmail.com',
        		'password' => bcrypt('21464791'),               
                'telefono'=> '04142563212',
                'direccion' => 'Caracas'
        	]);
    }
}
