/*
Navicat MySQL Data Transfer

Source Server         : Laragon
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : karla

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-01-30 22:57:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `bancos`
-- ----------------------------
DROP TABLE IF EXISTS `bancos`;
CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuenta` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of bancos
-- ----------------------------
INSERT INTO `bancos` VALUES ('1', 'BANCO DEL CARIBE', '1145236665', '9', null, '2019-01-21 22:27:11');
INSERT INTO `bancos` VALUES ('2', 'BANC DE VENEZUELA', '523362222', '1', null, '2019-01-21 04:13:08');
INSERT INTO `bancos` VALUES ('5', 'BANCO BANESCO BANCO UNIVERSAL', null, '1', '2019-01-21 04:26:06', '2019-01-21 04:26:06');
INSERT INTO `bancos` VALUES ('6', 'BANCO FONDO COMÚN', null, '1', '2019-01-21 04:26:58', '2019-01-21 04:26:58');
INSERT INTO `bancos` VALUES ('7', 'BANCO PROVINCIAL', null, '1', '2019-01-21 04:27:17', '2019-01-21 04:27:17');
INSERT INTO `bancos` VALUES ('8', 'BANCO MERCANTIL', null, '9', '2019-01-21 04:27:38', '2019-01-21 22:46:32');

-- ----------------------------
-- Table structure for `banco_pago`
-- ----------------------------
DROP TABLE IF EXISTS `banco_pago`;
CREATE TABLE `banco_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_banco` varchar(250) DEFAULT NULL,
  `cuenta_filial` int(20) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of banco_pago
-- ----------------------------

-- ----------------------------
-- Table structure for `categories`
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_user_id_foreign` (`user_id`),
  CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of categories
-- ----------------------------
INSERT INTO `categories` VALUES ('1', 'Maquillaje', '9', '1', '2019-01-12 01:28:49', '2019-01-30 06:03:26');
INSERT INTO `categories` VALUES ('2', 'Cosmeticos', '1', '1', '2019-01-12 01:28:58', '2019-01-21 02:17:07');
INSERT INTO `categories` VALUES ('3', 'BISUTERIA', '1', '1', '2019-01-30 05:02:44', '2019-01-30 05:02:44');
INSERT INTO `categories` VALUES ('4', 'Ropa Interior', '1', '1', '2019-01-30 05:04:37', '2019-01-30 05:04:37');
INSERT INTO `categories` VALUES ('5', 'Calzados', '1', '1', '2019-01-30 05:35:46', '2019-01-30 05:35:46');
INSERT INTO `categories` VALUES ('6', 'Cosmeticos', '1', '1', '2019-01-30 05:37:14', '2019-01-30 05:37:14');

-- ----------------------------
-- Table structure for `compras_carts`
-- ----------------------------
DROP TABLE IF EXISTS `compras_carts`;
CREATE TABLE `compras_carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nro_orden` int(11) NOT NULL,
  `annio_fiscal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nro_factura` int(11) NOT NULL,
  `nro_control` int(11) NOT NULL,
  `fecha_compra` timestamp NOT NULL,
  `monto_compra` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('1','9','3','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `envios_id` int(10) unsigned NOT NULL,
  `monto_envio` double(15,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `tipo_id` int(10) unsigned DEFAULT NULL,
  `ref_pago` int(11) DEFAULT NULL,
  `banco_id` int(10) unsigned DEFAULT NULL,
  `cajero` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plataforma` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_carts_user_id_foreign` (`user_id`),
  CONSTRAINT `compras_carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of compras_carts
-- ----------------------------
INSERT INTO `compras_carts` VALUES ('1', '1', '2019', '46', '47', '2019-01-15 02:22:19', '7120.60', null, '2', '3', '7', null, '2019-01-20 19:49:35', '2', '985632', '1', null, '', '2019-01-15 02:22:19', '2019-01-20 19:49:35');
INSERT INTO `compras_carts` VALUES ('2', '1', '2019', '46', '47', '2019-01-18 02:27:39', '6230.00', null, '2', '3', '8', null, '2019-01-20 19:36:15', '2', '985632', '2', null, '', '2019-01-18 02:27:40', '2019-01-20 19:36:15');
INSERT INTO `compras_carts` VALUES ('10', '2', '2019', '47', '48', '2019-01-18 04:02:15', '17260.00', null, '2', '1', '7', null, '2019-01-20 19:34:09', '1', '3963', null, null, '', '2019-01-18 04:02:15', '2019-01-23 19:52:26');
INSERT INTO `compras_carts` VALUES ('11', '3', '2019', '48', '49', '2019-01-20 20:02:27', '12460.00', null, '3', '9', '6', null, null, null, null, '1', null, '', '2019-01-20 20:02:27', '2019-01-31 02:52:43');
INSERT INTO `compras_carts` VALUES ('12', '4', '2019', '49', '50', '2019-01-23 15:32:42', '1200.00', '16', '3', '3', '5', null, '2019-01-25 02:31:39', '1', '987656', '5', null, '', '2019-01-23 15:32:42', '2019-01-26 21:02:00');
INSERT INTO `compras_carts` VALUES ('13', '5', '2019', '50', '51', '2019-01-23 16:35:21', '1500.00', '16', '3', '3', '4', '3450.00', '2019-01-23 19:10:00', '2', '9098', '5', null, '', '2019-01-23 16:35:21', '2019-01-26 20:59:12');
INSERT INTO `compras_carts` VALUES ('14', '6', '2019', '51', '52', '2019-01-23 16:39:18', '5300.00', '16', '1', '1', '4', '7800.00', null, null, null, null, null, '', '2019-01-23 16:39:18', '2019-01-23 20:24:15');
INSERT INTO `compras_carts` VALUES ('15', '7', '2019', '52', '53', '2019-01-28 03:12:14', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:12:14', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:12:14', '2019-01-28 03:12:14');
INSERT INTO `compras_carts` VALUES ('16', '8', '2019', '53', '54', '2019-01-28 03:20:47', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:20:47', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:20:47', '2019-01-28 03:20:47');
INSERT INTO `compras_carts` VALUES ('17', '9', '2019', '54', '55', '2019-01-28 03:21:23', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:21:23', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:21:23', '2019-01-28 03:21:23');
INSERT INTO `compras_carts` VALUES ('18', '10', '2019', '55', '56', '2019-01-28 03:22:05', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:22:05', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:22:05', '2019-01-28 03:22:05');
INSERT INTO `compras_carts` VALUES ('19', '11', '2019', '56', '57', '2019-01-28 03:26:06', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:26:06', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:26:06', '2019-01-28 03:26:06');
INSERT INTO `compras_carts` VALUES ('20', '12', '2019', '57', '58', '2019-01-28 03:27:02', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:27:02', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:27:02', '2019-01-28 03:27:02');
INSERT INTO `compras_carts` VALUES ('21', '13', '2019', '58', '59', '2019-01-28 03:27:17', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:27:17', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:27:17', '2019-01-28 03:27:17');
INSERT INTO `compras_carts` VALUES ('22', '14', '2019', '59', '60', '2019-01-28 03:28:45', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:28:45', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:28:45', '2019-01-28 03:28:45');
INSERT INTO `compras_carts` VALUES ('23', '15', '2019', '60', '61', '2019-01-28 03:28:57', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:28:57', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:28:57', '2019-01-28 03:28:57');
INSERT INTO `compras_carts` VALUES ('24', '16', '2019', '61', '62', '2019-01-28 03:30:46', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:30:46', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:30:46', '2019-01-28 03:30:46');
INSERT INTO `compras_carts` VALUES ('25', '17', '2019', '62', '63', '2019-01-28 03:31:36', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:31:36', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:31:36', '2019-01-28 03:31:36');
INSERT INTO `compras_carts` VALUES ('26', '18', '2019', '63', '64', '2019-01-28 03:32:04', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:32:04', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:32:04', '2019-01-28 03:32:04');
INSERT INTO `compras_carts` VALUES ('27', '19', '2019', '64', '65', '2019-01-28 03:32:23', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:32:23', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:32:23', '2019-01-28 03:32:23');
INSERT INTO `compras_carts` VALUES ('28', '20', '2019', '65', '66', '2019-01-28 03:33:23', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:33:23', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:33:23', '2019-01-28 03:33:23');
INSERT INTO `compras_carts` VALUES ('29', '21', '2019', '66', '67', '2019-01-28 03:33:39', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:33:39', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:33:39', '2019-01-28 03:33:39');
INSERT INTO `compras_carts` VALUES ('30', '22', '2019', '67', '68', '2019-01-28 03:33:53', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:33:53', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:33:53', '2019-01-28 03:33:53');
INSERT INTO `compras_carts` VALUES ('31', '23', '2019', '68', '69', '2019-01-28 03:34:16', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:34:16', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:34:16', '2019-01-28 03:34:16');
INSERT INTO `compras_carts` VALUES ('32', '24', '2019', '69', '70', '2019-01-28 03:34:47', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:34:47', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:34:47', '2019-01-28 03:34:47');
INSERT INTO `compras_carts` VALUES ('33', '25', '2019', '70', '71', '2019-01-28 03:35:45', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:35:45', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:35:45', '2019-01-28 03:35:45');
INSERT INTO `compras_carts` VALUES ('34', '26', '2019', '71', '72', '2019-01-28 03:36:00', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:36:00', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:36:00', '2019-01-28 03:36:00');
INSERT INTO `compras_carts` VALUES ('35', '27', '2019', '72', '73', '2019-01-28 03:36:15', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:36:15', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:36:15', '2019-01-28 03:36:15');
INSERT INTO `compras_carts` VALUES ('36', '28', '2019', '73', '74', '2019-01-28 03:36:37', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:36:37', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:36:37', '2019-01-28 03:36:37');
INSERT INTO `compras_carts` VALUES ('37', '29', '2019', '74', '75', '2019-01-28 03:37:02', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:37:02', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:37:02', '2019-01-28 03:37:02');
INSERT INTO `compras_carts` VALUES ('38', '30', '2019', '75', '76', '2019-01-28 03:37:18', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:37:18', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:37:18', '2019-01-28 03:37:18');
INSERT INTO `compras_carts` VALUES ('39', '31', '2019', '76', '77', '2019-01-28 03:37:38', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:37:38', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:37:38', '2019-01-28 03:37:38');
INSERT INTO `compras_carts` VALUES ('40', '32', '2019', '77', '78', '2019-01-28 03:38:15', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:38:15', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:38:15', '2019-01-28 03:38:15');
INSERT INTO `compras_carts` VALUES ('41', '33', '2019', '78', '79', '2019-01-28 03:38:44', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:38:44', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:38:44', '2019-01-28 03:38:44');
INSERT INTO `compras_carts` VALUES ('42', '34', '2019', '79', '80', '2019-01-28 03:39:05', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:39:05', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:39:05', '2019-01-28 03:39:05');
INSERT INTO `compras_carts` VALUES ('43', '35', '2019', '80', '81', '2019-01-28 03:39:33', '88210.30', '16', '1', '1', '5', '3450.00', '2019-01-28 03:39:33', '2', '9087989', '6', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 03:39:33', '2019-01-28 03:39:33');
INSERT INTO `compras_carts` VALUES ('44', '36', '2019', '81', '82', '2019-01-28 04:04:24', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:04:24', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:04:24', '2019-01-28 04:04:24');
INSERT INTO `compras_carts` VALUES ('45', '37', '2019', '82', '83', '2019-01-28 04:05:37', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:05:37', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:05:37', '2019-01-28 04:05:37');
INSERT INTO `compras_carts` VALUES ('46', '38', '2019', '83', '84', '2019-01-28 04:06:29', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:06:29', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:06:29', '2019-01-28 04:06:29');
INSERT INTO `compras_carts` VALUES ('47', '39', '2019', '84', '85', '2019-01-28 04:06:52', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:06:52', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:06:52', '2019-01-28 04:06:52');
INSERT INTO `compras_carts` VALUES ('48', '40', '2019', '85', '86', '2019-01-28 04:07:35', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:07:35', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:07:35', '2019-01-28 04:07:35');
INSERT INTO `compras_carts` VALUES ('49', '41', '2019', '86', '87', '2019-01-28 04:08:25', '13240.30', '16', '1', '1', '5', '3450.00', '2019-01-28 04:08:25', '3', '456', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 04:08:25', '2019-01-28 04:08:25');
INSERT INTO `compras_carts` VALUES ('50', '42', '2019', '87', '88', '2019-01-28 21:57:50', '17640.30', '16', '1', '1', '5', '3450.00', '2019-01-28 21:57:50', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 21:57:50', '2019-01-28 21:57:50');
INSERT INTO `compras_carts` VALUES ('51', '43', '2019', '88', '89', '2019-01-28 22:27:54', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:27:54', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:27:54', '2019-01-28 22:27:54');
INSERT INTO `compras_carts` VALUES ('52', '44', '2019', '89', '90', '2019-01-28 22:29:48', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:29:48', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:29:48', '2019-01-28 22:29:48');
INSERT INTO `compras_carts` VALUES ('53', '45', '2019', '90', '91', '2019-01-28 22:32:11', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:32:11', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:32:11', '2019-01-28 22:32:11');
INSERT INTO `compras_carts` VALUES ('54', '46', '2019', '91', '92', '2019-01-28 22:33:30', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:33:30', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:33:30', '2019-01-28 22:33:30');
INSERT INTO `compras_carts` VALUES ('55', '47', '2019', '92', '93', '2019-01-28 22:34:30', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:34:30', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:34:30', '2019-01-28 22:34:30');
INSERT INTO `compras_carts` VALUES ('56', '48', '2019', '93', '94', '2019-01-28 22:34:49', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:34:49', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:34:49', '2019-01-28 22:34:49');
INSERT INTO `compras_carts` VALUES ('57', '49', '2019', '94', '95', '2019-01-28 22:43:35', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:43:35', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:43:35', '2019-01-28 22:43:35');
INSERT INTO `compras_carts` VALUES ('58', '50', '2019', '95', '96', '2019-01-28 22:53:14', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:53:14', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:53:14', '2019-01-28 22:53:14');
INSERT INTO `compras_carts` VALUES ('59', '51', '2019', '96', '97', '2019-01-28 22:53:28', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:53:28', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:53:28', '2019-01-28 22:53:28');
INSERT INTO `compras_carts` VALUES ('60', '52', '2019', '97', '98', '2019-01-28 22:53:51', '17991.10', '16', '1', '1', '6', '6700.80', '2019-01-28 22:53:51', '1', '9878', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-28 22:53:51', '2019-01-28 22:53:51');
INSERT INTO `compras_carts` VALUES ('61', '53', '2019', '98', '99', '2019-01-29 01:02:24', '9790.30', '16', '5', '3', '8', '0.00', '2019-01-29 01:02:24', '1', '9899', '5', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 01:02:24', '2019-01-29 01:02:24');
INSERT INTO `compras_carts` VALUES ('62', '54', '2019', '99', '100', '2019-01-29 02:25:45', '14790.30', '16', '1', '3', '5', '3450.00', '2019-01-29 02:25:45', '1', '234555', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:25:45', '2019-01-29 02:25:45');
INSERT INTO `compras_carts` VALUES ('63', '55', '2019', '100', '101', '2019-01-29 02:27:26', '11360.30', '16', '5', '3', '4', '7800.00', '2019-01-29 02:27:26', '1', '987899', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:27:26', '2019-01-29 02:27:26');
INSERT INTO `compras_carts` VALUES ('64', '56', '2019', '101', '102', '2019-01-29 02:28:10', '11360.30', '16', '5', '3', '4', '7800.00', '2019-01-29 02:28:10', '1', '987899', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:28:10', '2019-01-29 02:28:10');
INSERT INTO `compras_carts` VALUES ('65', '57', '2019', '102', '103', '2019-01-29 02:28:24', '11960.30', '16', '5', '3', '4', '7800.00', '2019-01-29 02:28:24', '1', '987899', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:28:24', '2019-01-29 02:28:24');
INSERT INTO `compras_carts` VALUES ('66', '58', '2019', '103', '104', '2019-01-29 02:29:00', '11960.30', '16', '5', '3', '4', '7800.00', '2019-01-29 02:29:00', '1', '987899', '2', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:29:00', '2019-01-29 02:29:00');
INSERT INTO `compras_carts` VALUES ('67', '59', '2019', '104', '105', '2019-01-29 02:29:41', '99600.00', '16', '5', '3', '4', '7800.00', '2019-01-29 02:29:41', '1', '987899', '5', 'Yenderson Hernandez', 'TIENDA FISICA', '2019-01-29 02:29:41', '2019-01-29 02:29:41');
INSERT INTO `compras_carts` VALUES ('68', '60', '2019', '105', '106', '2019-01-31 02:17:53', '1500.00', '16', '3', '3', '5', '3450.00', null, null, null, null, 'CLIENTE', 'PLATAFORMA EN LINEA', '2019-01-31 02:17:54', '2019-01-31 02:17:54');

-- ----------------------------
-- Table structure for `compras_cart_detalles`
-- ----------------------------
DROP TABLE IF EXISTS `compras_cart_detalles`;
CREATE TABLE `compras_cart_detalles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `compras_carts_id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_cart_detalles_compras_carts_id_foreign` (`compras_carts_id`),
  KEY `compras_cart_detalles_products_id_foreign` (`products_id`),
  CONSTRAINT `compras_cart_detalles_compras_carts_id_foreign` FOREIGN KEY (`compras_carts_id`) REFERENCES `compras_carts` (`id`),
  CONSTRAINT `compras_cart_detalles_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of compras_cart_detalles
-- ----------------------------
INSERT INTO `compras_cart_detalles` VALUES ('1', '1', '1', '2', '3560.30', null, '2019-01-15 02:22:20', '2019-01-15 02:22:20');
INSERT INTO `compras_cart_detalles` VALUES ('2', '2', '2', '1', '5630.00', null, '2019-01-18 02:27:40', '2019-01-18 02:27:40');
INSERT INTO `compras_cart_detalles` VALUES ('3', '2', '3', '1', '600.00', null, '2019-01-18 02:27:40', '2019-01-18 02:27:40');
INSERT INTO `compras_cart_detalles` VALUES ('4', '10', '2', '2', '5630.00', null, '2019-01-18 04:02:15', '2019-01-18 04:02:15');
INSERT INTO `compras_cart_detalles` VALUES ('5', '10', '10', '2', '3000.00', null, '2019-01-18 04:02:15', '2019-01-18 04:02:15');
INSERT INTO `compras_cart_detalles` VALUES ('6', '11', '3', '2', '600.00', null, '2019-01-20 20:02:27', '2019-01-20 20:02:27');
INSERT INTO `compras_cart_detalles` VALUES ('7', '11', '2', '2', '5630.00', null, '2019-01-20 20:02:27', '2019-01-20 20:02:27');
INSERT INTO `compras_cart_detalles` VALUES ('8', '12', '3', '2', '600.00', '16', '2019-01-23 15:32:42', '2019-01-23 15:32:42');
INSERT INTO `compras_cart_detalles` VALUES ('9', '13', '4', '1', '1500.00', '16', '2019-01-23 16:35:21', '2019-01-23 16:35:21');
INSERT INTO `compras_cart_detalles` VALUES ('10', '14', '9', '1', '5300.00', '16', '2019-01-23 16:39:19', '2019-01-23 16:39:19');
INSERT INTO `compras_cart_detalles` VALUES ('11', '24', '1', '1', '879.00', '16', '2019-01-28 03:30:46', '2019-01-28 03:30:46');
INSERT INTO `compras_cart_detalles` VALUES ('12', '24', '10', '1', '879.00', '16', '2019-01-28 03:30:46', '2019-01-28 03:30:46');
INSERT INTO `compras_cart_detalles` VALUES ('13', '24', '7', '1', '879.00', '16', '2019-01-28 03:30:46', '2019-01-28 03:30:46');
INSERT INTO `compras_cart_detalles` VALUES ('14', '25', '1', '1', '879.00', '16', '2019-01-28 03:31:36', '2019-01-28 03:31:36');
INSERT INTO `compras_cart_detalles` VALUES ('15', '25', '1', '1', '879.00', '16', '2019-01-28 03:31:37', '2019-01-28 03:31:37');
INSERT INTO `compras_cart_detalles` VALUES ('16', '25', '1', '1', '879.00', '16', '2019-01-28 03:31:37', '2019-01-28 03:31:37');
INSERT INTO `compras_cart_detalles` VALUES ('17', '26', '1', '1', '879.00', '16', '2019-01-28 03:32:04', '2019-01-28 03:32:04');
INSERT INTO `compras_cart_detalles` VALUES ('18', '26', '1', '1', '879.00', '16', '2019-01-28 03:32:04', '2019-01-28 03:32:04');
INSERT INTO `compras_cart_detalles` VALUES ('19', '27', '1', '1', '879.00', '16', '2019-01-28 03:32:23', '2019-01-28 03:32:23');
INSERT INTO `compras_cart_detalles` VALUES ('20', '27', '1', '1', '879.00', '16', '2019-01-28 03:32:23', '2019-01-28 03:32:23');
INSERT INTO `compras_cart_detalles` VALUES ('21', '27', '1', '1', '879.00', '16', '2019-01-28 03:32:23', '2019-01-28 03:32:23');
INSERT INTO `compras_cart_detalles` VALUES ('22', '39', '1', '1', '879.00', '16', '2019-01-28 03:37:38', '2019-01-28 03:37:38');
INSERT INTO `compras_cart_detalles` VALUES ('23', '39', '10', '1', '879.00', '16', '2019-01-28 03:37:38', '2019-01-28 03:37:38');
INSERT INTO `compras_cart_detalles` VALUES ('24', '39', '7', '1', '879.00', '16', '2019-01-28 03:37:38', '2019-01-28 03:37:38');
INSERT INTO `compras_cart_detalles` VALUES ('26', '27', '1', '1', '879.00', '16', '2019-01-28 03:38:44', '2019-01-28 03:38:44');
INSERT INTO `compras_cart_detalles` VALUES ('27', '27', '10', '1', '879.00', '16', '2019-01-28 03:38:44', '2019-01-28 03:38:44');
INSERT INTO `compras_cart_detalles` VALUES ('28', '27', '7', '1', '879.00', '16', '2019-01-28 03:38:44', '2019-01-28 03:38:44');
INSERT INTO `compras_cart_detalles` VALUES ('29', '27', '1', '1', '879.00', '16', '2019-01-28 03:39:05', '2019-01-28 03:39:05');
INSERT INTO `compras_cart_detalles` VALUES ('30', '27', '10', '1', '879.00', '16', '2019-01-28 03:39:05', '2019-01-28 03:39:05');
INSERT INTO `compras_cart_detalles` VALUES ('31', '27', '7', '1', '879.00', '16', '2019-01-28 03:39:05', '2019-01-28 03:39:05');
INSERT INTO `compras_cart_detalles` VALUES ('32', '60', '1', '1', '3560.30', '16', '2019-01-28 22:53:51', '2019-01-28 22:53:51');
INSERT INTO `compras_cart_detalles` VALUES ('33', '60', '2', '1', '5630.00', '16', '2019-01-28 22:53:52', '2019-01-28 22:53:52');
INSERT INTO `compras_cart_detalles` VALUES ('34', '60', '3', '1', '600.00', '16', '2019-01-28 22:53:52', '2019-01-28 22:53:52');
INSERT INTO `compras_cart_detalles` VALUES ('35', '61', '1', '1', '3560.30', '16', '2019-01-29 01:02:24', '2019-01-29 01:02:24');
INSERT INTO `compras_cart_detalles` VALUES ('36', '61', '2', '1', '5630.00', '16', '2019-01-29 01:02:24', '2019-01-29 01:02:24');
INSERT INTO `compras_cart_detalles` VALUES ('37', '61', '3', '1', '600.00', '16', '2019-01-29 01:02:24', '2019-01-29 01:02:24');
INSERT INTO `compras_cart_detalles` VALUES ('38', '62', '1', '1', '3560.30', '16', '2019-01-29 02:25:45', '2019-01-29 02:25:45');
INSERT INTO `compras_cart_detalles` VALUES ('39', '62', '2', '1', '5630.00', '16', '2019-01-29 02:25:45', '2019-01-29 02:25:45');
INSERT INTO `compras_cart_detalles` VALUES ('40', '62', '3', '1', '600.00', '16', '2019-01-29 02:25:45', '2019-01-29 02:25:45');
INSERT INTO `compras_cart_detalles` VALUES ('41', '63', '1', '1', '3560.30', '16', '2019-01-29 02:27:26', '2019-01-29 02:27:26');
INSERT INTO `compras_cart_detalles` VALUES ('42', '65', '3', '1', '600.00', '16', '2019-01-29 02:28:24', '2019-01-29 02:28:24');
INSERT INTO `compras_cart_detalles` VALUES ('43', '66', '1', '1', '3560.30', '16', '2019-01-29 02:29:00', '2019-01-29 02:29:00');
INSERT INTO `compras_cart_detalles` VALUES ('44', '66', '3', '1', '600.00', '16', '2019-01-29 02:29:01', '2019-01-29 02:29:01');
INSERT INTO `compras_cart_detalles` VALUES ('45', '67', '6', '1', '3500.00', '16', '2019-01-29 02:29:41', '2019-01-29 02:29:41');
INSERT INTO `compras_cart_detalles` VALUES ('46', '67', '5', '1', '5600.00', '16', '2019-01-29 02:29:41', '2019-01-29 02:29:41');
INSERT INTO `compras_cart_detalles` VALUES ('47', '67', '4', '1', '1500.00', '16', '2019-01-29 02:29:41', '2019-01-29 02:29:41');
INSERT INTO `compras_cart_detalles` VALUES ('48', '68', '4', '1', '1500.00', '16', '2019-01-31 02:17:54', '2019-01-31 02:17:54');

-- ----------------------------
-- Table structure for `envios`
-- ----------------------------
DROP TABLE IF EXISTS `envios`;
CREATE TABLE `envios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of envios
-- ----------------------------
INSERT INTO `envios` VALUES ('4', 'RapiEnvios', '7800.00', '1', '2019-01-22 00:59:29', '2019-01-22 01:08:41');
INSERT INTO `envios` VALUES ('5', 'Thealca', '3450.00', '1', '2019-01-22 01:00:05', '2019-01-22 01:00:05');
INSERT INTO `envios` VALUES ('6', 'Liberty Express', '6700.80', '1', '2019-01-22 01:00:29', '2019-01-22 01:00:29');
INSERT INTO `envios` VALUES ('7', 'Zoom', '8000.00', '1', '2019-01-22 01:00:46', '2019-01-22 01:00:46');
INSERT INTO `envios` VALUES ('8', 'Acordar Con el Vendedor', '0.00', '1', null, null);
INSERT INTO `envios` VALUES ('9', 'Retirar En Tienda Fisica', '0.00', '1', null, '2019-01-30 06:05:19');

-- ----------------------------
-- Table structure for `ivas`
-- ----------------------------
DROP TABLE IF EXISTS `ivas`;
CREATE TABLE `ivas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `porc` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of ivas
-- ----------------------------
INSERT INTO `ivas` VALUES ('1', '16', null, null);

-- ----------------------------
-- Table structure for `logs`
-- ----------------------------
DROP TABLE IF EXISTS `logs`;
CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `accion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of logs
-- ----------------------------
INSERT INTO `logs` VALUES ('1', '1', 'Creo Una Nueva Categoria LlamadaCosmeticos', '2019-01-30 05:37:14', '2019-01-30 05:37:14');
INSERT INTO `logs` VALUES ('2', '1', 'Creo Una Nueva Categoria Llamadavehiculos', '2019-01-30 05:39:28', '2019-01-30 05:39:28');
INSERT INTO `logs` VALUES ('3', '1', 'Eliminación de Categoriavehiculos', '2019-01-30 06:02:37', '2019-01-30 06:02:37');
INSERT INTO `logs` VALUES ('4', '1', 'Eliminación de Categoria vehiculos', '2019-01-30 06:03:17', '2019-01-30 06:03:17');
INSERT INTO `logs` VALUES ('5', '1', 'La Categoria Maquillaje Ha Sido Inhabilitada Satisfactoriamente', '2019-01-30 06:03:26', '2019-01-30 06:03:26');
INSERT INTO `logs` VALUES ('6', '1', 'Modificación de la Categoria Maquillaje', '2019-01-30 06:03:59', '2019-01-30 06:03:59');
INSERT INTO `logs` VALUES ('7', '1', 'Modificación de Agencia de Envio Retirar En Tienda Fisica', '2019-01-30 06:04:48', '2019-01-30 06:04:48');
INSERT INTO `logs` VALUES ('8', '1', 'Modificación del Usuario .KARLA KASTILLO', '2019-01-31 00:57:38', '2019-01-31 00:57:38');
INSERT INTO `logs` VALUES ('9', '1', 'Modificación del Usuario .KARLA KASTILLO', '2019-01-31 01:00:00', '2019-01-31 01:00:00');
INSERT INTO `logs` VALUES ('10', '1', 'Modificación del Usuario .KARLA KASTILLO', '2019-01-31 01:00:35', '2019-01-31 01:00:35');
INSERT INTO `logs` VALUES ('11', '1', 'Modificación del Usuario .KARLA KASTILLO', '2019-01-31 01:41:21', '2019-01-31 01:41:21');
INSERT INTO `logs` VALUES ('12', '1', 'Modificación del Usuario .KARLA KASTILLO', '2019-01-31 01:52:18', '2019-01-31 01:52:18');
INSERT INTO `logs` VALUES ('13', '3', 'El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 60', '2019-01-31 02:17:54', '2019-01-31 02:17:54');
INSERT INTO `logs` VALUES ('14', '1', 'La Compra Número:3Ha Fue Eliminada', '2019-01-31 02:52:43', '2019-01-31 02:52:43');

-- ----------------------------
-- Table structure for `migrations`
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('10', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('11', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('12', '2018_12_18_030738_create_categories_table', '1');
INSERT INTO `migrations` VALUES ('13', '2018_12_18_032522_create_products_table', '1');
INSERT INTO `migrations` VALUES ('14', '2018_12_20_015820_create_products_images_table', '1');
INSERT INTO `migrations` VALUES ('15', '2019_01_03_234633_create_compras_carts_table', '1');
INSERT INTO `migrations` VALUES ('16', '2019_01_03_234950_create_compras_cart_detalles_table', '1');
INSERT INTO `migrations` VALUES ('17', '2019_01_04_003117_create_bancos_table', '1');
INSERT INTO `migrations` VALUES ('18', '2019_01_04_003226_create_tipo_pagos_table', '1');
INSERT INTO `migrations` VALUES ('19', '2019_01_20_201233_create_sugerencias_table', '2');
INSERT INTO `migrations` VALUES ('20', '2019_01_21_225233_create_envios_table', '3');
INSERT INTO `migrations` VALUES ('21', '2019_01_23_032331_create_ivas_table', '4');
INSERT INTO `migrations` VALUES ('22', '2019_01_30_045725_create_logs_table', '5');

-- ----------------------------
-- Table structure for `password_resets`
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for `products`
-- ----------------------------
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  KEY `products_user_id_foreign` (`user_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of products
-- ----------------------------
INSERT INTO `products` VALUES ('1', '1', 'TABLA DE MAQUILLAJE', 'MAQUILLAJE CONTENEDOR CON ESPECIFICACIONES DE GRAN INDOLE', '50', '3560.30', '10', '15', '5c3a95281a788-IMG_20181126_201345.png', '1', '1', null, '1', '2019-01-12 01:58:23', '2019-01-29 02:29:00');
INSERT INTO `products` VALUES ('2', '1', 'ESTUCHE MULTICOLOR INTEGRADO', 'ESTUCHE COORPORTATIVO', '27', '5630.00', '5', '100', '5c3ce4877c66e-IMG_20181126_202333.png', '0', '1', null, '1', '2019-01-14 19:35:35', '2019-01-29 02:25:45');
INSERT INTO `products` VALUES ('3', '1', 'labial principal', '-', '36', '600.00', '1', '300', '5c3ce4fb9dab1-IMG_20181126_195549.png', '1', '1', null, '1', '2019-01-14 19:37:31', '2019-01-29 02:29:01');
INSERT INTO `products` VALUES ('4', '2', 'ZAPATOS DEPORTVOS', '--', '91', '1500.00', '20', '60', '5c3ce52955090-IMG_20181126_195647.png', '0', '1', null, '1', '2019-01-14 19:38:17', '2019-01-31 02:17:54');
INSERT INTO `products` VALUES ('5', '2', 'BODY ROJO', '--', '19', '5600.00', '2', '1', '5c3ce85c63d4d-IMG_20181126_195627.png', '0', '1', null, '1', '2019-01-14 19:52:00', '2019-01-29 02:29:41');
INSERT INTO `products` VALUES ('6', '2', 'BODY TROPICAL', '-', '99', '3500.00', '5', '100', '5c3d35ee71ca7-IMG_20181126_201711.png', '1', '1', null, '1', '2019-01-15 01:22:55', '2019-01-29 02:29:41');
INSERT INTO `products` VALUES ('7', '2', 'BERMUDA FALDON', '--', '50', '78200.00', '1', '100', '5c3d36402451a-IMG_20181126_202155.png', '1', '1', null, '1', '2019-01-15 01:24:16', '2019-01-15 01:24:16');
INSERT INTO `products` VALUES ('8', '2', 'BLUSAS ESCOTEX', '--', '100', '5000.00', '1', '100', '5c3d366d699e2-IMG_20181126_201751.png', '0', '1', null, '1', '2019-01-15 01:25:01', '2019-01-15 01:25:01');
INSERT INTO `products` VALUES ('9', '2', 'PINTURA LABIAL', '--', '11', '5300.00', '1', '10', '5c3d36c02ff8d-IMG_20181126_195549.png', '1', '1', null, '1', '2019-01-15 01:26:24', '2019-01-23 16:39:19');
INSERT INTO `products` VALUES ('10', '1', 'DELINEADOR PRINCIPAL', '--', '26', '3000.00', '25', '50', '5c3d36ef135f2-IMG_20181126_195659.png', '0', '1', null, '1', '2019-01-15 01:27:11', '2019-01-18 04:02:15');

-- ----------------------------
-- Table structure for `products_images`
-- ----------------------------
DROP TABLE IF EXISTS `products_images`;
CREATE TABLE `products_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_producto_id_foreign` (`producto_id`),
  CONSTRAINT `products_images_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of products_images
-- ----------------------------
INSERT INTO `products_images` VALUES ('1', '1', '5c3949bf491d3-IMG_20181126_202333.png', '0', '2019-01-12 01:58:23', '2019-01-12 01:58:23');
INSERT INTO `products_images` VALUES ('2', '2', '5c3ce4877c66e-IMG_20181126_202333.png', '1', '2019-01-14 19:35:35', '2019-01-14 19:35:35');
INSERT INTO `products_images` VALUES ('3', '3', '5c3ce4fb9dab1-IMG_20181126_195549.png', '1', '2019-01-14 19:37:31', '2019-01-14 19:37:31');
INSERT INTO `products_images` VALUES ('4', '4', '5c3ce52955090-IMG_20181126_195647.png', '1', '2019-01-14 19:38:17', '2019-01-14 19:38:17');
INSERT INTO `products_images` VALUES ('5', '5', '5c3ce85c63d4d-IMG_20181126_195627.png', '1', '2019-01-14 19:52:00', '2019-01-14 19:52:00');
INSERT INTO `products_images` VALUES ('6', '6', '5c3d35ee71ca7-IMG_20181126_201711.png', '1', '2019-01-15 01:22:55', '2019-01-15 01:22:55');
INSERT INTO `products_images` VALUES ('7', '7', '5c3d36402451a-IMG_20181126_202155.png', '1', '2019-01-15 01:24:16', '2019-01-15 01:24:16');
INSERT INTO `products_images` VALUES ('8', '8', '5c3d366d699e2-IMG_20181126_201751.png', '1', '2019-01-15 01:25:01', '2019-01-15 01:25:01');
INSERT INTO `products_images` VALUES ('9', '9', '5c3d36c02ff8d-IMG_20181126_195549.png', '1', '2019-01-15 01:26:24', '2019-01-15 01:26:24');
INSERT INTO `products_images` VALUES ('10', '10', '5c3d36ef135f2-IMG_20181126_195659.png', '1', '2019-01-15 01:27:11', '2019-01-15 01:27:11');

-- ----------------------------
-- Table structure for `sugerencias`
-- ----------------------------
DROP TABLE IF EXISTS `sugerencias`;
CREATE TABLE `sugerencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sugerencias_user_id_foreign` (`user_id`),
  CONSTRAINT `sugerencias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of sugerencias
-- ----------------------------
INSERT INTO `sugerencias` VALUES ('1', 'hola feliz tarde Quiero reportar la entrega de mi producto cuya orden fue generada el dia 26/12/2018 y aun no se me da ningúna respuesta', '3', '2019-01-20 21:31:23', '2019-01-20 21:31:23');
INSERT INTO `sugerencias` VALUES ('2', 'Y por consiguiente Quería Saber Cuanto debemos esperar la respuesta de parte del personal.', '3', '2019-01-20 21:44:44', '2019-01-20 21:44:44');

-- ----------------------------
-- Table structure for `tipo_pagos`
-- ----------------------------
DROP TABLE IF EXISTS `tipo_pagos`;
CREATE TABLE `tipo_pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of tipo_pagos
-- ----------------------------
INSERT INTO `tipo_pagos` VALUES ('1', 'Deposito Bancario', '1', null, null);
INSERT INTO `tipo_pagos` VALUES ('2', 'Transferencia Electrónica', '1', null, null);
INSERT INTO `tipo_pagos` VALUES ('3', 'Efectivo', '1', null, null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ced_user` int(15) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'KARLA KASTILLO', '21464791', 'yendersonhernandez@gmail.com', null, '$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS', '(0412) 767-2042', 'SECTOR SAN JOAQUINDE TURMERO CRUCE CON LA PARROQUIA PEDRO AREVALO APONTE SECTOR BRISAS DEL LAGO. PUNTO DE REFERENCIA REFINERIA LA ANDORR PRECIOS FULL Y AL DETALL', '2', null, 'VZ7BrdbnZH2w5RWX3VulhjflaK5D4ctCtS53fnlTEVeVMnPeLgPLtQNQOXEx', '2019-01-12 01:27:47', '2019-01-31 01:00:35');
INSERT INTO `users` VALUES ('2', 'MARIA QUINTERO', null, 'mquintero@gmail.com', null, '$2y$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS', '(0412) 767-2042', 'URB LA ARBOLEDA CRUCE CON CACIQUE TEREPAIMA', '1', null, null, '2019-01-15 02:22:05', '2019-01-15 02:22:05');
INSERT INTO `users` VALUES ('3', 'MARIA QUINTERO', null, 'mq@gmail.com', null, '$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS', '(0414) 526-3256', 'AV URDANETA CRUCECON RONDON SEGÚN AV PRINCIPAL DE BELLO CAMPO', '1', '1', '6jmQ1y3MSYzY4oDg8jtyaB4CD41HHQtlqXds8xKpE5L183R3dyOs2PsKruzA', '2019-01-18 02:26:57', '2019-01-24 04:01:59');
INSERT INTO `users` VALUES ('4', 'JUAN GUAIDÓ', null, 'alx.morales@outlook.com', null, '$2y$10$YUjssMXDObb5T0K1d91K6ejLgexy.LWSpu7S9/H4tgBwMHZxzL.2u', '(0414) 526-3256', 'TURMERO', '1', null, null, '2019-01-24 16:49:02', '2019-01-24 16:49:02');
INSERT INTO `users` VALUES ('5', 'MARIA QUINTERO', '20878654', 'MARIA_23@GMAIL.COM', null, '$2y$10$eHApmHNjCc4X1WQTN39oQO1kKDA2XHzSg8cXvqP8zcaM4fv4fi8XK', '(0424) 568-9978', 'TURMERO', '1', null, null, '2019-01-29 01:02:24', '2019-01-29 01:02:24');
INSERT INTO `users` VALUES ('6', 'LISSETH ROJAS', null, 'glissethrojas_10@gmail.com', null, '$2y$10$5IFFnQPX2hwm0tZf6tJQo.3lNa.bVzf/2YU/k7Szk.mXA2ySS4lGK', '(0414) 345-6798', 'TURMERO', '1', null, 'KoW6o6LKGnMtfJBej7q8AdnDLdvUMm9nmFijBmeG4q8lXOUHLoHoxgvtxC5p', '2019-01-31 02:21:21', '2019-01-31 02:21:21');
