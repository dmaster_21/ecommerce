DROP TABLE IF EXISTS  banco_pago;

CREATE TABLE `banco_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_banco` varchar(250) DEFAULT NULL,
  `cuenta_filial` int(20) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS  bancos;

CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuenta` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO bancos VALUES("1","BANCO DEL CARIBE","1145236665","9","","2019-01-21 22:57:11");
INSERT INTO bancos VALUES("2","BANCO DE VENEZUELA","523362222","1","","2019-03-17 16:38:12");
INSERT INTO bancos VALUES("5","BANCO BANESCO BANCO UNIVERSAL","","1","2019-01-21 04:56:06","2019-01-21 04:56:06");
INSERT INTO bancos VALUES("6","BANCO FONDO COMÚN","","1","2019-01-21 04:56:58","2019-01-21 04:56:58");
INSERT INTO bancos VALUES("7","BANCO PROVINCIAL","","1","2019-01-21 04:57:17","2019-01-21 04:57:17");
INSERT INTO bancos VALUES("8","BANCO MERCANTIL","","9","2019-01-21 04:57:38","2019-01-21 23:16:32");
INSERT INTO bancos VALUES("9","Banco Nacional de Credito","","1","2019-02-12 23:21:36","2019-02-12 23:21:36");

DROP TABLE IF EXISTS  categories;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_user_id_foreign` (`user_id`),
  CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO categories VALUES("1","Maquillaje","1","1","2019-01-12 01:58:49","2019-02-01 03:29:11");
INSERT INTO categories VALUES("2","Cosmeticos","9","1","2019-01-12 01:58:58","2019-03-17 15:43:15");
INSERT INTO categories VALUES("4","Vestidos","1","1","2019-01-30 05:34:37","2019-02-01 03:29:05");
INSERT INTO categories VALUES("5","Calzado","1","1","2019-01-30 06:05:46","2019-03-17 15:42:57");
INSERT INTO categories VALUES("8","Bodys","1","1","2019-02-12 23:18:57","2019-02-12 23:19:08");
INSERT INTO categories VALUES("9","Blusas","1","1","2019-03-17 15:42:37","2019-03-17 15:42:37");
INSERT INTO categories VALUES("10","Otros","1","1","2019-03-17 15:42:48","2019-03-17 15:42:48");

DROP TABLE IF EXISTS  compras_cart_detalles;

CREATE TABLE `compras_cart_detalles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `compras_carts_id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_cart_detalles_compras_carts_id_foreign` (`compras_carts_id`),
  KEY `compras_cart_detalles_products_id_foreign` (`products_id`),
  CONSTRAINT `compras_cart_detalles_compras_carts_id_foreign` FOREIGN KEY (`compras_carts_id`) REFERENCES `compras_carts` (`id`),
  CONSTRAINT `compras_cart_detalles_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO compras_cart_detalles VALUES("1","1","1","2","3560.30","","2019-01-15 02:52:20","2019-01-15 02:52:20");
INSERT INTO compras_cart_detalles VALUES("2","2","2","1","5630.00","","2019-01-18 02:57:40","2019-01-18 02:57:40");
INSERT INTO compras_cart_detalles VALUES("3","2","3","1","600.00","","2019-01-18 02:57:40","2019-01-18 02:57:40");
INSERT INTO compras_cart_detalles VALUES("4","10","2","2","5630.00","","2019-01-18 04:32:15","2019-01-18 04:32:15");
INSERT INTO compras_cart_detalles VALUES("5","10","10","2","3000.00","","2019-01-18 04:32:15","2019-01-18 04:32:15");
INSERT INTO compras_cart_detalles VALUES("6","11","3","2","600.00","","2019-01-20 20:32:27","2019-01-20 20:32:27");
INSERT INTO compras_cart_detalles VALUES("7","11","2","2","5630.00","","2019-01-20 20:32:27","2019-01-20 20:32:27");
INSERT INTO compras_cart_detalles VALUES("8","12","3","2","600.00","16","2019-01-23 16:02:42","2019-01-23 16:02:42");
INSERT INTO compras_cart_detalles VALUES("9","13","4","1","1500.00","16","2019-01-23 17:05:21","2019-01-23 17:05:21");
INSERT INTO compras_cart_detalles VALUES("10","14","9","1","5300.00","16","2019-01-23 17:09:19","2019-01-23 17:09:19");
INSERT INTO compras_cart_detalles VALUES("11","24","1","1","879.00","16","2019-01-28 04:00:46","2019-01-28 04:00:46");
INSERT INTO compras_cart_detalles VALUES("12","24","10","1","879.00","16","2019-01-28 04:00:46","2019-01-28 04:00:46");
INSERT INTO compras_cart_detalles VALUES("13","24","7","1","879.00","16","2019-01-28 04:00:46","2019-01-28 04:00:46");
INSERT INTO compras_cart_detalles VALUES("14","25","1","1","879.00","16","2019-01-28 04:01:36","2019-01-28 04:01:36");
INSERT INTO compras_cart_detalles VALUES("15","25","1","1","879.00","16","2019-01-28 04:01:37","2019-01-28 04:01:37");
INSERT INTO compras_cart_detalles VALUES("16","25","1","1","879.00","16","2019-01-28 04:01:37","2019-01-28 04:01:37");
INSERT INTO compras_cart_detalles VALUES("17","26","1","1","879.00","16","2019-01-28 04:02:04","2019-01-28 04:02:04");
INSERT INTO compras_cart_detalles VALUES("18","26","1","1","879.00","16","2019-01-28 04:02:04","2019-01-28 04:02:04");
INSERT INTO compras_cart_detalles VALUES("19","27","1","1","879.00","16","2019-01-28 04:02:23","2019-01-28 04:02:23");
INSERT INTO compras_cart_detalles VALUES("20","27","1","1","879.00","16","2019-01-28 04:02:23","2019-01-28 04:02:23");
INSERT INTO compras_cart_detalles VALUES("21","27","1","1","879.00","16","2019-01-28 04:02:23","2019-01-28 04:02:23");
INSERT INTO compras_cart_detalles VALUES("22","39","1","1","879.00","16","2019-01-28 04:07:38","2019-01-28 04:07:38");
INSERT INTO compras_cart_detalles VALUES("23","39","10","1","879.00","16","2019-01-28 04:07:38","2019-01-28 04:07:38");
INSERT INTO compras_cart_detalles VALUES("24","39","7","1","879.00","16","2019-01-28 04:07:38","2019-01-28 04:07:38");
INSERT INTO compras_cart_detalles VALUES("26","27","1","1","879.00","16","2019-01-28 04:08:44","2019-01-28 04:08:44");
INSERT INTO compras_cart_detalles VALUES("27","27","10","1","879.00","16","2019-01-28 04:08:44","2019-01-28 04:08:44");
INSERT INTO compras_cart_detalles VALUES("28","27","7","1","879.00","16","2019-01-28 04:08:44","2019-01-28 04:08:44");
INSERT INTO compras_cart_detalles VALUES("29","27","1","1","879.00","16","2019-01-28 04:09:05","2019-01-28 04:09:05");
INSERT INTO compras_cart_detalles VALUES("30","27","10","1","879.00","16","2019-01-28 04:09:05","2019-01-28 04:09:05");
INSERT INTO compras_cart_detalles VALUES("31","27","7","1","879.00","16","2019-01-28 04:09:05","2019-01-28 04:09:05");
INSERT INTO compras_cart_detalles VALUES("32","60","1","1","3560.30","16","2019-01-28 23:23:51","2019-01-28 23:23:51");
INSERT INTO compras_cart_detalles VALUES("33","60","2","1","5630.00","16","2019-01-28 23:23:52","2019-01-28 23:23:52");
INSERT INTO compras_cart_detalles VALUES("34","60","3","1","600.00","16","2019-01-28 23:23:52","2019-01-28 23:23:52");
INSERT INTO compras_cart_detalles VALUES("35","61","1","1","3560.30","16","2019-01-29 01:32:24","2019-01-29 01:32:24");
INSERT INTO compras_cart_detalles VALUES("36","61","2","1","5630.00","16","2019-01-29 01:32:24","2019-01-29 01:32:24");
INSERT INTO compras_cart_detalles VALUES("37","61","3","1","600.00","16","2019-01-29 01:32:24","2019-01-29 01:32:24");
INSERT INTO compras_cart_detalles VALUES("38","62","1","1","3560.30","16","2019-01-29 02:55:45","2019-01-29 02:55:45");
INSERT INTO compras_cart_detalles VALUES("39","62","2","1","5630.00","16","2019-01-29 02:55:45","2019-01-29 02:55:45");
INSERT INTO compras_cart_detalles VALUES("40","62","3","1","600.00","16","2019-01-29 02:55:45","2019-01-29 02:55:45");
INSERT INTO compras_cart_detalles VALUES("41","63","1","1","3560.30","16","2019-01-29 02:57:26","2019-01-29 02:57:26");
INSERT INTO compras_cart_detalles VALUES("42","65","3","1","600.00","16","2019-01-29 02:58:24","2019-01-29 02:58:24");
INSERT INTO compras_cart_detalles VALUES("43","66","1","1","3560.30","16","2019-01-29 02:59:00","2019-01-29 02:59:00");
INSERT INTO compras_cart_detalles VALUES("44","66","3","1","600.00","16","2019-01-29 02:59:01","2019-01-29 02:59:01");
INSERT INTO compras_cart_detalles VALUES("45","67","6","1","3500.00","16","2019-01-29 02:59:41","2019-01-29 02:59:41");
INSERT INTO compras_cart_detalles VALUES("46","67","5","1","5600.00","16","2019-01-29 02:59:41","2019-01-29 02:59:41");
INSERT INTO compras_cart_detalles VALUES("47","67","4","1","1500.00","16","2019-01-29 02:59:41","2019-01-29 02:59:41");
INSERT INTO compras_cart_detalles VALUES("48","68","4","1","1500.00","16","2019-01-31 02:47:54","2019-01-31 02:47:54");
INSERT INTO compras_cart_detalles VALUES("49","69","9","1","5300.00","16","2019-02-01 02:56:20","2019-02-01 02:56:20");
INSERT INTO compras_cart_detalles VALUES("50","69","3","1","600.00","16","2019-02-01 02:56:20","2019-02-01 02:56:20");
INSERT INTO compras_cart_detalles VALUES("51","70","5","1","5600.00","16","2019-02-01 03:21:12","2019-02-01 03:21:12");
INSERT INTO compras_cart_detalles VALUES("52","71","5","1","5600.00","16","2019-02-01 04:02:17","2019-02-01 04:02:17");
INSERT INTO compras_cart_detalles VALUES("53","73","5","1","5600.00","16","2019-02-01 04:07:17","2019-02-01 04:07:17");
INSERT INTO compras_cart_detalles VALUES("54","75","5","1","5600.00","16","2019-02-01 04:09:13","2019-02-01 04:09:13");
INSERT INTO compras_cart_detalles VALUES("55","76","5","1","3565.30","16","2019-02-01 04:10:05","2019-02-01 04:10:05");
INSERT INTO compras_cart_detalles VALUES("56","77","5","1","5600.00","16","2019-02-01 04:11:00","2019-02-01 04:11:00");
INSERT INTO compras_cart_detalles VALUES("57","77","6","1","3500.00","16","2019-02-01 04:11:00","2019-02-01 04:11:00");
INSERT INTO compras_cart_detalles VALUES("58","78","1","1","3565.30","16","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO compras_cart_detalles VALUES("59","78","4","1","1500.00","16","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO compras_cart_detalles VALUES("60","79","4","2","1500.00","16","2019-02-01 22:17:57","2019-02-01 22:17:57");
INSERT INTO compras_cart_detalles VALUES("61","79","1","1","3565.30","16","2019-02-01 22:17:57","2019-02-01 22:17:57");
INSERT INTO compras_cart_detalles VALUES("62","80","6","1","3500.00","16","2019-02-01 22:30:38","2019-02-01 22:30:38");
INSERT INTO compras_cart_detalles VALUES("63","81","10","3","3000.00","16","2019-02-12 23:13:12","2019-02-12 23:13:12");
INSERT INTO compras_cart_detalles VALUES("64","81","7","1","78200.00","16","2019-02-12 23:13:12","2019-02-12 23:13:12");
INSERT INTO compras_cart_detalles VALUES("65","82","5","1","5600.00","16","2019-02-12 23:46:08","2019-02-12 23:46:08");
INSERT INTO compras_cart_detalles VALUES("66","82","6","1","3500.00","16","2019-02-12 23:46:08","2019-02-12 23:46:08");
INSERT INTO compras_cart_detalles VALUES("67","83","3","1","650.00","16","2019-02-14 23:48:29","2019-02-14 23:48:29");
INSERT INTO compras_cart_detalles VALUES("68","83","4","1","1500.00","16","2019-02-14 23:48:29","2019-02-14 23:48:29");
INSERT INTO compras_cart_detalles VALUES("69","84","48","1","10000.00","16","2019-03-17 16:49:51","2019-03-17 16:49:51");
INSERT INTO compras_cart_detalles VALUES("70","85","22","1","15000.00","16","2019-03-17 16:51:28","2019-03-17 16:51:28");
INSERT INTO compras_cart_detalles VALUES("71","86","1","2","3565.30","16","2019-03-19 22:01:01","2019-03-19 22:01:01");
INSERT INTO compras_cart_detalles VALUES("72","86","7","1","78200.00","16","2019-03-19 22:01:01","2019-03-19 22:01:01");
INSERT INTO compras_cart_detalles VALUES("73","87","11","1","60000.00","16","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO compras_cart_detalles VALUES("74","87","13","1","20000.00","16","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO compras_cart_detalles VALUES("75","88","26","1","12500.00","16","2019-03-22 13:44:26","2019-03-22 13:44:26");
INSERT INTO compras_cart_detalles VALUES("76","89","41","1","17500.00","16","2019-03-22 13:44:45","2019-03-22 13:44:45");
INSERT INTO compras_cart_detalles VALUES("77","89","62","1","18200.00","16","2019-03-22 13:44:45","2019-03-22 13:44:45");
INSERT INTO compras_cart_detalles VALUES("78","90","37","1","3500.00","16","2019-03-22 14:09:17","2019-03-22 14:09:17");
INSERT INTO compras_cart_detalles VALUES("79","90","34","1","33450.00","16","2019-03-22 14:09:17","2019-03-22 14:09:17");
INSERT INTO compras_cart_detalles VALUES("80","91","32","1","12500.00","16","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO compras_cart_detalles VALUES("81","91","64","1","16500.00","16","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO compras_cart_detalles VALUES("82","92","50","2","19900.00","16","2019-03-22 15:26:05","2019-03-22 15:26:05");
INSERT INTO compras_cart_detalles VALUES("83","93","13","1","20000.00","16","2019-03-22 16:33:41","2019-03-22 16:33:41");
INSERT INTO compras_cart_detalles VALUES("84","93","27","1","12500.00","16","2019-03-22 16:33:41","2019-03-22 16:33:41");
INSERT INTO compras_cart_detalles VALUES("85","94","67","10","18500.00","16","2019-03-22 16:54:12","2019-03-22 16:54:12");
INSERT INTO compras_cart_detalles VALUES("86","94","42","1","17500.00","16","2019-03-22 16:54:12","2019-03-22 16:54:12");
INSERT INTO compras_cart_detalles VALUES("87","95","22","1","15000.00","16","2019-03-22 23:36:31","2019-03-22 23:36:31");
INSERT INTO compras_cart_detalles VALUES("88","96","14","1","21000.00","16","2019-03-22 23:51:44","2019-03-22 23:51:44");
INSERT INTO compras_cart_detalles VALUES("89","97","37","1","3500.00","16","2019-03-22 23:53:59","2019-03-22 23:53:59");
INSERT INTO compras_cart_detalles VALUES("90","98","13","1","20000.00","16","2019-03-29 13:03:47","2019-03-29 13:03:47");

DROP TABLE IF EXISTS  compras_carts;

CREATE TABLE `compras_carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nro_orden` int(11) NOT NULL,
  `annio_fiscal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nro_factura` int(11) NOT NULL,
  `nro_control` int(11) NOT NULL,
  `fecha_compra` timestamp NOT NULL,
  `monto_compra` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('1','9','3','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `envios_id` int(10) unsigned NOT NULL,
  `monto_envio` double(15,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `tipo_id` int(10) unsigned DEFAULT NULL,
  `ref_pago` int(11) DEFAULT NULL,
  `banco_id` int(10) unsigned DEFAULT NULL,
  `cajero` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plataforma` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_carts_user_id_foreign` (`user_id`),
  CONSTRAINT `compras_carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO compras_carts VALUES("1","1","2019","46","47","2019-01-15 02:52:19","7120.60","","2","3","7","","2019-01-20 20:19:35","2","985632","1","","","2019-01-15 02:52:19","2019-01-20 20:19:35");
INSERT INTO compras_carts VALUES("2","1","2019","46","47","2019-01-18 02:57:39","6230.00","","2","3","9","","2019-01-20 20:06:15","2","985632","2","","","2019-01-18 02:57:40","2019-01-20 20:06:15");
INSERT INTO compras_carts VALUES("10","2","2019","47","48","2019-01-18 04:32:15","17260.00","","2","9","7","","2019-01-20 20:04:09","1","3963","","","","2019-01-18 04:32:15","2019-02-12 23:18:38");
INSERT INTO compras_carts VALUES("11","3","2019","48","49","2019-01-20 20:32:27","12460.00","","3","9","6","","","","","1","","","2019-01-20 20:32:27","2019-01-31 03:22:43");
INSERT INTO compras_carts VALUES("12","4","2019","49","50","2019-01-23 16:02:42","1200.00","16","3","3","5","","2019-01-25 03:01:39","1","987656","5","","","2019-01-23 16:02:42","2019-01-26 21:32:00");
INSERT INTO compras_carts VALUES("13","5","2019","50","51","2019-01-23 17:05:21","1500.00","16","3","3","4","3450.00","2019-01-23 19:40:00","2","9098","5","","","2019-01-23 17:05:21","2019-01-26 21:29:12");
INSERT INTO compras_carts VALUES("14","6","2019","51","52","2019-01-23 17:09:18","5300.00","16","1","9","4","7800.00","","","","","","","2019-01-23 17:09:18","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("15","7","2019","52","53","2019-01-28 03:42:14","88210.30","16","1","9","5","3450.00","2019-01-28 03:42:14","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:42:14","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("16","8","2019","53","54","2019-01-28 03:50:47","88210.30","16","1","9","5","3450.00","2019-01-28 03:50:47","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:50:47","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("17","9","2019","54","55","2019-01-28 03:51:23","88210.30","16","1","9","5","3450.00","2019-01-28 03:51:23","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:51:23","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("18","10","2019","55","56","2019-01-28 03:52:05","88210.30","16","1","9","5","3450.00","2019-01-28 03:52:05","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:52:05","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("19","11","2019","56","57","2019-01-28 03:56:06","88210.30","16","1","9","5","3450.00","2019-01-28 03:56:06","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:56:06","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("20","12","2019","57","58","2019-01-28 03:57:02","88210.30","16","1","9","5","3450.00","2019-01-28 03:57:02","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:57:02","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("21","13","2019","58","59","2019-01-28 03:57:17","88210.30","16","1","9","5","3450.00","2019-01-28 03:57:17","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:57:17","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("22","14","2019","59","60","2019-01-28 03:58:45","88210.30","16","1","9","5","3450.00","2019-01-28 03:58:45","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:58:45","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("23","15","2019","60","61","2019-01-28 03:58:57","88210.30","16","1","9","5","3450.00","2019-01-28 03:58:57","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 03:58:57","2019-02-12 23:16:17");
INSERT INTO compras_carts VALUES("24","16","2019","61","62","2019-01-28 04:00:46","88210.30","16","1","9","5","3450.00","2019-01-28 04:00:46","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:00:46","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("25","17","2019","62","63","2019-01-28 04:01:36","88210.30","16","1","9","5","3450.00","2019-01-28 04:01:36","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:01:36","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("26","18","2019","63","64","2019-01-28 04:02:04","88210.30","16","1","9","5","3450.00","2019-01-28 04:02:04","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:02:04","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("27","19","2019","64","65","2019-01-28 04:02:23","88210.30","16","1","9","5","3450.00","2019-01-28 04:02:23","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:02:23","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("28","20","2019","65","66","2019-01-28 04:03:23","88210.30","16","1","9","5","3450.00","2019-01-28 04:03:23","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:03:23","2019-03-17 02:30:18");
INSERT INTO compras_carts VALUES("29","21","2019","66","67","2019-01-28 04:03:39","88210.30","16","1","9","5","3450.00","2019-01-28 04:03:39","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:03:39","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("30","22","2019","67","68","2019-01-28 04:03:53","88210.30","16","1","9","5","3450.00","2019-01-28 04:03:53","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:03:53","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("31","23","2019","68","69","2019-01-28 04:04:16","88210.30","16","1","9","5","3450.00","2019-01-28 04:04:16","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:04:16","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("32","24","2019","69","70","2019-01-28 04:04:47","88210.30","16","1","9","5","3450.00","2019-01-28 04:04:47","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:04:47","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("33","25","2019","70","71","2019-01-28 04:05:45","88210.30","16","1","9","5","3450.00","2019-01-28 04:05:45","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:05:45","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("34","26","2019","71","72","2019-01-28 04:06:00","88210.30","16","1","9","5","3450.00","2019-01-28 04:06:00","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:06:00","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("35","27","2019","72","73","2019-01-28 04:06:15","88210.30","16","1","9","5","3450.00","2019-01-28 04:06:15","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:06:15","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("36","28","2019","73","74","2019-01-28 04:06:37","88210.30","16","1","9","5","3450.00","2019-01-28 04:06:37","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:06:37","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("37","29","2019","74","75","2019-01-28 04:07:02","88210.30","16","1","9","5","3450.00","2019-01-28 04:07:02","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:07:02","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("38","30","2019","75","76","2019-01-28 04:07:18","88210.30","16","1","9","5","3450.00","2019-01-28 04:07:18","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:07:18","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("39","31","2019","76","77","2019-01-28 04:07:38","88210.30","16","1","9","5","3450.00","2019-01-28 04:07:38","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:07:38","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("40","32","2019","77","78","2019-01-28 04:08:15","88210.30","16","1","9","5","3450.00","2019-01-28 04:08:15","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:08:15","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("41","33","2019","78","79","2019-01-28 04:08:44","88210.30","16","1","9","5","3450.00","2019-01-28 04:08:44","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:08:44","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("42","34","2019","79","80","2019-01-28 04:09:05","88210.30","16","1","9","5","3450.00","2019-01-28 04:09:05","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:09:05","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("43","35","2019","80","81","2019-01-28 04:09:33","88210.30","16","1","9","5","3450.00","2019-01-28 04:09:33","2","9087989","6","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:09:33","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("44","36","2019","81","82","2019-01-28 04:34:24","13240.30","16","1","9","5","3450.00","2019-01-28 04:34:24","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:34:24","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("45","37","2019","82","83","2019-01-28 04:35:37","13240.30","16","1","9","5","3450.00","2019-01-28 04:35:37","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:35:37","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("46","38","2019","83","84","2019-01-28 04:36:29","13240.30","16","1","9","5","3450.00","2019-01-28 04:36:29","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:36:29","2019-03-17 02:30:19");
INSERT INTO compras_carts VALUES("47","39","2019","84","85","2019-01-28 04:36:52","13240.30","16","1","9","5","3450.00","2019-01-28 04:36:52","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:36:52","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("48","40","2019","85","86","2019-01-28 04:37:35","13240.30","16","1","9","5","3450.00","2019-01-28 04:37:35","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:37:35","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("49","41","2019","86","87","2019-01-28 04:38:25","13240.30","16","1","9","5","3450.00","2019-01-28 04:38:25","3","456","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 04:38:25","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("50","42","2019","87","88","2019-01-28 22:27:50","17640.30","16","1","9","5","3450.00","2019-01-28 22:27:50","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 22:27:50","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("51","43","2019","88","89","2019-01-28 22:57:54","17991.10","16","1","9","6","6700.80","2019-01-28 22:57:54","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 22:57:54","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("52","44","2019","89","90","2019-01-28 22:59:48","17991.10","16","1","9","6","6700.80","2019-01-28 22:59:48","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 22:59:48","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("53","45","2019","90","91","2019-01-28 23:02:11","17991.10","16","1","9","6","6700.80","2019-01-28 23:02:11","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:02:11","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("54","46","2019","91","92","2019-01-28 23:03:30","17991.10","16","1","9","6","6700.80","2019-01-28 23:03:30","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:03:30","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("55","47","2019","92","93","2019-01-28 23:04:30","17991.10","16","1","9","6","6700.80","2019-01-28 23:04:30","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:04:30","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("56","48","2019","93","94","2019-01-28 23:04:49","17991.10","16","1","9","6","6700.80","2019-01-28 23:04:49","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:04:49","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("57","49","2019","94","95","2019-01-28 23:13:35","17991.10","16","1","9","6","6700.80","2019-01-28 23:13:35","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:13:35","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("58","50","2019","95","96","2019-01-28 23:23:14","17991.10","16","1","9","6","6700.80","2019-01-28 23:23:14","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:23:14","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("59","51","2019","96","97","2019-01-28 23:23:28","17991.10","16","1","9","6","6700.80","2019-01-28 23:23:28","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:23:28","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("60","52","2019","97","98","2019-01-28 23:23:51","17991.10","16","1","9","6","6700.80","2019-01-28 23:23:51","1","9878","2","Yenderson Hernandez","TIENDA FISICA","2019-01-28 23:23:51","2019-03-17 02:30:20");
INSERT INTO compras_carts VALUES("61","53","2019","98","99","2019-01-29 01:32:24","9790.30","16","5","3","9","0.00","2019-01-29 01:32:24","1","9899","5","Yenderson Hernandez","TIENDA FISICA","2019-01-29 01:32:24","2019-01-29 01:32:24");
INSERT INTO compras_carts VALUES("62","54","2019","99","100","2019-01-29 02:55:45","14790.30","16","1","3","5","3450.00","2019-01-29 02:55:45","1","234555","2","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:55:45","2019-01-29 02:55:45");
INSERT INTO compras_carts VALUES("63","55","2019","100","101","2019-01-29 02:57:26","11360.30","16","5","3","4","7800.00","2019-01-29 02:57:26","1","987899","2","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:57:26","2019-01-29 02:57:26");
INSERT INTO compras_carts VALUES("64","56","2019","101","102","2019-01-29 02:58:10","11360.30","16","5","3","4","7800.00","2019-01-29 02:58:10","1","987899","2","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:58:10","2019-01-29 02:58:10");
INSERT INTO compras_carts VALUES("65","57","2019","102","103","2019-01-29 02:58:24","11960.30","16","5","3","4","7800.00","2019-01-29 02:58:24","1","987899","2","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:58:24","2019-01-29 02:58:24");
INSERT INTO compras_carts VALUES("66","58","2019","103","104","2019-01-29 02:59:00","11960.30","16","5","3","4","7800.00","2019-01-29 02:59:00","1","987899","2","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:59:00","2019-01-29 02:59:00");
INSERT INTO compras_carts VALUES("67","59","2019","104","105","2019-01-29 02:59:41","99600.00","16","5","3","4","7800.00","2019-01-29 02:59:41","1","987899","5","Yenderson Hernandez","TIENDA FISICA","2019-01-29 02:59:41","2019-01-29 02:59:41");
INSERT INTO compras_carts VALUES("68","60","2019","105","106","2019-01-31 02:47:53","1500.00","16","3","3","5","3450.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-01-31 02:47:54","2019-01-31 02:47:54");
INSERT INTO compras_carts VALUES("69","61","2019","106","107","2019-02-01 02:56:20","12600.80","16","1","3","6","6700.80","2019-02-01 02:56:20","1","985632","2","KARLA KASTILLO RONDON","TIENDA FISICA","2019-02-01 02:56:20","2019-02-01 02:56:20");
INSERT INTO compras_carts VALUES("70","62","2019","107","108","2019-02-01 03:21:12","5600.00","16","9","3","9","0.00","2019-02-01 03:21:12","3","123544546","6","Luis Salazar","TIENDA FISICA","2019-02-01 03:21:12","2019-02-01 03:21:12");
INSERT INTO compras_carts VALUES("71","63","2019","108","109","2019-02-01 04:02:16","5600.00","16","10","3","9","0.00","2019-02-01 04:02:16","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:02:16","2019-02-01 04:02:16");
INSERT INTO compras_carts VALUES("72","64","2019","109","110","2019-02-01 04:06:23","5600.00","16","10","3","9","0.00","2019-02-01 04:06:23","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:06:23","2019-02-01 04:06:23");
INSERT INTO compras_carts VALUES("73","65","2019","110","111","2019-02-01 04:07:17","5600.00","16","10","3","9","0.00","2019-02-01 04:07:17","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:07:17","2019-02-01 04:07:17");
INSERT INTO compras_carts VALUES("74","66","2019","111","112","2019-02-01 04:08:04","5600.00","16","10","3","9","0.00","2019-02-01 04:08:04","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:08:04","2019-02-01 04:08:04");
INSERT INTO compras_carts VALUES("75","67","2019","112","113","2019-02-01 04:09:12","5600.00","16","10","3","9","0.00","2019-02-01 04:09:12","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:09:12","2019-02-01 04:09:12");
INSERT INTO compras_carts VALUES("76","68","2019","113","114","2019-02-01 04:10:04","5600.00","16","10","3","9","0.00","2019-02-01 04:10:04","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:10:04","2019-02-01 04:10:04");
INSERT INTO compras_carts VALUES("77","69","2019","114","115","2019-02-01 04:11:00","16900.00","16","10","3","4","7800.00","2019-02-01 04:11:00","3","237847489","7","Luis Salazar","TIENDA FISICA","2019-02-01 04:11:00","2019-02-01 04:11:00");
INSERT INTO compras_carts VALUES("78","70","2019","115","116","2019-02-01 22:16:25","5065.30","16","11","3","9","0.00","2019-02-01 22:16:25","2","124356","5","KARLA KASTILLO RONDON","TIENDA FISICA","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO compras_carts VALUES("79","71","2019","116","117","2019-02-01 22:17:57","6565.30","16","12","3","9","0.00","2019-02-01 22:17:57","2","6736363","5","KARLA KASTILLO RONDON","TIENDA FISICA","2019-02-01 22:17:57","2019-02-01 22:17:57");
INSERT INTO compras_carts VALUES("80","72","2019","117","118","2019-02-01 22:30:38","3500.00","16","13","3","4","7800.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-01 22:30:38","2019-02-01 22:30:38");
INSERT INTO compras_carts VALUES("81","73","2019","118","119","2019-02-12 23:13:12","87200.00","16","14","3","7","8000.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-12 23:13:12","2019-02-12 23:13:12");
INSERT INTO compras_carts VALUES("82","74","2019","119","120","2019-02-12 23:46:08","9100.00","16","16","3","9","0.00","2019-02-12 23:46:08","2","363632362","5","Luis Salazar","TIENDA FISICA","2019-02-12 23:46:08","2019-02-12 23:46:08");
INSERT INTO compras_carts VALUES("83","75","2019","120","121","2019-02-14 23:48:29","13450.00","16","9","3","4","7800.00","2019-02-14 23:48:29","1","26513657","2","KARLA KASTILLO RONDON","TIENDA FISICA","2019-02-14 23:48:29","2019-02-14 23:48:29");
INSERT INTO compras_carts VALUES("84","76","2019","121","122","2019-03-17 16:49:51","10000.00","16","18","3","4","25000.00","2019-03-17 16:52:10","1","124051","5","CLIENTE","PLATAFORMA EN LINEA","2019-03-17 16:49:51","2019-03-17 16:52:51");
INSERT INTO compras_carts VALUES("85","77","2019","122","123","2019-03-17 16:51:28","15000.00","16","18","9","7","26000.00","2019-03-17 16:52:23","2","448830","2","CLIENTE","PLATAFORMA EN LINEA","2019-03-17 16:51:28","2019-03-22 13:42:57");
INSERT INTO compras_carts VALUES("86","78","2019","123","124","2019-03-19 22:01:01","131930.60","16","10","3","7","26000.00","2019-03-19 22:01:01","2","45643","5","KARLA KASTILLO RONDON","TIENDA FISICA","2019-03-19 22:01:01","2019-03-19 22:01:01");
INSERT INTO compras_carts VALUES("87","79","2019","124","125","2019-03-22 13:33:51","80000.00","16","25","3","9","0.00","2019-03-22 13:33:51","2","35367474","5","Yenderson Hernandez","TIENDA FISICA","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO compras_carts VALUES("88","80","2019","125","126","2019-03-22 13:44:26","12500.00","16","25","3","9","0.00","2019-03-22 13:44:26","1","12345678","5","Yenderson Hernandez","TIENDA FISICA","2019-03-22 13:44:26","2019-03-22 13:44:26");
INSERT INTO compras_carts VALUES("89","81","2019","126","127","2019-03-22 13:44:45","35700.00","16","25","3","9","0.00","2019-03-22 13:44:45","1","12345678","5","Yenderson Hernandez","TIENDA FISICA","2019-03-22 13:44:45","2019-03-22 13:44:45");
INSERT INTO compras_carts VALUES("90","82","2019","127","128","2019-03-22 14:09:17","61950.00","16","26","3","9","0.00","2019-03-22 14:09:17","2","1258963","5","Luis Salazar","TIENDA FISICA","2019-03-22 14:09:17","2019-03-22 14:09:17");
INSERT INTO compras_carts VALUES("91","83","2019","128","129","2019-03-22 14:37:31","59000.00","16","27","3","9","0.00","2019-03-22 14:37:31","1","12358922","2","Emma Rosales","TIENDA FISICA","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO compras_carts VALUES("92","84","2019","129","130","2019-03-22 15:26:05","39800.00","16","18","3","7","26000.00","2019-03-22 15:26:47","2","12345","5","CLIENTE","PLATAFORMA EN LINEA","2019-03-22 15:26:05","2019-03-22 15:28:41");
INSERT INTO compras_carts VALUES("93","85","2019","130","131","2019-03-22 16:33:40","32500.00","16","28","3","9","0.00","2019-03-22 16:34:41","2","252226","5","CLIENTE","PLATAFORMA EN LINEA","2019-03-22 16:33:40","2019-03-22 16:41:30");
INSERT INTO compras_carts VALUES("94","86","2019","131","132","2019-03-22 16:54:12","202500.00","16","29","3","9","0.00","2019-03-22 16:54:12","2","123456","5","Emma Rosales","TIENDA FISICA","2019-03-22 16:54:12","2019-03-22 16:54:12");
INSERT INTO compras_carts VALUES("95","87","2019","132","133","2019-03-22 23:36:30","15000.00","16","18","3","7","26000.00","2019-03-22 23:54:13","1","22455","2","CLIENTE","PLATAFORMA EN LINEA","2019-03-22 23:36:31","2019-03-30 17:01:41");
INSERT INTO compras_carts VALUES("96","88","2019","133","134","2019-03-22 23:51:43","21000.00","16","18","9","9","0.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-03-22 23:51:44","2019-03-29 13:02:37");
INSERT INTO compras_carts VALUES("97","89","2019","134","135","2019-03-22 23:53:59","3500.00","16","18","9","9","0.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-03-22 23:53:59","2019-03-29 13:02:37");
INSERT INTO compras_carts VALUES("98","90","2019","135","136","2019-03-29 13:03:45","20000.00","16","18","3","7","26000.00","2019-03-31 16:24:39","1","3652145","2","CLIENTE","PLATAFORMA EN LINEA","2019-03-29 13:03:45","2019-04-01 11:40:57");

DROP TABLE IF EXISTS  envios;

CREATE TABLE `envios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO envios VALUES("4","Mrw","25000.00","1","2019-01-22 01:29:29","2019-03-17 16:37:13");
INSERT INTO envios VALUES("5","Tealca","20000.00","9","2019-01-22 01:30:05","2019-03-22 16:45:53");
INSERT INTO envios VALUES("6","Liberty Express","6700.80","9","2019-01-22 01:30:29","2019-03-17 16:37:17");
INSERT INTO envios VALUES("7","Zoom","26000.00","1","2019-01-22 01:30:46","2019-03-17 16:37:34");
INSERT INTO envios VALUES("9","Retirar En Tienda Fisica","0.00","1","","2019-01-30 06:35:19");
INSERT INTO envios VALUES("10","tuenvio.com","17500.00","1","2019-02-12 23:20:02","2019-03-17 16:37:50");

DROP TABLE IF EXISTS  ivas;

CREATE TABLE `ivas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `porc` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO ivas VALUES("1","16","","");

DROP TABLE IF EXISTS  logs;

CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `accion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO logs VALUES("1","1","Creo Una Nueva Categoria LlamadaCosmeticos","2019-01-30 06:07:14","2019-01-30 06:07:14");
INSERT INTO logs VALUES("2","1","Creo Una Nueva Categoria Llamadavehiculos","2019-01-30 06:09:28","2019-01-30 06:09:28");
INSERT INTO logs VALUES("3","1","Eliminación de Categoriavehiculos","2019-01-30 06:32:37","2019-01-30 06:32:37");
INSERT INTO logs VALUES("4","1","Eliminación de Categoria vehiculos","2019-01-30 06:33:17","2019-01-30 06:33:17");
INSERT INTO logs VALUES("5","1","La Categoria Maquillaje Ha Sido Inhabilitada Satisfactoriamente","2019-01-30 06:33:26","2019-01-30 06:33:26");
INSERT INTO logs VALUES("6","1","Modificación de la Categoria Maquillaje","2019-01-30 06:33:59","2019-01-30 06:33:59");
INSERT INTO logs VALUES("7","1","Modificación de Agencia de Envio Retirar En Tienda Fisica","2019-01-30 06:34:48","2019-01-30 06:34:48");
INSERT INTO logs VALUES("8","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:27:38","2019-01-31 01:27:38");
INSERT INTO logs VALUES("9","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:30:00","2019-01-31 01:30:00");
INSERT INTO logs VALUES("10","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:30:35","2019-01-31 01:30:35");
INSERT INTO logs VALUES("11","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 02:11:21","2019-01-31 02:11:21");
INSERT INTO logs VALUES("12","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 02:22:18","2019-01-31 02:22:18");
INSERT INTO logs VALUES("13","3","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 60","2019-01-31 02:47:54","2019-01-31 02:47:54");
INSERT INTO logs VALUES("14","1","La Compra Número:3Ha Fue Eliminada","2019-01-31 03:22:43","2019-01-31 03:22:43");
INSERT INTO logs VALUES("15","1","Modificación del Usuario .KARLA KASTILLO RONDON","2019-02-01 02:49:32","2019-02-01 02:49:32");
INSERT INTO logs VALUES("16","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número61","2019-02-01 02:56:20","2019-02-01 02:56:20");
INSERT INTO logs VALUES("17","1","Creación del Usuario Luis Salazar","2019-02-01 03:13:38","2019-02-01 03:13:38");
INSERT INTO logs VALUES("18","1","Creación del Usuario Karla Castillo","2019-02-01 03:14:53","2019-02-01 03:14:53");
INSERT INTO logs VALUES("19","7","Creción de un Nuevo Cliente de Nombre:luisa perez","2019-02-01 03:21:11","2019-02-01 03:21:11");
INSERT INTO logs VALUES("20","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número62","2019-02-01 03:21:12","2019-02-01 03:21:12");
INSERT INTO logs VALUES("21","1","Eliminación de Categoria BISUTERIA","2019-02-01 03:28:40","2019-02-01 03:28:40");
INSERT INTO logs VALUES("22","1","Eliminación de Categoria Cosmeticos","2019-02-01 03:28:48","2019-02-01 03:28:48");
INSERT INTO logs VALUES("23","1","Modificación de la Categoria Vestidos","2019-02-01 03:29:05","2019-02-01 03:29:05");
INSERT INTO logs VALUES("24","1","La Categoria Maquillaje Ha Sido Habilitada Satisfactoriamente","2019-02-01 03:29:11","2019-02-01 03:29:11");
INSERT INTO logs VALUES("25","1","Creación de  Una Nueva Categoria Llamada Pants","2019-02-01 03:29:26","2019-02-01 03:29:26");
INSERT INTO logs VALUES("26","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:41:47","2019-02-01 03:41:47");
INSERT INTO logs VALUES("27","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:42:03","2019-02-01 03:42:03");
INSERT INTO logs VALUES("28","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:43:39","2019-02-01 03:43:39");
INSERT INTO logs VALUES("29","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:45:59","2019-02-01 03:45:59");
INSERT INTO logs VALUES("30","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:47:41","2019-02-01 03:47:41");
INSERT INTO logs VALUES("31","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:49:06","2019-02-01 03:49:06");
INSERT INTO logs VALUES("32","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:49:53","2019-02-01 03:49:53");
INSERT INTO logs VALUES("33","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:50:59","2019-02-01 03:50:59");
INSERT INTO logs VALUES("34","1","Modificación de Articulo|Producto Denominado:Tacones","2019-02-01 03:53:26","2019-02-01 03:53:26");
INSERT INTO logs VALUES("35","7","Creción de un Nuevo Cliente de Nombre:Luis m Salazar","2019-02-01 04:02:16","2019-02-01 04:02:16");
INSERT INTO logs VALUES("36","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número63","2019-02-01 04:02:17","2019-02-01 04:02:17");
INSERT INTO logs VALUES("37","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número64","2019-02-01 04:06:23","2019-02-01 04:06:23");
INSERT INTO logs VALUES("38","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número65","2019-02-01 04:07:17","2019-02-01 04:07:17");
INSERT INTO logs VALUES("39","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número66","2019-02-01 04:08:04","2019-02-01 04:08:04");
INSERT INTO logs VALUES("40","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número67","2019-02-01 04:09:13","2019-02-01 04:09:13");
INSERT INTO logs VALUES("41","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número68","2019-02-01 04:10:05","2019-02-01 04:10:05");
INSERT INTO logs VALUES("42","7","Procesamiento De Compra en Tienda Fisíca, Bajo El Número69","2019-02-01 04:11:00","2019-02-01 04:11:00");
INSERT INTO logs VALUES("43","7","Eliminacioón del Producto ESTUCHE MULTICOLOR INTEGRADO","2019-02-01 04:11:55","2019-02-01 04:11:55");
INSERT INTO logs VALUES("44","7","Modificación de Articulo|Producto Denominado:labial principal","2019-02-01 04:12:17","2019-02-01 04:12:17");
INSERT INTO logs VALUES("45","1","Creción de un Nuevo Cliente de Nombre:marilyn","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO logs VALUES("46","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número70","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO logs VALUES("47","1","Creción de un Nuevo Cliente de Nombre:marylin","2019-02-01 22:17:57","2019-02-01 22:17:57");
INSERT INTO logs VALUES("48","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número71","2019-02-01 22:17:57","2019-02-01 22:17:57");
INSERT INTO logs VALUES("49","1","Modificación de Articulo|Producto Denominado:ZAPATOS DEPORTVOS","2019-02-01 22:22:02","2019-02-01 22:22:02");
INSERT INTO logs VALUES("50","13","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 72","2019-02-01 22:30:38","2019-02-01 22:30:38");
INSERT INTO logs VALUES("51","14","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 73","2019-02-12 23:13:12","2019-02-12 23:13:12");
INSERT INTO logs VALUES("52","14","Modificación del Usuario .yuraima","2019-02-12 23:14:27","2019-02-12 23:14:27");
INSERT INTO logs VALUES("53","1","La Compra Número:15Ha Fue Eliminada","2019-02-12 23:16:17","2019-02-12 23:16:17");
INSERT INTO logs VALUES("54","1","La Compra Número:2Ha Fue Eliminada","2019-02-12 23:18:38","2019-02-12 23:18:38");
INSERT INTO logs VALUES("55","1","Creación de  Una Nueva Categoria Llamada 123","2019-02-12 23:18:57","2019-02-12 23:18:57");
INSERT INTO logs VALUES("56","1","Modificación de la Categoria Bodys","2019-02-12 23:19:08","2019-02-12 23:19:08");
INSERT INTO logs VALUES("57","1","La Categoria Pants Ha Sido Inhabilitada Satisfactoriamente","2019-02-12 23:19:19","2019-02-12 23:19:19");
INSERT INTO logs VALUES("58","1","Modificación de Agencia de Envio mrw","2019-02-12 23:19:39","2019-02-12 23:19:39");
INSERT INTO logs VALUES("59","1","Creacioón de Agencia de Envio Denóminada tuenvio.com","2019-02-12 23:20:02","2019-02-12 23:20:02");
INSERT INTO logs VALUES("60","1","Creación del BancoBanco Nacional de Credito","2019-02-12 23:21:36","2019-02-12 23:21:36");
INSERT INTO logs VALUES("61","1","Creación del BancoBanco Nacional de Credito","2019-02-12 23:21:36","2019-02-12 23:21:36");
INSERT INTO logs VALUES("62","1","Eliminacioón del Producto Tacones","2019-02-12 23:27:04","2019-02-12 23:27:04");
INSERT INTO logs VALUES("63","1","Eliminacioón del Producto labial principal","2019-02-12 23:27:09","2019-02-12 23:27:09");
INSERT INTO logs VALUES("64","1","Eliminacioón del Producto ZAPATOS DEPORTVOS","2019-02-12 23:27:13","2019-02-12 23:27:13");
INSERT INTO logs VALUES("65","1","Eliminacioón del Producto BODY ROJO","2019-02-12 23:27:20","2019-02-12 23:27:20");
INSERT INTO logs VALUES("66","1","Eliminacioón del Producto BODY TROPICAL","2019-02-12 23:27:24","2019-02-12 23:27:24");
INSERT INTO logs VALUES("67","1","Eliminacioón del Producto BERMUDA FALDON","2019-02-12 23:27:28","2019-02-12 23:27:28");
INSERT INTO logs VALUES("68","1","Eliminacioón del Producto BLUSAS ESCOTEX","2019-02-12 23:27:35","2019-02-12 23:27:35");
INSERT INTO logs VALUES("69","1","Eliminacioón del Producto PINTURA LABIAL","2019-02-12 23:27:39","2019-02-12 23:27:39");
INSERT INTO logs VALUES("70","1","Eliminacioón del Producto DELINEADOR PRINCIPAL","2019-02-12 23:27:43","2019-02-12 23:27:43");
INSERT INTO logs VALUES("71","1","Eliminacioón del Producto Tacones","2019-02-12 23:27:47","2019-02-12 23:27:47");
INSERT INTO logs VALUES("72","1","Ha Eliminado Al Usuario .Luis Salazar","2019-02-12 23:39:52","2019-02-12 23:39:52");
INSERT INTO logs VALUES("73","1","Creación del Usuario Luis Salazar","2019-02-12 23:41:55","2019-02-12 23:41:55");
INSERT INTO logs VALUES("74","15","Creción de un Nuevo Cliente de Nombre:juliana gomez","2019-02-12 23:46:08","2019-02-12 23:46:08");
INSERT INTO logs VALUES("75","15","Procesamiento De Compra en Tienda Fisíca, Bajo El Número74","2019-02-12 23:46:08","2019-02-12 23:46:08");
INSERT INTO logs VALUES("76","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número75","2019-02-14 23:48:29","2019-02-14 23:48:29");
INSERT INTO logs VALUES("77","1","Creación de Articulo|Producto Denominado:Paleta de sombras","2019-03-17 02:41:12","2019-03-17 02:41:12");
INSERT INTO logs VALUES("78","1","Creación de Articulo|Producto Denominado:Paleta de sombras","2019-03-17 02:42:23","2019-03-17 02:42:23");
INSERT INTO logs VALUES("79","1","Creación de Articulo|Producto Denominado:Labial","2019-03-17 02:44:59","2019-03-17 02:44:59");
INSERT INTO logs VALUES("80","1","Creación de Articulo|Producto Denominado:Labial","2019-03-17 02:45:15","2019-03-17 02:45:15");
INSERT INTO logs VALUES("81","1","Creación de Articulo|Producto Denominado:Labial Kyle","2019-03-17 02:46:06","2019-03-17 02:46:06");
INSERT INTO logs VALUES("82","1","Creación de Articulo|Producto Denominado:Zapatos Deportivos","2019-03-17 02:49:35","2019-03-17 02:49:35");
INSERT INTO logs VALUES("83","1","Creación de Articulo|Producto Denominado:Tacones de plataforma","2019-03-17 02:51:02","2019-03-17 02:51:02");
INSERT INTO logs VALUES("84","1","Creación de Articulo|Producto Denominado:Vestido Sport","2019-03-17 15:38:23","2019-03-17 15:38:23");
INSERT INTO logs VALUES("85","1","Creación de Articulo|Producto Denominado:Vestido Sport","2019-03-17 15:38:42","2019-03-17 15:38:42");
INSERT INTO logs VALUES("86","1","Creación de Articulo|Producto Denominado:Body Jazmin","2019-03-17 15:39:33","2019-03-17 15:39:33");
INSERT INTO logs VALUES("87","1","Creación de Articulo|Producto Denominado:Body Jazmin","2019-03-17 15:40:14","2019-03-17 15:40:14");
INSERT INTO logs VALUES("88","1","Creación de Articulo|Producto Denominado:Body Juliette","2019-03-17 15:41:54","2019-03-17 15:41:54");
INSERT INTO logs VALUES("89","1","Eliminación de Categoria Pants","2019-03-17 15:42:13","2019-03-17 15:42:13");
INSERT INTO logs VALUES("90","1","Creación de  Una Nueva Categoria Llamada Blusas","2019-03-17 15:42:37","2019-03-17 15:42:37");
INSERT INTO logs VALUES("91","1","Creación de  Una Nueva Categoria Llamada Otros","2019-03-17 15:42:48","2019-03-17 15:42:48");
INSERT INTO logs VALUES("92","1","Modificación de la Categoria Calzado","2019-03-17 15:42:57","2019-03-17 15:42:57");
INSERT INTO logs VALUES("93","1","La Categoria Cosmeticos Ha Sido Inhabilitada Satisfactoriamente","2019-03-17 15:43:16","2019-03-17 15:43:16");
INSERT INTO logs VALUES("94","1","Creación de Articulo|Producto Denominado:Blusa Smith","2019-03-17 15:44:36","2019-03-17 15:44:36");
INSERT INTO logs VALUES("95","1","Creación de Articulo|Producto Denominado:Blusa Smith","2019-03-17 15:45:19","2019-03-17 15:45:19");
INSERT INTO logs VALUES("96","1","Creación de Articulo|Producto Denominado:Blusa Carol","2019-03-17 15:48:37","2019-03-17 15:48:37");
INSERT INTO logs VALUES("97","1","Creación de Articulo|Producto Denominado:Blusa Carol","2019-03-17 15:49:14","2019-03-17 15:49:14");
INSERT INTO logs VALUES("98","1","Creación de Articulo|Producto Denominado:Blusa Carol","2019-03-17 15:50:12","2019-03-17 15:50:12");
INSERT INTO logs VALUES("99","1","Creación de Articulo|Producto Denominado:Blusa Carol","2019-03-17 15:50:38","2019-03-17 15:50:38");
INSERT INTO logs VALUES("100","1","Creación de Articulo|Producto Denominado:Blusa Carol","2019-03-17 15:51:08","2019-03-17 15:51:08");
INSERT INTO logs VALUES("101","1","Creación de Articulo|Producto Denominado:Blusa Smith","2019-03-17 15:53:46","2019-03-17 15:53:46");
INSERT INTO logs VALUES("102","1","Creación de Articulo|Producto Denominado:Vestido Casual","2019-03-17 15:54:47","2019-03-17 15:54:47");
INSERT INTO logs VALUES("103","1","Creación de Articulo|Producto Denominado:Zapato ZERO","2019-03-17 15:56:23","2019-03-17 15:56:23");
INSERT INTO logs VALUES("104","1","Creación de Articulo|Producto Denominado:Zapato ZERO","2019-03-17 15:57:08","2019-03-17 15:57:08");
INSERT INTO logs VALUES("105","1","Creación de Articulo|Producto Denominado:Zapato ZERO","2019-03-17 15:58:17","2019-03-17 15:58:17");
INSERT INTO logs VALUES("106","1","Creación de Articulo|Producto Denominado:Zapato ZERO","2019-03-17 15:58:50","2019-03-17 15:58:50");
INSERT INTO logs VALUES("107","1","Creación de Articulo|Producto Denominado:Tacones Elisse","2019-03-17 16:11:12","2019-03-17 16:11:12");
INSERT INTO logs VALUES("108","1","Creación de Articulo|Producto Denominado:Tacones Elisse","2019-03-17 16:13:38","2019-03-17 16:13:38");
INSERT INTO logs VALUES("109","1","Creación de Articulo|Producto Denominado:Tacones Elisse","2019-03-17 16:14:21","2019-03-17 16:14:21");
INSERT INTO logs VALUES("110","1","Creación de Articulo|Producto Denominado:Body Juliette","2019-03-17 16:16:44","2019-03-17 16:16:44");
INSERT INTO logs VALUES("111","1","Modificación de Articulo|Producto Denominado:Body Juliette","2019-03-17 16:17:57","2019-03-17 16:17:57");
INSERT INTO logs VALUES("112","1","Creación de Articulo|Producto Denominado:Body Juliette","2019-03-17 16:19:29","2019-03-17 16:19:29");
INSERT INTO logs VALUES("113","1","Creación de Articulo|Producto Denominado:Body Juliette","2019-03-17 16:19:50","2019-03-17 16:19:50");
INSERT INTO logs VALUES("114","1","Creación de Articulo|Producto Denominado:Body Juliette","2019-03-17 16:20:11","2019-03-17 16:20:11");
INSERT INTO logs VALUES("115","1","Creación de Articulo|Producto Denominado:Vestido Casual","2019-03-17 16:23:51","2019-03-17 16:23:51");
INSERT INTO logs VALUES("116","1","Creación de Articulo|Producto Denominado:Vestido Casual","2019-03-17 16:25:08","2019-03-17 16:25:08");
INSERT INTO logs VALUES("117","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:26:06","2019-03-17 16:26:06");
INSERT INTO logs VALUES("118","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:26:37","2019-03-17 16:26:37");
INSERT INTO logs VALUES("119","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:28:56","2019-03-17 16:28:56");
INSERT INTO logs VALUES("120","1","Creación de Articulo|Producto Denominado:JumpSuit","2019-03-17 16:32:02","2019-03-17 16:32:02");
INSERT INTO logs VALUES("121","1","Creación de Articulo|Producto Denominado:JumpSuit","2019-03-17 16:32:29","2019-03-17 16:32:29");
INSERT INTO logs VALUES("122","1","Creación de Articulo|Producto Denominado:JumpSuit","2019-03-17 16:32:55","2019-03-17 16:32:55");
INSERT INTO logs VALUES("123","1","Creación de Articulo|Producto Denominado:JumpSuit","2019-03-17 16:33:24","2019-03-17 16:33:24");
INSERT INTO logs VALUES("124","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:34:46","2019-03-17 16:34:46");
INSERT INTO logs VALUES("125","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:35:19","2019-03-17 16:35:19");
INSERT INTO logs VALUES("126","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:35:50","2019-03-17 16:35:50");
INSERT INTO logs VALUES("127","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:36:16","2019-03-17 16:36:16");
INSERT INTO logs VALUES("128","1","Creación de Articulo|Producto Denominado:Bralette","2019-03-17 16:36:41","2019-03-17 16:36:41");
INSERT INTO logs VALUES("129","1","Modificación de Agencia de Envio Tealca","2019-03-17 16:37:01","2019-03-17 16:37:01");
INSERT INTO logs VALUES("130","1","Modificación de Agencia de Envio Mrw","2019-03-17 16:37:13","2019-03-17 16:37:13");
INSERT INTO logs VALUES("131","1","Modificación de Agencia de Envio Zoom","2019-03-17 16:37:34","2019-03-17 16:37:34");
INSERT INTO logs VALUES("132","1","Modificación de Agencia de Envio tuenvio.com","2019-03-17 16:37:50","2019-03-17 16:37:50");
INSERT INTO logs VALUES("133","1","Modificación  del BancoBANCO DE VENEZUELA","2019-03-17 16:38:12","2019-03-17 16:38:12");
INSERT INTO logs VALUES("134","1","Eliminación  del BancoBanco Nacional de Credito","2019-03-17 16:38:22","2019-03-17 16:38:22");
INSERT INTO logs VALUES("135","1","Creación del Usuario Emma Rosales","2019-03-17 16:46:20","2019-03-17 16:46:20");
INSERT INTO logs VALUES("136","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 76","2019-03-17 16:49:51","2019-03-17 16:49:51");
INSERT INTO logs VALUES("137","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 77","2019-03-17 16:51:28","2019-03-17 16:51:28");
INSERT INTO logs VALUES("138","18","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:124051","2019-03-17 16:52:10","2019-03-17 16:52:10");
INSERT INTO logs VALUES("139","18","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:448830","2019-03-17 16:52:23","2019-03-17 16:52:23");
INSERT INTO logs VALUES("140","17","Confirmación de Pago de La Compra Número:76","2019-03-17 16:52:51","2019-03-17 16:52:51");
INSERT INTO logs VALUES("141","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número78","2019-03-19 22:01:01","2019-03-19 22:01:01");
INSERT INTO logs VALUES("142","1","Modificación de Articulo|Producto Denominado:Body Jazmin","2019-03-19 22:17:15","2019-03-19 22:17:15");
INSERT INTO logs VALUES("143","19","Modificación del Usuario .MARIANELL PEREZ","2019-03-19 23:19:03","2019-03-19 23:19:03");
INSERT INTO logs VALUES("144","1","Ha Eliminado Al Usuario .Karla Castillo","2019-03-22 03:36:28","2019-03-22 03:36:28");
INSERT INTO logs VALUES("145","1","Creación del Usuario Karla Castillo","2019-03-22 03:37:23","2019-03-22 03:37:23");
INSERT INTO logs VALUES("146","1","Creación del Usuario Marieglis Tinoco","2019-03-22 03:38:58","2019-03-22 03:38:58");
INSERT INTO logs VALUES("147","1","Creación del Usuario Pedro Toussaint","2019-03-22 03:39:53","2019-03-22 03:39:53");
INSERT INTO logs VALUES("148","1","Modificación de Articulo|Producto Denominado:Paleta de sombras MAC","2019-03-22 03:40:22","2019-03-22 03:40:22");
INSERT INTO logs VALUES("149","1","Modificación de Articulo|Producto Denominado:Paleta de sombras Kyle","2019-03-22 03:40:49","2019-03-22 03:40:49");
INSERT INTO logs VALUES("150","1","Modificación de Articulo|Producto Denominado:Vestido Sport Rosa","2019-03-22 03:42:19","2019-03-22 03:42:19");
INSERT INTO logs VALUES("151","1","Eliminacioón del Producto Vestido Sport","2019-03-22 03:42:29","2019-03-22 03:42:29");
INSERT INTO logs VALUES("152","1","Eliminacioón del Producto Labial","2019-03-22 03:42:36","2019-03-22 03:42:36");
INSERT INTO logs VALUES("153","1","Eliminacioón del Producto Labial","2019-03-22 03:42:42","2019-03-22 03:42:42");
INSERT INTO logs VALUES("154","1","Modificación de Articulo|Producto Denominado:Body Jazmin","2019-03-22 03:43:05","2019-03-22 03:43:05");
INSERT INTO logs VALUES("155","1","Modificación de Articulo|Producto Denominado:Body Eunor","2019-03-22 03:43:25","2019-03-22 03:43:25");
INSERT INTO logs VALUES("156","1","Modificación de Articulo|Producto Denominado:Body Eunor","2019-03-22 03:43:35","2019-03-22 03:43:35");
INSERT INTO logs VALUES("157","1","Eliminacioón del Producto Blusa Smith","2019-03-22 03:44:05","2019-03-22 03:44:05");
INSERT INTO logs VALUES("158","1","Creación de Articulo|Producto Denominado:Sweater Supreme","2019-03-22 03:50:45","2019-03-22 03:50:45");
INSERT INTO logs VALUES("159","1","Creación de Articulo|Producto Denominado:Sweater Terciopelo","2019-03-22 03:51:23","2019-03-22 03:51:23");
INSERT INTO logs VALUES("160","1","Creación de Articulo|Producto Denominado:Body Danielle","2019-03-22 03:51:55","2019-03-22 03:51:55");
INSERT INTO logs VALUES("161","1","Creación de Articulo|Producto Denominado:Body Lim","2019-03-22 03:52:42","2019-03-22 03:52:42");
INSERT INTO logs VALUES("162","1","Creación de Articulo|Producto Denominado:Body Bassel","2019-03-22 03:53:32","2019-03-22 03:53:32");
INSERT INTO logs VALUES("163","1","Creación de Articulo|Producto Denominado:Blusa Luci","2019-03-22 03:54:26","2019-03-22 03:54:26");
INSERT INTO logs VALUES("164","1","Creación de Articulo|Producto Denominado:Blusa Jenniel","2019-03-22 03:55:09","2019-03-22 03:55:09");
INSERT INTO logs VALUES("165","1","Creación de Articulo|Producto Denominado:Blusa Kriss","2019-03-22 03:55:45","2019-03-22 03:55:45");
INSERT INTO logs VALUES("166","17","Eliminacioón del Producto Tacones de plataforma","2019-03-22 12:50:11","2019-03-22 12:50:11");
INSERT INTO logs VALUES("167","17","El Producto Blusa Carol 01 Ha Sido Destacado","2019-03-22 12:50:59","2019-03-22 12:50:59");
INSERT INTO logs VALUES("168","17","El Producto Vestido Sport Rosa Ha Sido Destacado","2019-03-22 12:51:07","2019-03-22 12:51:07");
INSERT INTO logs VALUES("169","17","El Producto Vestido Jenn Ha Sido Destacado","2019-03-22 12:51:32","2019-03-22 12:51:32");
INSERT INTO logs VALUES("170","17","El Producto Body Bassel Ha Sido Destacado","2019-03-22 12:52:08","2019-03-22 12:52:08");
INSERT INTO logs VALUES("171","17","El Producto Blusa Kriss Ha Sido Destacado","2019-03-22 12:52:15","2019-03-22 12:52:15");
INSERT INTO logs VALUES("172","17","Modificación del Usuario .Emma Rosales","2019-03-22 12:52:52","2019-03-22 12:52:52");
INSERT INTO logs VALUES("173","17","Creción de un Nuevo Cliente de Nombre:Belkis Rangel","2019-03-22 13:31:35","2019-03-22 13:31:35");
INSERT INTO logs VALUES("174","1","Creción de un Nuevo Cliente de Nombre:carlos castillo","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO logs VALUES("175","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número79","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO logs VALUES("176","1","La Compra Número:77Ha Fue Eliminada","2019-03-22 13:42:57","2019-03-22 13:42:57");
INSERT INTO logs VALUES("177","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número80","2019-03-22 13:44:26","2019-03-22 13:44:26");
INSERT INTO logs VALUES("178","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número81","2019-03-22 13:44:45","2019-03-22 13:44:45");
INSERT INTO logs VALUES("179","15","Creción de un Nuevo Cliente de Nombre:Mari Tinoco","2019-03-22 14:09:16","2019-03-22 14:09:16");
INSERT INTO logs VALUES("180","15","Procesamiento De Compra en Tienda Fisíca, Bajo El Número82","2019-03-22 14:09:17","2019-03-22 14:09:17");
INSERT INTO logs VALUES("181","17","Creción de un Nuevo Cliente de Nombre:Rut Veliz","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO logs VALUES("182","17","Procesamiento De Compra en Tienda Fisíca, Bajo El Número83","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO logs VALUES("183","18","Modificación del Usuario .Genesis Perez","2019-03-22 15:25:06","2019-03-22 15:25:06");
INSERT INTO logs VALUES("184","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 84","2019-03-22 15:26:05","2019-03-22 15:26:05");
INSERT INTO logs VALUES("185","18","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:12345","2019-03-22 15:26:47","2019-03-22 15:26:47");
INSERT INTO logs VALUES("186","17","Confirmación de Pago de La Compra Número:84","2019-03-22 15:28:41","2019-03-22 15:28:41");
INSERT INTO logs VALUES("187","17","Confirmación de Pago de La Compra Número:84","2019-03-22 15:28:42","2019-03-22 15:28:42");
INSERT INTO logs VALUES("188","28","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 85","2019-03-22 16:33:41","2019-03-22 16:33:41");
INSERT INTO logs VALUES("189","28","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:252226","2019-03-22 16:34:41","2019-03-22 16:34:41");
INSERT INTO logs VALUES("190","17","Confirmación de Pago de La Compra Número:85","2019-03-22 16:41:30","2019-03-22 16:41:30");
INSERT INTO logs VALUES("191","17","Creación de Articulo|Producto Denominado:Body Kristen","2019-03-22 16:50:29","2019-03-22 16:50:29");
INSERT INTO logs VALUES("192","17","Modificación de Articulo|Producto Denominado:Body Kristen","2019-03-22 16:51:21","2019-03-22 16:51:21");
INSERT INTO logs VALUES("193","17","Creción de un Nuevo Cliente de Nombre:Karol jimenez","2019-03-22 16:54:12","2019-03-22 16:54:12");
INSERT INTO logs VALUES("194","17","Procesamiento De Compra en Tienda Fisíca, Bajo El Número86","2019-03-22 16:54:12","2019-03-22 16:54:12");
INSERT INTO logs VALUES("195","17","Modificación de Articulo|Producto Denominado:Body Kristen","2019-03-22 16:54:36","2019-03-22 16:54:36");
INSERT INTO logs VALUES("196","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 87","2019-03-22 23:36:31","2019-03-22 23:36:31");
INSERT INTO logs VALUES("197","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 88","2019-03-22 23:51:44","2019-03-22 23:51:44");
INSERT INTO logs VALUES("198","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 89","2019-03-22 23:53:59","2019-03-22 23:53:59");
INSERT INTO logs VALUES("199","18","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:22455","2019-03-22 23:54:13","2019-03-22 23:54:13");
INSERT INTO logs VALUES("200","18","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 90","2019-03-29 13:03:47","2019-03-29 13:03:47");
INSERT INTO logs VALUES("201","1","Confirmación de Pago de La Compra Número:87","2019-03-30 17:01:41","2019-03-30 17:01:41");
INSERT INTO logs VALUES("202","1","Creación del Usuario LILIANA BOLIVAR","2019-03-31 16:11:20","2019-03-31 16:11:20");
INSERT INTO logs VALUES("203","18","Reporto  Pago Mediante Plataforma en Linea Con El Número de Referencia:543456","2019-03-31 16:24:39","2019-03-31 16:24:39");
INSERT INTO logs VALUES("204","1","Confirmación de Pago de La Compra Número:90","2019-04-01 11:40:57","2019-04-01 11:40:57");

DROP TABLE IF EXISTS  migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES("10","2014_10_12_000000_create_users_table","1");
INSERT INTO migrations VALUES("11","2014_10_12_100000_create_password_resets_table","1");
INSERT INTO migrations VALUES("12","2018_12_18_030738_create_categories_table","1");
INSERT INTO migrations VALUES("13","2018_12_18_032522_create_products_table","1");
INSERT INTO migrations VALUES("14","2018_12_20_015820_create_products_images_table","1");
INSERT INTO migrations VALUES("15","2019_01_03_234633_create_compras_carts_table","1");
INSERT INTO migrations VALUES("16","2019_01_03_234950_create_compras_cart_detalles_table","1");
INSERT INTO migrations VALUES("17","2019_01_04_003117_create_bancos_table","1");
INSERT INTO migrations VALUES("18","2019_01_04_003226_create_tipo_pagos_table","1");
INSERT INTO migrations VALUES("19","2019_01_20_201233_create_sugerencias_table","2");
INSERT INTO migrations VALUES("20","2019_01_21_225233_create_envios_table","3");
INSERT INTO migrations VALUES("21","2019_01_23_032331_create_ivas_table","4");
INSERT INTO migrations VALUES("22","2019_01_30_045725_create_logs_table","5");

DROP TABLE IF EXISTS  password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS  products;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  KEY `products_user_id_foreign` (`user_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products VALUES("1","2","TABLA DE MAQUILLAJE","MAQUILLAJE CONTENEDOR CON ESPECIFICACIONES DE GRAN INDOLE","45","3565.30","10","15","5c3a95281a788-IMG_20181126_201345.png","1","9","","1","2019-01-12 02:28:23","2019-03-19 22:01:01");
INSERT INTO products VALUES("2","2","ESTUCHE MULTICOLOR INTEGRADO","ESTUCHE COORPORTATIVO","27","5000.00","5","100","5c3ce4877c66e-IMG_20181126_202333.png","0","9","","1","2019-01-14 20:05:35","2019-02-01 04:11:55");
INSERT INTO products VALUES("3","1","labial principal","-","34","650.00","1","300","5c3ce4fb9dab1-IMG_20181126_195549.png","1","9","","1","2019-01-14 20:07:31","2019-02-14 23:48:29");
INSERT INTO products VALUES("4","2","ZAPATOS DEPORTVOS","--","89","1500.00","5","90","5c3ce52955090-IMG_20181126_195647.png","0","9","","1","2019-01-14 20:08:17","2019-02-14 23:48:29");
INSERT INTO products VALUES("5","2","BODY ROJO","--","43","5600.00","2","1","5c3ce85c63d4d-IMG_20181126_195627.png","0","9","","1","2019-01-14 20:22:00","2019-02-12 23:46:08");
INSERT INTO products VALUES("6","2","BODY TROPICAL","-","91","3500.00","5","100","5c3d35ee71ca7-IMG_20181126_201711.png","1","9","","1","2019-01-15 01:52:55","2019-02-12 23:46:08");
INSERT INTO products VALUES("7","2","BERMUDA FALDON","--","42","78200.00","1","100","5c3d36402451a-IMG_20181126_202155.png","1","9","","1","2019-01-15 01:54:16","2019-03-19 22:01:01");
INSERT INTO products VALUES("8","2","BLUSAS ESCOTEX","--","100","5000.00","1","100","5c3d366d699e2-IMG_20181126_201751.png","0","9","","1","2019-01-15 01:55:01","2019-02-12 23:27:35");
INSERT INTO products VALUES("9","2","PINTURA LABIAL","--","45","5300.00","1","10","5c3d36c02ff8d-IMG_20181126_195549.png","1","9","","1","2019-01-15 01:56:24","2019-02-12 23:27:39");
INSERT INTO products VALUES("10","1","DELINEADOR PRINCIPAL","--","16","3000.00","5","50","5c3d36ef135f2-IMG_20181126_195659.png","0","9","","1","2019-01-15 01:57:11","2019-02-12 23:27:43");
INSERT INTO products VALUES("11","5","Tacones","tacones","29","60000.00","5","30","5c53bb1b1eeaa-5c2c3bb02d2ab-IMG_20181126_201327.png","0","9","","1","2019-02-01 03:26:54","2019-03-22 13:33:51");
INSERT INTO products VALUES("12","5","Tacones","tacones","30","50000.00","5","30","5c53b5862086e-61FPmD9XlsL._UY500_.jpg","0","9","","1","2019-02-01 03:27:10","2019-02-12 23:27:04");
INSERT INTO products VALUES("13","1","Paleta de sombras Kyle","Paleta de sombras","3","20000.00","5","30","5c8db3c85ddff-5c2c3a6449ae4-IMG_20181126_202333.png","0","1","","1","2019-03-17 02:41:12","2019-03-29 13:03:47");
INSERT INTO products VALUES("14","1","Paleta de sombras MAC","Paleta de sombras marca MAC","16","21000.00","5","30","5c8db40fb9341-5c33ac2c8c6b7-IMG_20181126_200056.png","0","1","","1","2019-03-17 02:42:23","2019-03-22 23:51:44");
INSERT INTO products VALUES("15","1","Labial MAC","Labiales MATE, variedad de colores.

-Nude
-Fashion
-Mocca
-Fucsia
-Verano
-Pink","30","10000.00","5","30","5c8db4ab56ecc-5c2c3a227b6e3-IMG_20181126_202544.png","0","9","","1","2019-03-17 02:44:59","2019-03-22 03:42:36");
INSERT INTO products VALUES("16","1","Labial Rosse","Labiales MATE, variedad de colores.

-Nude
-Fashion
-Mocca
-Fucsia
-Verano
-Pink","30","10000.00","5","30","5c8db4bb4aac0-5c2c3a227b6e3-IMG_20181126_202544.png","0","9","","1","2019-03-17 02:45:15","2019-03-22 03:42:42");
INSERT INTO products VALUES("17","1","Labial Kyle","Labiales Colores Calidos","30","12000.00","5","30","5c8db4ee37dc4-5c33ad40d8fe2-IMG_20181126_195659.png","0","1","","1","2019-03-17 02:46:06","2019-03-17 02:46:06");
INSERT INTO products VALUES("18","5","Zapatos Deportivos","Zapatos deportivos marca ZOE 

-Disponibles en plateado y dorado
-Tallas Desde la 35 a 40","15","30000.00","4","15","5c8db5bf11f3c-5c33ac9c6109e-IMG_20181126_195647.png","0","1","","1","2019-03-17 02:49:35","2019-03-17 02:49:35");
INSERT INTO products VALUES("19","5","Tacones de plataforma","Tacones de plataforma Marca ZERO

Disponibles unicamente en color caqui

Tallas desde la 36 hasta la 39","10","40000.00","5","10","5c8db616c3d20-5c2c3bb02d2ab-IMG_20181126_201327.png","0","9","","1","2019-03-17 02:51:02","2019-03-22 12:50:11");
INSERT INTO products VALUES("20","4","Vestido Sport","Vestido Sport Talla Unica","20","35000.00","5","20","5c8e69eee2118-vestidoa1.png","0","9","","1","2019-03-17 15:38:22","2019-03-22 03:42:29");
INSERT INTO products VALUES("21","4","Vestido Sport Rosa","Vestido Sport Talla Unica","20","35000.00","5","20","5c8e6a024aba4-vestidoa1.png","1","1","","1","2019-03-17 15:38:42","2019-03-22 12:51:07");
INSERT INTO products VALUES("22","8","Body Jazmin 03","Talla unica","12","15000.00","5","20","5c8e6a35399dd-Body a1.png","0","1","","1","2019-03-17 15:39:33","2019-03-22 23:36:31");
INSERT INTO products VALUES("23","8","Body Jazmin 02","Body Talla Unica","20","15000.00","5","20","5c8e6a5e45aa1-body a2.png","0","1","","1","2019-03-17 15:40:14","2019-03-22 03:43:35");
INSERT INTO products VALUES("24","8","Body Juliette 05","Body "Juliette" Talla Unica.","20","17500.00","5","20","5c8e6ac2069aa-IMG_20181126_201711.png","0","1","","1","2019-03-17 15:41:54","2019-03-17 15:41:54");
INSERT INTO products VALUES("25","9","Blusa Smith 01","Blusa "Smith" variedad de colores. 
Todas las Tallas","20","12500.00","5","20","5c8e6b648181d-IMG_20181126_201954.png","0","1","","1","2019-03-17 15:44:36","2019-03-17 15:44:36");
INSERT INTO products VALUES("26","9","Blusa Smith 02","Blusa "Smith" variedad de colores. 
Todas las Tallas","19","12500.00","5","20","5c8e6b8ee0e33-IMG_20181126_202059.png","0","9","","1","2019-03-17 15:45:18","2019-03-22 13:44:26");
INSERT INTO products VALUES("27","9","Blusa Carol 01","Blusa "Carol" 
Variedad de estampados
Talla Unica","-7","12500.00","5","20","5c8e6c551cc00-IMG_20181126_195752.png","1","1","","1","2019-03-17 15:48:37","2019-03-22 16:33:41");
INSERT INTO products VALUES("28","9","Blusa Carol 02","Blusa "Carol"

Variedad de estampados

Talla Unica","20","12500.00","5","20","5c8e6c7abc90a-IMG_20181126_195804.png","0","1","","1","2019-03-17 15:49:14","2019-03-17 15:49:14");
INSERT INTO products VALUES("29","9","Blusa Carol 03","Blusa "Carol"

Variedad de estampados

Talla Unica","20","12500.00","5","20","5c8e6cb4925fd-IMG_20181126_201656.png","0","1","","1","2019-03-17 15:50:12","2019-03-17 15:50:12");
INSERT INTO products VALUES("30","9","Blusa Carol 04","Blusa "Carol"

Variedad de estampados

Talla Unica","20","12500.00","5","20","5c8e6cce86c2e-IMG_20181126_201734.png","0","1","","1","2019-03-17 15:50:38","2019-03-17 15:50:38");
INSERT INTO products VALUES("31","9","Blusa Carol 05","Blusa "Carol"

Variedad de estampados

Talla Unica","20","12500.00","5","20","5c8e6cecce5dc-IMG_20181126_201826.png","0","1","","1","2019-03-17 15:51:08","2019-03-17 15:51:08");
INSERT INTO products VALUES("32","9","Blusa Smith 03","Blusa "Smith"

Variedad de Colores

Talla Unica","19","12500.00","5","20","5c8e6d8abc85a-IMG_20181126_195850.png","0","1","","1","2019-03-17 15:53:46","2019-03-22 14:37:31");
INSERT INTO products VALUES("33","4","Vestido Casual","Vestido Casual Marca "Kleos"

Talla unica","15","22500.00","5","15","5c8e6dc7af750-IMG_20181126_200342.png","0","1","","1","2019-03-17 15:54:47","2019-03-17 15:54:47");
INSERT INTO products VALUES("34","5","Zapato ZERO 01","Zapato Sport Marca ZERO

Disponible desde la talla 35 hasta la 40","14","33450.00","5","15","5c8e6e27b187b-IMG_20181126_195637.png","0","1","","1","2019-03-17 15:56:23","2019-03-22 14:09:17");
INSERT INTO products VALUES("35","5","Zapato ZERO 02","Zapato Sport Marca ZERO

Disponible desde la talla 35 hasta la 40","15","33450.00","5","15","5c8e6e54277a9-IMG_20181126_202356.png","0","1","","1","2019-03-17 15:57:08","2019-03-17 15:57:08");
INSERT INTO products VALUES("36","5","Zapato ZERO 03","Zapato Sport Marca ZERO

Disponible desde la talla 35 hasta la 40","15","33450.00","5","20","5c8e6e99a9eea-IMG_20181126_202428.png","0","1","","1","2019-03-17 15:58:17","2019-03-17 15:58:17");
INSERT INTO products VALUES("37","5","Zapato ZERO 04","Zapato Sport Marca ZERO

Disponible desde la talla 35 hasta la 40","-23","3500.00","5","15","5c8e6eba7fc82-IMG_20181126_201359.png","0","1","","1","2019-03-17 15:58:50","2019-03-22 23:53:59");
INSERT INTO products VALUES("38","5","Tacones Elisse 01","Tacones Elisse

Desde la talla 35 hasta la 40","15","110000.00","5","15","5c8e71a081de9-2.png","0","1","","1","2019-03-17 16:11:12","2019-03-17 16:11:12");
INSERT INTO products VALUES("39","5","Tacones Elisse 02","Tacones Ellise

Desde la talla 35 hasta la 40","15","110000.00","5","15","5c8e7232a0cae-4.png","0","1","","1","2019-03-17 16:13:38","2019-03-17 16:13:38");
INSERT INTO products VALUES("40","5","Tacones Elisse 03","Tacones Elisse

Desde la talla 35 hasta la 40","15","110000.00","5","15","5c8e725d804ce-6.png","0","1","","1","2019-03-17 16:14:21","2019-03-17 16:14:21");
INSERT INTO products VALUES("41","8","Body Juliette 01","Body Juliette

Talla unica","29","17500.00","5","30","5c8e73356d802-7.png","0","1","","1","2019-03-17 16:16:43","2019-03-22 13:44:45");
INSERT INTO products VALUES("42","8","Body Juliette 02","Body Juliette

Talla unica","29","17500.00","5","30","5c8e739123560-8.png","0","1","","1","2019-03-17 16:19:29","2019-03-22 16:54:12");
INSERT INTO products VALUES("43","8","Body Juliette 03","Body Juliette

Talla unica","30","17500.00","5","30","5c8e73a63ea3a-8-1.png","0","1","","1","2019-03-17 16:19:50","2019-03-17 16:19:50");
INSERT INTO products VALUES("44","8","Body Juliette 04","Body Juliette

Talla unica","30","17500.00","5","30","5c8e73bb3c603-9.png","0","1","","1","2019-03-17 16:20:11","2019-03-17 16:20:11");
INSERT INTO products VALUES("45","4","Vestido Cash","Vestido Casual Unicolor Talla unica","15","22500.00","5","10","5c8e7497b239f-10.png","0","1","","1","2019-03-17 16:23:51","2019-03-17 16:23:51");
INSERT INTO products VALUES("46","4","Vestido Jenn","Vestido Casual

Unico color disponible

Talla unica","15","20500.00","5","10","5c8e74e3e7719-11.png","1","1","","1","2019-03-17 16:25:07","2019-03-22 12:51:32");
INSERT INTO products VALUES("47","10","Bralette 01","Tallas S,M,L","15","10000.00","5","15","5c8e751e26af1-12.png","0","1","","1","2019-03-17 16:26:06","2019-03-17 16:26:06");
INSERT INTO products VALUES("48","10","Bralette 02","Tallas S,M,L","15","10000.00","5","15","5c8e753d73a73-13.png","0","1","","1","2019-03-17 16:26:37","2019-03-17 16:49:51");
INSERT INTO products VALUES("49","10","Bralette 03","Tallas S,M,L","15","10000.00","5","15","5c8e75c7f4083-14.png","0","1","","1","2019-03-17 16:28:56","2019-03-17 16:28:56");
INSERT INTO products VALUES("50","10","JumpSuit 01","Jumpper Tallas S,M,L","-20","19900.00","5","15","5c8e7682bd83c-15.png","0","1","","1","2019-03-17 16:32:02","2019-03-22 15:26:05");
INSERT INTO products VALUES("51","10","JumpSuit 02","Jumpper Tallas S,M,L","30","19900.00","5","30","5c8e769d95bf5-15A.png","0","1","","1","2019-03-17 16:32:29","2019-03-17 16:32:29");
INSERT INTO products VALUES("52","10","JumpSuit 03","Jumpper Tallas S,M,L","30","19900.00","5","30","5c8e76b7c8375-16.png","0","1","","1","2019-03-17 16:32:55","2019-03-17 16:32:55");
INSERT INTO products VALUES("53","10","JumpSuit 04","Jumpper Tallas S,M,L","30","19900.00","5","30","5c8e76d48826e-17.png","0","1","","1","2019-03-17 16:33:24","2019-03-17 16:33:24");
INSERT INTO products VALUES("54","10","Bralette 04","Tallas S,M,L","15","10000.00","5","15","5c8e772677624-B1.png","0","1","","1","2019-03-17 16:34:46","2019-03-17 16:34:46");
INSERT INTO products VALUES("55","10","Bralette 05","Tallas S,M,L","15","10000.00","5","15","5c8e77471ae5d-B2.png","0","1","","1","2019-03-17 16:35:19","2019-03-17 16:35:19");
INSERT INTO products VALUES("56","10","Bralette 06","Tallas S,M,L","15","10000.00","5","15","5c8e7766038e6-B3.png","0","1","","1","2019-03-17 16:35:50","2019-03-17 16:35:50");
INSERT INTO products VALUES("57","10","Bralette 07","Tallas S,M,L","15","10000.00","5","15","5c8e77809fbeb-B4.png","0","1","","1","2019-03-17 16:36:16","2019-03-17 16:36:16");
INSERT INTO products VALUES("58","8","Bralette 08","Tallas S,M,L","15","10000.00","5","15","5c8e77994b0b1-B5.png","0","1","","1","2019-03-17 16:36:41","2019-03-17 16:36:41");
INSERT INTO products VALUES("59","10","Sweater Supreme","Talla Unica","30","30000.00","5","30","5c945b95a047a-S1.png","0","1","","1","2019-03-22 03:50:45","2019-03-22 03:50:45");
INSERT INTO products VALUES("60","10","Sweater Terciopelo","Talla Unica","30","35000.00","5","30","5c945bbbd544d-S2.png","0","1","","1","2019-03-22 03:51:23","2019-03-22 03:51:23");
INSERT INTO products VALUES("61","8","Body Danielle","Talla Unica","15","17800.00","5","15","5c945bdb15549-IMG_20181126_195627.png","0","1","","1","2019-03-22 03:51:55","2019-03-22 03:51:55");
INSERT INTO products VALUES("62","8","Body Lim","Talla unica","33","18200.00","5","34","5c945c0a9db9c-IMG_20181126_200242.png","0","1","","1","2019-03-22 03:52:42","2019-03-22 13:44:45");
INSERT INTO products VALUES("63","8","Body Bassel","Talla Unica","30","14500.00","5","30","5c945c3bf2b1f-IMG_20181126_202711.png","1","1","","1","2019-03-22 03:53:32","2019-03-22 12:52:08");
INSERT INTO products VALUES("64","9","Blusa Luci","Talla M","24","16500.00","5","25","5c945c721ce22-IMG_20181126_195528.png","0","1","","1","2019-03-22 03:54:26","2019-03-22 14:37:31");
INSERT INTO products VALUES("65","9","Blusa Jenniel","Talla S","14","16000.00","5","14","5c945c9d47d7a-IMG_20181126_200714.png","0","1","","1","2019-03-22 03:55:09","2019-03-22 03:55:09");
INSERT INTO products VALUES("66","9","Blusa Kriss","Talla L","18","15000.00","5","18","5c945cc1595c2-IMG_20181126_200353.png","1","1","","1","2019-03-22 03:55:45","2019-03-22 12:52:15");
INSERT INTO products VALUES("67","8","Body Kristen","Talla unica","25","18500.00","5","20","5c9512556e9ec-IMG_20181126_200425.png","0","1","","17","2019-03-22 16:50:29","2019-03-22 16:54:36");

DROP TABLE IF EXISTS  products_images;

CREATE TABLE `products_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_producto_id_foreign` (`producto_id`),
  CONSTRAINT `products_images_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products_images VALUES("1","13","5c8db3c85ddff-5c2c3a6449ae4-IMG_20181126_202333.png","1","2019-03-17 02:41:12","2019-03-17 02:41:12");
INSERT INTO products_images VALUES("2","14","5c8db40fb9341-5c33ac2c8c6b7-IMG_20181126_200056.png","1","2019-03-17 02:42:23","2019-03-17 02:42:23");
INSERT INTO products_images VALUES("4","17","5c8db4ee37dc4-5c33ad40d8fe2-IMG_20181126_195659.png","1","2019-03-17 02:46:06","2019-03-17 02:46:06");
INSERT INTO products_images VALUES("5","18","5c8db5bf11f3c-5c33ac9c6109e-IMG_20181126_195647.png","1","2019-03-17 02:49:35","2019-03-17 02:49:35");
INSERT INTO products_images VALUES("7","21","5c8e6a024aba4-vestidoa1.png","1","2019-03-17 15:38:42","2019-03-17 15:38:42");
INSERT INTO products_images VALUES("8","22","5c8e6a35399dd-Body a1.png","1","2019-03-17 15:39:33","2019-03-17 15:39:33");
INSERT INTO products_images VALUES("9","23","5c8e6a5e45aa1-body a2.png","1","2019-03-17 15:40:14","2019-03-17 15:40:14");
INSERT INTO products_images VALUES("10","24","5c8e6ac2069aa-IMG_20181126_201711.png","1","2019-03-17 15:41:54","2019-03-17 15:41:54");
INSERT INTO products_images VALUES("11","25","5c8e6b648181d-IMG_20181126_201954.png","1","2019-03-17 15:44:36","2019-03-17 15:44:36");
INSERT INTO products_images VALUES("13","27","5c8e6c551cc00-IMG_20181126_195752.png","1","2019-03-17 15:48:37","2019-03-17 15:48:37");
INSERT INTO products_images VALUES("14","28","5c8e6c7abc90a-IMG_20181126_195804.png","1","2019-03-17 15:49:14","2019-03-17 15:49:14");
INSERT INTO products_images VALUES("15","29","5c8e6cb4925fd-IMG_20181126_201656.png","1","2019-03-17 15:50:12","2019-03-17 15:50:12");
INSERT INTO products_images VALUES("16","30","5c8e6cce86c2e-IMG_20181126_201734.png","1","2019-03-17 15:50:38","2019-03-17 15:50:38");
INSERT INTO products_images VALUES("17","31","5c8e6cecce5dc-IMG_20181126_201826.png","1","2019-03-17 15:51:08","2019-03-17 15:51:08");
INSERT INTO products_images VALUES("18","32","5c8e6d8abc85a-IMG_20181126_195850.png","1","2019-03-17 15:53:46","2019-03-17 15:53:46");
INSERT INTO products_images VALUES("19","33","5c8e6dc7af750-IMG_20181126_200342.png","1","2019-03-17 15:54:47","2019-03-17 15:54:47");
INSERT INTO products_images VALUES("20","34","5c8e6e27b187b-IMG_20181126_195637.png","1","2019-03-17 15:56:23","2019-03-17 15:56:23");
INSERT INTO products_images VALUES("21","35","5c8e6e54277a9-IMG_20181126_202356.png","1","2019-03-17 15:57:08","2019-03-17 15:57:08");
INSERT INTO products_images VALUES("22","36","5c8e6e99a9eea-IMG_20181126_202428.png","1","2019-03-17 15:58:17","2019-03-17 15:58:17");
INSERT INTO products_images VALUES("23","37","5c8e6eba7fc82-IMG_20181126_201359.png","1","2019-03-17 15:58:50","2019-03-17 15:58:50");
INSERT INTO products_images VALUES("24","38","5c8e71a081de9-2.png","1","2019-03-17 16:11:12","2019-03-17 16:11:12");
INSERT INTO products_images VALUES("25","39","5c8e7232a0cae-4.png","1","2019-03-17 16:13:38","2019-03-17 16:13:38");
INSERT INTO products_images VALUES("26","40","5c8e725d804ce-6.png","1","2019-03-17 16:14:21","2019-03-17 16:14:21");
INSERT INTO products_images VALUES("27","41","5c8e72ebdb0e8-7.png","1","2019-03-17 16:16:44","2019-03-17 16:16:44");
INSERT INTO products_images VALUES("28","41","5c8e7335a23ce-7.png","0","2019-03-17 16:17:57","2019-03-17 16:17:57");
INSERT INTO products_images VALUES("29","42","5c8e739123560-8.png","1","2019-03-17 16:19:29","2019-03-17 16:19:29");
INSERT INTO products_images VALUES("30","43","5c8e73a63ea3a-8-1.png","1","2019-03-17 16:19:50","2019-03-17 16:19:50");
INSERT INTO products_images VALUES("31","44","5c8e73bb3c603-9.png","1","2019-03-17 16:20:11","2019-03-17 16:20:11");
INSERT INTO products_images VALUES("32","45","5c8e7497b239f-10.png","1","2019-03-17 16:23:51","2019-03-17 16:23:51");
INSERT INTO products_images VALUES("33","46","5c8e74e3e7719-11.png","1","2019-03-17 16:25:08","2019-03-17 16:25:08");
INSERT INTO products_images VALUES("34","47","5c8e751e26af1-12.png","1","2019-03-17 16:26:06","2019-03-17 16:26:06");
INSERT INTO products_images VALUES("35","48","5c8e753d73a73-13.png","1","2019-03-17 16:26:37","2019-03-17 16:26:37");
INSERT INTO products_images VALUES("36","49","5c8e75c7f4083-14.png","1","2019-03-17 16:28:56","2019-03-17 16:28:56");
INSERT INTO products_images VALUES("37","50","5c8e7682bd83c-15.png","1","2019-03-17 16:32:02","2019-03-17 16:32:02");
INSERT INTO products_images VALUES("38","51","5c8e769d95bf5-15A.png","1","2019-03-17 16:32:29","2019-03-17 16:32:29");
INSERT INTO products_images VALUES("39","52","5c8e76b7c8375-16.png","1","2019-03-17 16:32:55","2019-03-17 16:32:55");
INSERT INTO products_images VALUES("40","53","5c8e76d48826e-17.png","1","2019-03-17 16:33:24","2019-03-17 16:33:24");
INSERT INTO products_images VALUES("41","54","5c8e772677624-B1.png","1","2019-03-17 16:34:46","2019-03-17 16:34:46");
INSERT INTO products_images VALUES("42","55","5c8e77471ae5d-B2.png","1","2019-03-17 16:35:19","2019-03-17 16:35:19");
INSERT INTO products_images VALUES("43","56","5c8e7766038e6-B3.png","1","2019-03-17 16:35:50","2019-03-17 16:35:50");
INSERT INTO products_images VALUES("44","57","5c8e77809fbeb-B4.png","1","2019-03-17 16:36:16","2019-03-17 16:36:16");
INSERT INTO products_images VALUES("45","58","5c8e77994b0b1-B5.png","1","2019-03-17 16:36:41","2019-03-17 16:36:41");
INSERT INTO products_images VALUES("46","59","5c945b95a047a-S1.png","1","2019-03-22 03:50:45","2019-03-22 03:50:45");
INSERT INTO products_images VALUES("47","60","5c945bbbd544d-S2.png","1","2019-03-22 03:51:24","2019-03-22 03:51:24");
INSERT INTO products_images VALUES("48","61","5c945bdb15549-IMG_20181126_195627.png","1","2019-03-22 03:51:55","2019-03-22 03:51:55");
INSERT INTO products_images VALUES("49","62","5c945c0a9db9c-IMG_20181126_200242.png","1","2019-03-22 03:52:42","2019-03-22 03:52:42");
INSERT INTO products_images VALUES("50","63","5c945c3bf2b1f-IMG_20181126_202711.png","1","2019-03-22 03:53:32","2019-03-22 03:53:32");
INSERT INTO products_images VALUES("51","64","5c945c721ce22-IMG_20181126_195528.png","1","2019-03-22 03:54:26","2019-03-22 03:54:26");
INSERT INTO products_images VALUES("52","65","5c945c9d47d7a-IMG_20181126_200714.png","1","2019-03-22 03:55:09","2019-03-22 03:55:09");
INSERT INTO products_images VALUES("53","66","5c945cc1595c2-IMG_20181126_200353.png","1","2019-03-22 03:55:45","2019-03-22 03:55:45");
INSERT INTO products_images VALUES("54","67","5c9512556e9ec-IMG_20181126_200425.png","1","2019-03-22 16:50:29","2019-03-22 16:50:29");

DROP TABLE IF EXISTS  sugerencias;

CREATE TABLE `sugerencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sugerencias_user_id_foreign` (`user_id`),
  CONSTRAINT `sugerencias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO sugerencias VALUES("1","hola feliz tarde Quiero reportar la entrega de mi producto cuya orden fue generada el dia 26/12/2018 y aun no se me da ningúna respuesta","3","2019-01-20 22:01:23","2019-01-20 22:01:23");
INSERT INTO sugerencias VALUES("2","Y por consiguiente Quería Saber Cuanto debemos esperar la respuesta de parte del personal.","3","2019-01-20 22:14:44","2019-01-20 22:14:44");
INSERT INTO sugerencias VALUES("3","ya reporte el pago de la compra realizada","14","2019-02-12 23:13:57","2019-02-12 23:13:57");

DROP TABLE IF EXISTS  tipo_pagos;

CREATE TABLE `tipo_pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tipo_pagos VALUES("1","Deposito Bancario","1","","");
INSERT INTO tipo_pagos VALUES("2","Transferencia Electrónica","1","","");
INSERT INTO tipo_pagos VALUES("3","Efectivo","1","","");

DROP TABLE IF EXISTS  users;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nac_usr` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ced_user` int(15) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES("1","Yenderson Hernandez","","21464791","yendersonhernandez@gmail.com","","$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0412) 767-2042","SECTOR SAN JOAQUINDE TURMERO CRUCE CON LA PARROQUIA PEDRO AREVALO APONTE SECTOR BRISAS DEL LAGO. PUNTO DE REFERENCIA REFINERIA LA ANDORR PRECIOS FULL Y AL DETALL","2","","WAVMWoe75UIMq7esYomex1wXFaO6670UqqTwUMfXsRAfsCEmGJysoc7PCgQH","2019-01-12 01:57:47","2019-02-01 02:49:32");
INSERT INTO users VALUES("2","MARIA QUINTERO","","","mquintero@gmail.com","","$2y$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0412) 767-2042","URB LA ARBOLEDA CRUCE CON CACIQUE TEREPAIMA","1","","","2019-01-15 02:52:05","2019-01-15 02:52:05");
INSERT INTO users VALUES("3","MARIA QUINTERO","","","mq@gmail.com","","$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0414) 526-3256","AV URDANETA CRUCECON RONDON SEGÚN AV PRINCIPAL DE BELLO CAMPO","1","1","6jmQ1y3MSYzY4oDg8jtyaB4CD41HHQtlqXds8xKpE5L183R3dyOs2PsKruzA","2019-01-18 02:56:57","2019-01-24 04:31:59");
INSERT INTO users VALUES("4","JUAN GUAIDÓ","","","alx.morales@outlook.com","","$2y$10$YUjssMXDObb5T0K1d91K6ejLgexy.LWSpu7S9/H4tgBwMHZxzL.2u","(0414) 526-3256","TURMERO","1","","","2019-01-24 17:19:02","2019-01-24 17:19:02");
INSERT INTO users VALUES("5","MARIA QUINTERO","","20878654","MARIA_23@GMAIL.COM","","$2y$10$eHApmHNjCc4X1WQTN39oQO1kKDA2XHzSg8cXvqP8zcaM4fv4fi8XK","(0424) 568-9978","TURMERO","1","","","2019-01-29 01:32:24","2019-01-29 01:32:24");
INSERT INTO users VALUES("6","LISSETH ROJAS","","","glissethrojas_10@gmail.com","","$2y$10$5IFFnQPX2hwm0tZf6tJQo.3lNa.bVzf/2YU/k7Szk.mXA2ySS4lGK","(0414) 345-6798","TURMERO","1","","KoW6o6LKGnMtfJBej7q8AdnDLdvUMm9nmFijBmeG4q8lXOUHLoHoxgvtxC5p","2019-01-31 02:51:21","2019-01-31 02:51:21");
INSERT INTO users VALUES("9","Luisa Perez","V","26961450","luisaperez@gmail.com","","$2y$10$zOBzmRTkgnPrYjlacZxdReGqkoOujzw8kPWqR7sd4Pcl1pPiIoeT2","(0424) 374-2518","maracay","1","","","2019-02-01 03:21:11","2019-02-14 23:48:29");
INSERT INTO users VALUES("10","Genesis Perez M","V","25349743","gnesisperez@gmail.com","","$2y$10$p39n9d7kHMfHXWqDiQQOnuWASEMHQc6YZwIUkG.iKFartMMerY8z2","(0412) 798-3014","MARCAY","1","","","2019-02-01 04:02:16","2019-03-19 22:01:00");
INSERT INTO users VALUES("11","marilyn","V","24349732","marilyn@hotmail.com","","$2y$10$5n7fY4vyA1G7co9Wc1gH2Oo/PF9hkTAgNDBBKP3whVkWEH3pBoDxm","(0412) 678-7687","maracay","1","","","2019-02-01 22:16:25","2019-02-01 22:16:25");
INSERT INTO users VALUES("12","marylin","V","25349726","marylin@hotmail.com","","$2y$10$m.YKdvEto.tBBeeXaY7I2OSjTMc36AUn9m7Gs4OXfhoonOd/2YiVC","(0412) 562-7263","maracay","1","","","2019-02-01 22:17:56","2019-02-01 22:17:56");
INSERT INTO users VALUES("13","karla castillo","","","mary@hotmail.com","","$2y$10$.kcPKEfcEQB0a.keRs.5/.y2DgyXd15t1laCI2JLBeLAr7rWVR42W","(7826) 167-6721","caracas","1","","","2019-02-01 22:29:34","2019-02-01 22:29:34");
INSERT INTO users VALUES("14","yuraima","","","yuraima@hotmail.com","","$2y$10$PtabGI/jLM2b5DsbEtiEvex2zn7Y0ZvBTUSSpp5dhds9DchOcLyQW","(2388) 748-3843","valencia","1","","bT8qidg0Jo5Rlu7OzQRlmP93oPXxvQpzqQAAc037NJRmyyB34L616bULS3pT","2019-02-10 02:34:04","2019-02-10 02:34:04");
INSERT INTO users VALUES("15","Luis Salazar","","","luismsalazar24@gmail.com","","$2y$10$pss44qcBwENhQzW6Zhw6SeYVyzEXD32t97C1nWCmgS..XwEHo1MRS","(3476) 436-3464","maracay","4","","pCSX4nxzTNFFhqYTPtpcI565tyugG1OyZVOpdml3hMPCgyWxRNSACTTfKaMn","2019-02-12 23:41:55","2019-02-12 23:41:55");
INSERT INTO users VALUES("16","juliana gomez","","23456789","julianagomez@hotmail.com","","$2y$10$90emXSV2VMoQS0HGSfgYzu6CmcLMkNKdcCAOPMY5rGjv76PWtkHh6","(6732) 636-3636","maracay","1","","","2019-02-12 23:46:07","2019-02-12 23:46:07");
INSERT INTO users VALUES("17","Emma Rosales","","","Emmarosales@gmail.com","","$2y$10$CHL/I5IhE6ohYt6pOMFrYeObzWuFANsNeURy9enco7l2aLubAhVRq","(2424) 555-6510","Urbanizacion Montaña Fresca","2","","fZbE16c6AlvvoLyvucMNahX6yRY7g4QzKnnNxaYz2YDp3aGDFklSahSjqp0f","2019-03-17 16:46:20","2019-03-17 16:46:20");
INSERT INTO users VALUES("18","Genesis Perez","","","genesisperez@gmail.com","","$2y$10$6PZkbhmelUXJAYvjzafhNOsI1hqPPnfXARfJxvTMb4jEKkbr4/6u2","(0416) 232-1790","San mateo","1","5c94fe51eeb40-Capture20_47_17.jpg","RRMCLWOmzIbGHdJj0PNsVKX7VkxiwNFLcUDSZGwawaoc0XiwDS5n6DHqFzCE","2019-03-17 16:48:00","2019-03-22 15:25:05");
INSERT INTO users VALUES("19","MARIANELL PEREZ","","","MPEREZ@GMAIL.COM","","$2y$10$Hn3ovsacFr8kN33mjbJ6d.kMByr95YpBb54Dxx2aHvS5jYxM7HKpy","(0412) 345-6666","CARACAS","1","5c9178e778e8e-Koala.jpg","y01V3Rw8qNhe5IgPWQrBwAxcdxz4ExbSL5dIOvDOr28eaEJ6ammxeTPGNN42","2019-03-19 22:24:23","2019-03-19 23:19:03");
INSERT INTO users VALUES("20","Karla Castillo","","","karla_casti@hotmail.com","","$2y$10$r.UZBH9k.5owvCIlySUeEujJK3b/tdxOj5ocfy74eLORKl8Yi4uM6","(0412) 798-3014","Maracay","2","","","2019-03-22 03:37:23","2019-03-22 03:37:23");
INSERT INTO users VALUES("21","Marieglis Tinoco","","","iqui21@hotmail.com","","$2y$10$ipePMsnjd8p8PQtp4YPHlejC3Rh2sh35ZQiLYJHvOckg4EIwxZh1i","(0414) 444-3742","Santa Rita","3","","MEDNtMfStcLSfudhlekDh3h1Si4jBIKK0Qb4TXnEF2RxNOOlh0DeGPjKSPwb","2019-03-22 03:38:56","2019-03-22 03:38:56");
INSERT INTO users VALUES("22","Pedro Toussaint","","","pedrom@gmail.com","","$2y$10$klGUu9NT3YaX.LJakzGZLOlP.VG.frHjLTsu5TWZxcEeMufHQLqPK","(0424) 445-9272","Maracay","2","","","2019-03-22 03:39:53","2019-03-22 03:39:53");
INSERT INTO users VALUES("23","Eglismar Carvallo","","22189215","eglismar@hotmail.com","","$2y$10$LU53eM2iT7PlTI.GZv5loeGtipxASyB008IBJbZ8w.IEr94n3.R4u","(0416) 232-1790","Santa Rita","1","","xIu3dhf5eTgK2x2fLzBxi3eTv5zM4o5mSv6I4SKW5U8bN5Uzi2YvEEF7u8Ba","2019-03-22 04:07:24","2019-03-22 04:07:24");
INSERT INTO users VALUES("24","Belkis Rangel","V","4095139","belkisaida@gmail.com","","$2y$10$4FxgaEJzHrx3iD8Gt4WxTuc0ReAwoIIp0ZW7Vb4G7PG9HX1AmYDru","(0243) 217-6512","Maracay","1","","","2019-03-22 13:31:34","2019-03-22 13:31:34");
INSERT INTO users VALUES("25","carlos castillo","V","7199930","carloscastillo@gmail.com","","$2y$10$l2uJVjAH4MX0wnwIpMWK1uxZRCNmMpSDTGOOBL2ixCfcv1t555dUO","(0424) 734-2518","maracay","1","","","2019-03-22 13:33:51","2019-03-22 13:33:51");
INSERT INTO users VALUES("26","Mari Tinoco","V","16133221","tinoco23@hotmail.com","","$2y$10$JqEOyJujE.BnD12nmCGPdesIOtjwYrkLoyO7rrUcyVpZ30i/haenq","(0244) 390-4412","San mateo, estado aragua","1","","","2019-03-22 14:09:16","2019-03-22 14:09:16");
INSERT INTO users VALUES("27","Rut Veliz","V","20893983","rutveliz@hotmail.com","","$2y$10$Sm6H4TaDaQNplPF4WLwuIOQWo0DvKuCC/4d5KmzGz.ECnccKrmsNm","(0416) 233-1790","maracay","1","","","2019-03-22 14:37:31","2019-03-22 14:37:31");
INSERT INTO users VALUES("28","Luis Aponte","","","profesorluisaponte@hotmail.com","","$2y$10$cXMHoCKUgpZ5XNbc9xoyuOV/PFntxofZ0fKzKxwLuaPFw3UOWr5rG","(0412) 558-9933","Maracay","1","","PBqUPkwC89hCM9BxeTxiKZuaNFTMdN7rxA50ofq48spUFA0jZWtYH9ZVmEfI","2019-03-22 16:29:48","2019-03-22 16:29:48");
INSERT INTO users VALUES("29","Karol jimenez","V","1234567","karol@gmail.com","","$2y$10$PIK9uilIUB5NUpC28Hws8OkZr.KIqdsInNuaprAAhM2XKzUPcWPlq","(0243) 217-6512","MAracay","1","","","2019-03-22 16:54:11","2019-03-22 16:54:11");
INSERT INTO users VALUES("30","LILIANA BOLIVAR","V","27654345","mL@gmail.com","","$2y$10$WDpWMimYA4gIbZ19OkGAiO/JNuQftw8aRwZkt14Qm5aA/0k43WGbS","(4323) 344-4444","CARACAS","2","","","2019-03-31 16:11:20","2019-03-31 16:11:20");

