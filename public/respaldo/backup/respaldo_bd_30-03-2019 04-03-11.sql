DROP TABLE IF EXISTS  banco_pago;

CREATE TABLE `banco_pago` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_banco` varchar(250) DEFAULT NULL,
  `cuenta_filial` int(20) DEFAULT NULL,
  `user_create` int(11) DEFAULT NULL,
  `create_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS  bancos;

CREATE TABLE `bancos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cuenta` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO bancos VALUES("1","BANCO DEL CARIBE","1145236665","9","","2019-01-21 22:27:11");
INSERT INTO bancos VALUES("2","BANC DE VENEZUELA","523362222","1","","2019-01-21 04:13:08");
INSERT INTO bancos VALUES("5","BANCO BANESCO BANCO UNIVERSAL","","1","2019-01-21 04:26:06","2019-01-21 04:26:06");
INSERT INTO bancos VALUES("6","BANCO FONDO COMÚN","","1","2019-01-21 04:26:58","2019-01-21 04:26:58");
INSERT INTO bancos VALUES("7","BANCO PROVINCIAL","","1","2019-01-21 04:27:17","2019-01-21 04:27:17");
INSERT INTO bancos VALUES("8","BANCO MERCANTIL","","9","2019-01-21 04:27:38","2019-01-21 22:46:32");

DROP TABLE IF EXISTS  categories;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_user_id_foreign` (`user_id`),
  CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO categories VALUES("1","Maquillaje","9","1","2019-01-12 01:28:49","2019-01-30 06:03:26");
INSERT INTO categories VALUES("2","Cosmeticos","1","1","2019-01-12 01:28:58","2019-01-21 02:17:07");
INSERT INTO categories VALUES("3","BISUTERIA","1","1","2019-01-30 05:02:44","2019-01-30 05:02:44");
INSERT INTO categories VALUES("4","Ropa Interior","1","1","2019-01-30 05:04:37","2019-01-30 05:04:37");
INSERT INTO categories VALUES("5","Calzados","1","1","2019-01-30 05:35:46","2019-01-30 05:35:46");
INSERT INTO categories VALUES("6","Cosmeticos","1","1","2019-01-30 05:37:14","2019-01-30 05:37:14");

DROP TABLE IF EXISTS  compras_cart_detalles;

CREATE TABLE `compras_cart_detalles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `compras_carts_id` int(10) unsigned NOT NULL,
  `products_id` int(10) unsigned NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_cart_detalles_compras_carts_id_foreign` (`compras_carts_id`),
  KEY `compras_cart_detalles_products_id_foreign` (`products_id`),
  CONSTRAINT `compras_cart_detalles_compras_carts_id_foreign` FOREIGN KEY (`compras_carts_id`) REFERENCES `compras_carts` (`id`),
  CONSTRAINT `compras_cart_detalles_products_id_foreign` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO compras_cart_detalles VALUES("49","70","9","3","5300.00","16","2019-02-01 01:54:17","2019-02-01 01:54:17");
INSERT INTO compras_cart_detalles VALUES("50","70","1","1","3560.30","16","2019-02-01 01:54:17","2019-02-01 01:54:17");
INSERT INTO compras_cart_detalles VALUES("51","71","3","1","600.00","16","2019-02-05 02:23:19","2019-02-05 02:23:19");
INSERT INTO compras_cart_detalles VALUES("52","72","4","1","1500.00","16","2019-02-05 02:42:22","2019-02-05 02:42:22");
INSERT INTO compras_cart_detalles VALUES("53","73","1","1","3560.30","16","2019-02-06 02:51:54","2019-02-06 02:51:54");
INSERT INTO compras_cart_detalles VALUES("54","73","2","1","5630.00","16","2019-02-06 02:51:54","2019-02-06 02:51:54");
INSERT INTO compras_cart_detalles VALUES("55","74","4","2","1500.00","16","2019-02-15 21:34:09","2019-02-15 21:34:09");
INSERT INTO compras_cart_detalles VALUES("56","75","6","1","3500.00","16","2019-02-15 21:42:37","2019-02-15 21:42:37");
INSERT INTO compras_cart_detalles VALUES("57","75","7","1","78200.00","16","2019-02-15 21:42:37","2019-02-15 21:42:37");
INSERT INTO compras_cart_detalles VALUES("58","76","1","2","3560.30","16","2019-02-25 14:42:01","2019-02-25 14:42:01");
INSERT INTO compras_cart_detalles VALUES("59","77","1","2","3560.30","16","2019-03-13 23:46:30","2019-03-13 23:46:30");
INSERT INTO compras_cart_detalles VALUES("60","77","10","2","3000.00","16","2019-03-13 23:46:30","2019-03-13 23:46:30");
INSERT INTO compras_cart_detalles VALUES("61","78","4","1","1500.00","16","2019-03-19 02:41:26","2019-03-19 02:41:26");
INSERT INTO compras_cart_detalles VALUES("62","79","4","2","1500.00","16","2019-03-19 19:00:34","2019-03-19 19:00:34");

DROP TABLE IF EXISTS  compras_carts;

CREATE TABLE `compras_carts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nro_orden` int(11) NOT NULL,
  `annio_fiscal` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nro_factura` int(11) NOT NULL,
  `nro_control` int(11) NOT NULL,
  `fecha_compra` timestamp NOT NULL,
  `monto_compra` double(15,2) NOT NULL,
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` enum('1','9','3','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1',
  `envios_id` int(10) unsigned NOT NULL,
  `monto_envio` double(15,2) DEFAULT NULL,
  `fecha_pago` timestamp NULL DEFAULT NULL,
  `tipo_id` int(10) unsigned DEFAULT NULL,
  `ref_pago` int(11) DEFAULT NULL,
  `banco_id` int(10) unsigned DEFAULT NULL,
  `cajero` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `plataforma` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `compras_carts_user_id_foreign` (`user_id`),
  CONSTRAINT `compras_carts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO compras_carts VALUES("69","0","2018","0","0","2019-01-31 21:53:41","0.00","0","4","9","5","","","","","","","EN LINEA","","");
INSERT INTO compras_carts VALUES("70","1","2019","1","1","2019-02-01 01:54:17","22910.30","16","1","9","5","3450.00","2019-02-01 01:54:17","1","896652","7","KARLA KASTILLO","TIENDA FISICA","2019-02-01 01:54:17","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("71","2","2019","2","2","2019-02-05 02:23:18","600.00","16","3","9","4","7800.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-05 02:23:19","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("72","3","2019","3","3","2019-02-05 02:42:21","1500.00","16","3","9","9","0.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-05 02:42:22","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("73","4","2019","4","4","2019-02-06 02:51:54","9790.30","16","9","9","9","0.00","2019-02-06 02:51:54","1","896325","2","KARLA KASTILLO","TIENDA FISICA","2019-02-06 02:51:54","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("74","5","2019","5","5","2019-02-15 21:34:08","3000.00","16","10","9","6","6700.80","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-15 21:34:09","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("75","6","2019","6","6","2019-02-15 21:42:37","85150.00","16","9","9","5","3450.00","2019-02-15 21:42:37","3","8","2","KARLA KASTILLO","TIENDA FISICA","2019-02-15 21:42:37","2019-03-13 22:13:47");
INSERT INTO compras_carts VALUES("76","7","2019","7","7","2019-03-13 14:42:00","7120.60","16","1","9","6","6700.80","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-02-25 14:42:01","2019-03-19 03:11:20");
INSERT INTO compras_carts VALUES("77","8","2019","8","8","2019-03-13 23:46:29","100620.60","16","9","3","4","7800.00","2019-03-13 23:46:29","1","32563","2","KARLA KASTILLO","TIENDA FISICA","2019-03-13 23:46:29","2019-03-13 23:46:29");
INSERT INTO compras_carts VALUES("78","9","2019","9","9","2019-03-19 02:41:25","1500.00","16","10","9","4","7800.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-03-19 02:41:26","2019-03-30 12:13:39");
INSERT INTO compras_carts VALUES("79","10","2019","10","10","2019-03-19 19:00:33","3000.00","16","10","9","4","7800.00","","","","","CLIENTE","PLATAFORMA EN LINEA","2019-03-19 19:00:34","2019-03-30 12:13:39");

DROP TABLE IF EXISTS  envios;

CREATE TABLE `envios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(15,2) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO envios VALUES("4","RapiEnvios","7800.00","1","2019-01-22 00:59:29","2019-01-22 01:08:41");
INSERT INTO envios VALUES("5","Thealca","3450.00","1","2019-01-22 01:00:05","2019-01-22 01:00:05");
INSERT INTO envios VALUES("6","Liberty Express","6700.80","1","2019-01-22 01:00:29","2019-01-22 01:00:29");
INSERT INTO envios VALUES("7","Zoom","8000.00","1","2019-01-22 01:00:46","2019-01-22 01:00:46");
INSERT INTO envios VALUES("8","Acordar Con el Vendedor","0.00","1","","");
INSERT INTO envios VALUES("9","Retirar En Tienda Fisica","0.00","1","","2019-01-30 06:05:19");

DROP TABLE IF EXISTS  ivas;

CREATE TABLE `ivas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `porc` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO ivas VALUES("1","16","","");

DROP TABLE IF EXISTS  logs;

CREATE TABLE `logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `accion` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO logs VALUES("1","1","Creo Una Nueva Categoria LlamadaCosmeticos","2019-01-30 05:37:14","2019-01-30 05:37:14");
INSERT INTO logs VALUES("2","1","Creo Una Nueva Categoria Llamadavehiculos","2019-01-30 05:39:28","2019-01-30 05:39:28");
INSERT INTO logs VALUES("3","1","Eliminación de Categoriavehiculos","2019-01-30 06:02:37","2019-01-30 06:02:37");
INSERT INTO logs VALUES("4","1","Eliminación de Categoria vehiculos","2019-01-30 06:03:17","2019-01-30 06:03:17");
INSERT INTO logs VALUES("5","1","La Categoria Maquillaje Ha Sido Inhabilitada Satisfactoriamente","2019-01-30 06:03:26","2019-01-30 06:03:26");
INSERT INTO logs VALUES("6","1","Modificación de la Categoria Maquillaje","2019-01-30 06:03:59","2019-01-30 06:03:59");
INSERT INTO logs VALUES("7","1","Modificación de Agencia de Envio Retirar En Tienda Fisica","2019-01-30 06:04:48","2019-01-30 06:04:48");
INSERT INTO logs VALUES("8","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 00:57:38","2019-01-31 00:57:38");
INSERT INTO logs VALUES("9","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:00:00","2019-01-31 01:00:00");
INSERT INTO logs VALUES("10","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:00:35","2019-01-31 01:00:35");
INSERT INTO logs VALUES("11","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:41:21","2019-01-31 01:41:21");
INSERT INTO logs VALUES("12","1","Modificación del Usuario .KARLA KASTILLO","2019-01-31 01:52:18","2019-01-31 01:52:18");
INSERT INTO logs VALUES("13","3","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 60","2019-01-31 02:17:54","2019-01-31 02:17:54");
INSERT INTO logs VALUES("14","1","La Compra Número:3Ha Fue Eliminada","2019-01-31 02:52:43","2019-01-31 02:52:43");
INSERT INTO logs VALUES("15","1","La Compra Número:15Ha Fue Eliminada","2019-01-31 20:31:02","2019-01-31 20:31:02");
INSERT INTO logs VALUES("16","1","Creación del Usuario MARIANELL PEREZ","2019-01-31 20:44:21","2019-01-31 20:44:21");
INSERT INTO logs VALUES("17","1","Creación del Usuario JUAN VILORIA","2019-01-31 20:45:22","2019-01-31 20:45:22");
INSERT INTO logs VALUES("18","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número1","2019-02-01 01:54:17","2019-02-01 01:54:17");
INSERT INTO logs VALUES("19","3","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 2","2019-02-05 02:23:19","2019-02-05 02:23:19");
INSERT INTO logs VALUES("20","3","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 3","2019-02-05 02:42:22","2019-02-05 02:42:22");
INSERT INTO logs VALUES("21","1","Creción de un Nuevo Cliente de Nombre:PEDRO GUSTAVO HERNANDEZ BORGES","2019-02-06 02:51:53","2019-02-06 02:51:53");
INSERT INTO logs VALUES("22","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número4","2019-02-06 02:51:54","2019-02-06 02:51:54");
INSERT INTO logs VALUES("23","1","Modificación de Articulo|Producto Denominado:PINTURA LABIAL","2019-02-06 03:50:26","2019-02-06 03:50:26");
INSERT INTO logs VALUES("24","10","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 5","2019-02-15 21:34:09","2019-02-15 21:34:09");
INSERT INTO logs VALUES("25","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número6","2019-02-15 21:42:37","2019-02-15 21:42:37");
INSERT INTO logs VALUES("26","1","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 7","2019-02-25 14:42:01","2019-02-25 14:42:01");
INSERT INTO logs VALUES("27","1","Procesamiento De Compra en Tienda Fisíca, Bajo El Número8","2019-03-13 23:46:29","2019-03-13 23:46:29");
INSERT INTO logs VALUES("28","3","Modificación del Usuario .MARIA QUINTERO","2019-03-14 04:12:01","2019-03-14 04:12:01");
INSERT INTO logs VALUES("29","3","Modificación del Usuario .MARIA QUINTERO","2019-03-14 04:23:13","2019-03-14 04:23:13");
INSERT INTO logs VALUES("30","3","Modificación del Usuario .MARIA QUINTERO","2019-03-14 04:24:38","2019-03-14 04:24:38");
INSERT INTO logs VALUES("31","10","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 9","2019-03-19 02:41:26","2019-03-19 02:41:26");
INSERT INTO logs VALUES("32","10","El Cliente Proceso Su Compra Mediante La Plataforma En Linea, Orden: 10","2019-03-19 19:00:34","2019-03-19 19:00:34");

DROP TABLE IF EXISTS  migrations;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO migrations VALUES("10","2014_10_12_000000_create_users_table","1");
INSERT INTO migrations VALUES("11","2014_10_12_100000_create_password_resets_table","1");
INSERT INTO migrations VALUES("12","2018_12_18_030738_create_categories_table","1");
INSERT INTO migrations VALUES("13","2018_12_18_032522_create_products_table","1");
INSERT INTO migrations VALUES("14","2018_12_20_015820_create_products_images_table","1");
INSERT INTO migrations VALUES("15","2019_01_03_234633_create_compras_carts_table","1");
INSERT INTO migrations VALUES("16","2019_01_03_234950_create_compras_cart_detalles_table","1");
INSERT INTO migrations VALUES("17","2019_01_04_003117_create_bancos_table","1");
INSERT INTO migrations VALUES("18","2019_01_04_003226_create_tipo_pagos_table","1");
INSERT INTO migrations VALUES("19","2019_01_20_201233_create_sugerencias_table","2");
INSERT INTO migrations VALUES("20","2019_01_21_225233_create_envios_table","3");
INSERT INTO migrations VALUES("21","2019_01_23_032331_create_ivas_table","4");
INSERT INTO migrations VALUES("22","2019_01_30_045725_create_logs_table","5");

DROP TABLE IF EXISTS  password_resets;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS  products;

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `price` double(15,2) NOT NULL,
  `stock_min` int(11) NOT NULL,
  `stock_max` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `destacado` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `iva` int(11) DEFAULT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_category_id_foreign` (`category_id`),
  KEY `products_user_id_foreign` (`user_id`),
  CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products VALUES("1","1","TABLA DE MAQUILLAJE","MAQUILLAJE CONTENEDOR CON ESPECIFICACIONES DE GRAN INDOLE","45","3560.30","10","15","5c3a95281a788-IMG_20181126_201345.png","1","1","","1","2019-01-12 01:58:23","2019-03-13 23:46:30");
INSERT INTO products VALUES("2","1","ESTUCHE MULTICOLOR INTEGRADO","ESTUCHE COORPORTATIVO","26","5630.00","5","100","5c3ce4877c66e-IMG_20181126_202333.png","0","1","","1","2019-01-14 19:35:35","2019-02-06 02:51:54");
INSERT INTO products VALUES("3","1","labial principal","-","33","600.00","1","300","5c3ce4fb9dab1-IMG_20181126_195549.png","1","1","","1","2019-01-14 19:37:31","2019-02-05 02:23:19");
INSERT INTO products VALUES("4","2","ZAPATOS DEPORTVOS","--","75","1500.00","20","60","5c3ce52955090-IMG_20181126_195647.png","0","1","","1","2019-01-14 19:38:17","2019-03-19 19:00:34");
INSERT INTO products VALUES("5","2","BODY ROJO","--","19","5600.00","2","1","5c3ce85c63d4d-IMG_20181126_195627.png","0","1","","1","2019-01-14 19:52:00","2019-01-29 02:29:41");
INSERT INTO products VALUES("6","2","BODY TROPICAL","-","98","3500.00","5","100","5c3d35ee71ca7-IMG_20181126_201711.png","1","1","","1","2019-01-15 01:22:55","2019-02-15 21:42:37");
INSERT INTO products VALUES("7","2","BERMUDA FALDON","--","49","78200.00","1","100","5c3d36402451a-IMG_20181126_202155.png","1","1","","1","2019-01-15 01:24:16","2019-02-15 21:42:37");
INSERT INTO products VALUES("8","2","BLUSAS ESCOTEX","--","100","5000.00","1","100","5c3d366d699e2-IMG_20181126_201751.png","0","1","","1","2019-01-15 01:25:01","2019-01-15 01:25:01");
INSERT INTO products VALUES("9","2","PINTURA LABIAL","--","26","5300.00","1","10","5c3d36c02ff8d-IMG_20181126_195549.png","1","1","","1","2019-01-15 01:26:24","2019-02-06 03:50:25");
INSERT INTO products VALUES("10","1","DELINEADOR PRINCIPAL","--","0","3000.00","25","50","5c3d36ef135f2-IMG_20181126_195659.png","0","1","","1","2019-01-15 01:27:11","2019-03-13 23:46:31");

DROP TABLE IF EXISTS  products_images;

CREATE TABLE `products_images` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `producto_id` int(10) unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_images_producto_id_foreign` (`producto_id`),
  CONSTRAINT `products_images_producto_id_foreign` FOREIGN KEY (`producto_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO products_images VALUES("1","1","5c3949bf491d3-IMG_20181126_202333.png","0","2019-01-12 01:58:23","2019-01-12 01:58:23");
INSERT INTO products_images VALUES("2","2","5c3ce4877c66e-IMG_20181126_202333.png","1","2019-01-14 19:35:35","2019-01-14 19:35:35");
INSERT INTO products_images VALUES("3","3","5c3ce4fb9dab1-IMG_20181126_195549.png","1","2019-01-14 19:37:31","2019-01-14 19:37:31");
INSERT INTO products_images VALUES("4","4","5c3ce52955090-IMG_20181126_195647.png","1","2019-01-14 19:38:17","2019-01-14 19:38:17");
INSERT INTO products_images VALUES("5","5","5c3ce85c63d4d-IMG_20181126_195627.png","1","2019-01-14 19:52:00","2019-01-14 19:52:00");
INSERT INTO products_images VALUES("6","6","5c3d35ee71ca7-IMG_20181126_201711.png","1","2019-01-15 01:22:55","2019-01-15 01:22:55");
INSERT INTO products_images VALUES("7","7","5c3d36402451a-IMG_20181126_202155.png","1","2019-01-15 01:24:16","2019-01-15 01:24:16");
INSERT INTO products_images VALUES("8","8","5c3d366d699e2-IMG_20181126_201751.png","1","2019-01-15 01:25:01","2019-01-15 01:25:01");
INSERT INTO products_images VALUES("9","9","5c3d36c02ff8d-IMG_20181126_195549.png","1","2019-01-15 01:26:24","2019-01-15 01:26:24");
INSERT INTO products_images VALUES("10","10","5c3d36ef135f2-IMG_20181126_195659.png","1","2019-01-15 01:27:11","2019-01-15 01:27:11");

DROP TABLE IF EXISTS  sugerencias;

CREATE TABLE `sugerencias` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `leido` int(1) DEFAULT '0',
  `devuelto` int(1) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sugerencias_user_id_foreign` (`user_id`),
  CONSTRAINT `sugerencias_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO sugerencias VALUES("1","hola feliz tarde Quiero reportar la entrega de mi producto cuya orden fue generada el dia 26/12/2018 y aun no se me da ningúna respuesta","3","","","2019-01-20 21:31:23","2019-01-20 21:31:23");
INSERT INTO sugerencias VALUES("2","Y por consiguiente Quería Saber Cuanto debemos esperar la respuesta de parte del personal.","3","","","2019-01-20 21:44:44","2019-01-20 21:44:44");

DROP TABLE IF EXISTS  tipo_pagos;

CREATE TABLE `tipo_pagos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO tipo_pagos VALUES("1","Deposito Bancario","1","","");
INSERT INTO tipo_pagos VALUES("2","Transferencia Electrónica","1","","");
INSERT INTO tipo_pagos VALUES("3","Efectivo","1","","");

DROP TABLE IF EXISTS  users;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nac_usr` varchar(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ced_user` int(15) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefono` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO users VALUES("1","KARLA KASTILLO","","21464791","yendersonhernandez@gmail.com","","$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0412) 767-2042","SECTOR SAN JOAQUINDE TURMERO CRUCE CON LA PARROQUIA PEDRO AREVALO APONTE SECTOR BRISAS DEL LAGO. PUNTO DE REFERENCIA REFINERIA LA ANDORR PRECIOS FULL Y AL DETALL","2","","TfRu0ZpIs6Q7bYf2wI8ifIYi0yf0q8nFQNyo2HHGlSrgENLt90iVxfqxMosf","2019-01-12 01:27:47","2019-01-31 01:00:35");
INSERT INTO users VALUES("2","MARIA QUINTERO","","","mquintero@gmail.com","","$2y$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0412) 767-2042","URB LA ARBOLEDA CRUCE CON CACIQUE TEREPAIMA","1","","","2019-01-15 02:22:05","2019-01-15 02:22:05");
INSERT INTO users VALUES("3","MARIA QUINTERO","","","mq@gmail.com","","$2y$10$ndD1MOjmgcArBEUr0a/vee0iia1bPIM38Xy9c2NBmKDZ1s93qxAnS","(0414) 526-3256","AV URDANETA CRUCECON RONDON SEGÚN AV PRINCIPAL DE BELLO CAMPO","1","","Y3hFW6mxiq50DZ7KzPW2jpP4NikIUtBGwZCPOH03d34Su5xkC4ntFO7gChi8","2019-01-18 02:26:57","2019-03-14 04:24:38");
INSERT INTO users VALUES("4","JUAN GUAIDÓ","","","alx.morales@outlook.com","","$2y$10$YUjssMXDObb5T0K1d91K6ejLgexy.LWSpu7S9/H4tgBwMHZxzL.2u","(0414) 526-3256","TURMERO","1","","","2019-01-24 16:49:02","2019-01-24 16:49:02");
INSERT INTO users VALUES("5","MARIA QUINTERO","","20878654","MARIA_23@GMAIL.COM","","$2y$10$eHApmHNjCc4X1WQTN39oQO1kKDA2XHzSg8cXvqP8zcaM4fv4fi8XK","(0424) 568-9978","TURMERO","1","","","2019-01-29 01:02:24","2019-01-29 01:02:24");
INSERT INTO users VALUES("6","LISSETH ROJAS","","","glissethrojas_10@gmail.com","","$2y$10$5IFFnQPX2hwm0tZf6tJQo.3lNa.bVzf/2YU/k7Szk.mXA2ySS4lGK","(0414) 345-6798","TURMERO","1","","KoW6o6LKGnMtfJBej7q8AdnDLdvUMm9nmFijBmeG4q8lXOUHLoHoxgvtxC5p","2019-01-31 02:21:21","2019-01-31 02:21:21");
INSERT INTO users VALUES("7","MARIANELL PEREZ","","","MP@GMAIL.COM","","$2y$10$C0kQITYnyg3Z4Hj/X..88O3NYLmr2ifZsHemwlg25QzL3Pb.BNKD6","(0424) 345-6787","CAGUA","3","","0cDVAW39MKNF8eizT9ScTbonMqwg3IFIULIr2xq0ldZo5tka7TPLWinVbYfV","2019-01-31 20:44:20","2019-01-31 20:44:20");
INSERT INTO users VALUES("8","JUAN VILORIA","","","JV@GMAIL.COM","","$2y$10$y2tkQIMBK0/Chc8R8xYHaeblfZ9X1OFs08d8TrxmwCO6YbEBFY0gS","(0244) 667-8909","CIUDAD OJEDA","4","","","2019-01-31 20:45:22","2019-01-31 20:45:22");
INSERT INTO users VALUES("9","PEDRO GUSTAVO HERNANDEZ BORGES","V","8740300","PEDROHERNANDEZ@GMAIL.COM","","$2y$10$waoWYwxWNdgl2oDCRzAFOO8agFcrgOp0hSgQ8e9/FqAI1PB9Q/ORG","(0412) 563-2365","AV URDANETA SECTOR AVILA BLANCA","1","","","2019-02-06 02:51:53","2019-02-06 02:51:53");
INSERT INTO users VALUES("10","MARIANELL PEREZ","","","mperez@gmail.com","","$2y$10$rb71o88cJZYdqfVVBix4l.wggJISZi7eeEq.pUCFhWJOwiECO2fPq","(0414) 236-2632","CAGUA","1","","vMpqhls0xwKQR9GMUV68zDalQqtN0EpyqoQ2zk2jYzJh0C1ArFQLjtQP0onk","2019-02-15 21:33:32","2019-02-15 21:33:32");

