<!DOCTYPE html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

 

  <title>Respaldo web</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../admin/bower_components/font-awesome/css/font-awesome.min.css"><!-- DataTables -->
  <link rel="stylesheet" href="../admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../admin/dist/css/AdminLTE.min.css">

  <link rel="stylesheet" href="../admin/dist/css/skins/_all-skins.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
<?php include('navbar.php');?>
</header>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <!--aqui comienza todo-->
<section class="content-header">
      <h1>
        Copia de Seguridad 
        <small>Busqueda Avanzada</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Respaldo</a></li>
        <li class="active">Busqueda Avanzada</li>
      </ol>
    </section>

     <section class="content">
      
     <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           <center>
                                    <button class="btn btn-primary" onclick="GeneraR()">
                                      <i class="fa fa-check-circle"></i>
                                      Generar Respaldo
                                    </button>
                                    </center>

                                    <hr>


                                        <div class="col col-sm-12 " id="Cprin">

                                           
                                            
                                        </div>

        </div>

      </div>

    </section>

   <!--aqui termina-->
    <!-- /.content -->
  </div>
 
 
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<?PHP include('footer.php');?>

<!-- jQuery 3 -->
<script src="../admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="../admin/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>

</script>
<!-- Bootstrap 3.3.7 -->
<script src="../admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="../admin/bower_components/raphael/raphael.min.js"></script>
<script src="../admin/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="../admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="../admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="../admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="../admin/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="../admin/bower_components/moment/min/moment.min.js"></script>
<script src="../admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="../admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="../admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="../admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../admin/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../admin/dist/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="../admin/dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../admin/dist/js/demo.js"></script>
<!-- DataTables -->
<script src="../admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="../admin/dist/dropzone/fileinput.js'" type="text/javascript"></script>

<script src="../admin/dist/dropzone/locales/es.js'" type="text/javascript"></script>

<script src="../admin/plugins/input-mask/jquery.inputmask.js"></script>
<script src="../admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>

 <script src=../calendar/js/bootstrap-datepicker.js"></script>
    <!-- Languaje -->
<script src=../calendar/locales/bootstrap-datepicker.es.min.js"></script>


<script type="text/javascript">


    mostrar();
       function GeneraR(){
      if(confirm("Seguro que desea Generar Un Respaldo?")){
     $.ajax({url:'database.php'//ruta a donde queremos ir 
    ,type:'POST'//metodo 
    ,data:'id='
    ,success: function(data){
  
       alert('Respaldo generado Satisfactoriamente');
       mostrar();

    }
    ,error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
    }
    });

      }
  
    }


     function mostrar(){
     
     $.ajax({url:'busc_dir.php'//ruta a donde queremos ir 
    ,type:'POST'//metodo 
    ,data:'id='
    ,success: function(data){   

    $('#Cprin').html(data);
    }
    ,error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
    }
    });     
  
    }


    function ejec(archivo){

    if(confirm('Usted esta Seguro Que desea Restaurar La información Seleccionada??')){
    $('#bloquea').show();
    $('#Cprin').hide(); 

      $.ajax({url:'restore.php'//ruta a donde queremos ir 
    
    ,type:'POST'//metodo 
    ,data:'ruta='+ archivo
    ,success: function(data){ 
    $('#bloquea').hide();  
     alert(data)
   
   $('#Cprin').show(); 

    }
    ,error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.responseText);
            alert(thrownError);
    }
    });
  }
    }
</script>
</script> 
</body>
</html>
