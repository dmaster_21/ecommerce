@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">

       @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Agentes de Envios </strong></h3>
          <center><button class="btn btn-primary" data-toggle="modal" data-target="#data-register"><i class="fa fa-plus"></i>Nuevo Agente</button></center>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         <!--aqui va todo -->
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Agente de envios</th> 
                  <th>Precio Actualizado</th>                 
                  <th>Estatus</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                @foreach($envio as $envios)
                  
                   <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$envios->name}}</td>
                  <td>{{number_format($envios->price,'2',',','.')}}</td>
                  <td>
                   @if($envios->status==1)
                    <span class="badge bg-green">Activo</span>
                   @endif

                   @if($envios->status==9)
                    <span class="badge bg-red">InActivo</span>
                   @endif
                  </td>
                  <td>
                  @if($envios->status==1)
                    <a href="/envios/status/{{$envios->id}}/9" class="btn btn-default" title="Inactivar">Inactivar</a>
                   @endif

                    @if($envios->status==9)
                    <a href="/envios/status/{{$envios->id}}/1" class="btn btn-default" title="Activar">Activar</a>
                   @endif

                   
                   <button class="btn btn-info" data-toggle="modal" data-target="#data-edit{{$envios->id}}"><i class="fa fa-edit"></i></button>

                   <button class="btn btn-danger" data-toggle="modal" data-target="#data-delete{{$envios->id}}"><i class="fa fa-trash"></i></button>
                   
                  </td>                 
                </tr>


                  @endforeach
                
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                 
                </tr>
                </tfoot>
              </table>
    

        </div>

        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->


     <div class="modal fade" id="data-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">NUEVO AGENTE DE ENVIOS</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" action="{{action('EnviosController@store')}}" novalidate="">
         <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Nombre del envios</label>
                  <input type="text" class="form-control" name="nombre" id="name" value="{{ old('nombre') }}" placeholder="">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>

                   <label class="control-label" for="inputError">Costo Envio</label>
                  <input type="number" class="form-control" name="costo" id="costo" value="{{ old('costo') }}" placeholder="">
                   <span class="help-block">{{ $errors->first('costo') }}</span>
                   
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 <center>
                  <button class="btn btn-primary"> Enviar </button>

                 </center>
              </form>    
                </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>

 @foreach($envio as $envios)
<div class="modal fade" id="data-edit{{$envios->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Agente </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" action="{{action('EnviosController@update',$envios->id)}}">
         <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Nombre del Agente</label>
                  <input type="text" class="form-control" name="nombre" id="name" value="{{ $envios->name }}" placeholder="">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>

                   <label class="control-label" for="inputError">Costo Envio</label>
                  <input type="number" class="form-control" name="costo" id="costo" value="{{$envios->price}}" placeholder="">
                   <span class="help-block">{{ $errors->first('costo') }}</span>

                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 <center>
                  <button class="btn btn-primary"> Guardar Cambios </button>

                 </center>
              </form>    
                </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="data-delete{{$envios->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar envios ({{$envios->name}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar el Agente de Envio ?
      </center>
        <form action="{{action('EnviosController@destroy',$envios->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>


 @endforeach

@endsection


