			  @if (session('message'))
                <div class="alert alert-{{ session('class')}} alert-dismissible fade show" role="alert">

           			<i class="fa fa-info-circle "></i>   {{session('message')}}
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                    </button>
                   </div>
                   @endif