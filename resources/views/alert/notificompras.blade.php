  
               @if (session('mensaje'))
                <div class="alert alert-{{ session('class') }} alert-dismissible fade show" role="alert">

                <i class="fa fa-info-circle "></i>{{ session('mensaje') }}
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                    </button>
                   </div>
                      @endif


                       @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                   </div>
                    @endif