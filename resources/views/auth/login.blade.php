
@extends('layouts.login')
@section('content')
<div class="register-box box-primary" >
  <div class="register-logo">
    <a href="#"><b>Online</b>- Ecommerce</a>
  </div>

  <div class="register-box-body">
    <center>
    <img src="{{ asset('portal/images/logo.png') }}" style="width: 40%" >
     <br>
    <h4><b> Inicio de Sesion </b></h4>
  </center>
    

     
      <div class="form-group has-feedback">
        <form method="POST" action="{{ route('login') }}" novalidate="">
                        @csrf

                        <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="email" class="form-control" name="email" placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        <span class="help-block">{{ $errors->first('email') }}</span>
      </div>
      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }}">
        <input type="password" class="form-control" name="password" placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        <span class="help-block">{{ $errors->first('password') }}</span>

      </div> 

                      

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" >

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuerdame') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12 ">
                            	<center>
                                <button type="submit" class="btn btn-primary">
                                	<span class="glyphicon glyphicon-log-in "></span>
                                    {{ __('Inicio Sesion') }}
                                </button>
								<a href="/register" class="btn btn-default">
									<span class="glyphicon glyphicon-pencil "></span>                                
                                    {{ __('Registrarme') }}
                               
								</a>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Olvide Contraseña?') }}
                                    </a>
                                @endif
                                <br>

                                <a class="btn btn-link" href="/">
                                        {{ __('Ver Productos') }}
                                    </a>
                            </center>
                            </div>
                        </div>
                    </form>

       </div>
   

 

    


  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->
@endsection
