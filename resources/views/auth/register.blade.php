@extends('layouts.login')

@section('content')
<div class="register-box box-primary" >
  <div class="register-logo">
    <a href="#"><b>Online</b>- Ecommerce</a>
  </div>

  <div class="register-box-body">
    <center>
    <img src="{{ asset('portal/images/logo.png') }}" style="width: 40%" >
     <br>
    <h4><b> Inicio de Sesion </b></h4>
  </center>
    

     
      <div class="form-group has-feedback">
          <form method="POST" action="{{ route('register') }}" novalidate="">
                        @csrf

        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="name" placeholder="Nombre del Cliente">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('name') }}</span>
      </div>


        <div class="form-group has-feedback {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <input type="text" class="form-control " name="telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask>
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('telefono') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <textarea class="form-control  " name="direccion" placeholder="Domicilio Fiscal"></textarea> 
          <span class="glyphicon glyphicon-home form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('direccion') }}</span>
      </div>
      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="text" class="form-control " name="email" placeholder="Correo Electrónico">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
      </div>


      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }} ">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

       

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>

    <div class="row">
       
        <!-- /.col -->
        <div class="col-xs-12">
          <center>
          <button type="submit" class="btn btn-primary " ><span class="glyphicon glyphicon-log-in "></span> Crear Usuario</button>
          <a href="/" class="btn btn-primary "><span class="glyphicon glyphicon-arrow-left"></span> Volver</a>
       

         <br>
                                    <a class="btn btn-link" href="/login">
                                        {{ __('Login') }}
                                    </a>
                               </center>
        </div>
        <!-- /.col -->
      </div>



                        
          </form>

        </div>
   

 

    


  </div>
 
</div>

@endsection
