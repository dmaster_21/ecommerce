@extends('layouts.portal')

@section('content')

<div class="container">

	<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h4>Sumario de Productos</h4>
					</div>
				</div>

				<br>

  <div class="row">
<div class="col-md-12 card">
<div class="alert alert-info alert-dismissible fade show" role="alert">
     <i class="fa fa-info-circle "></i> Estimad@ Usario Queremos Informarte, Que nuestros Precios Incluyen IVA del {{$iva->porc}}%.
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                    </button>
                   </div>
    <div class="table-responsive">
      @if(count($cart))
      <br>
      <center>
        <a href="{{ route('cart.trash')}}">
          <button class="btn btn-danger"><i class="fa fa-trash "></i> Vaciar Pedido</button>
        </a>
      </center>
      <hr>

      <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$iva->porc}}%</th>
          <th>TOTAL</th>
          <th></th>

        </tr>
        <tbody>
          @foreach($cart as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{ $item->name }}</td>
          <td width="25px">
            <input type="number" class="form-control" min="1" max="100" value="{{ $item->qty }}" id="product_{{ $item->id }}" onchange="update('{{ $item->id }}')">
          </td>
         
          <td> {{ number_format(($item->price)-($item->price*$iva->porc/100),2,',','.') }}</td> 
          
          <td> {{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$iva->porc/100),2,',','.') }}</td>
          <td>{{number_format(($item->price*$iva->porc/100)*$item->qty,2,',','.')}}</td>
          <td> {{ number_format($item->price*$item->qty,2,',','.') }}</td>
          <th>
           <a href="{{ route('cart.delete',$item->id)}}"> <button class="btn btn-danger" title="Remover Articulo" ><i class="fa fa-trash"></i></button></a>
         </th>
          </th>
        </tr>

        
            
          @endforeach
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th>{{ number_format($total-($total*$iva->porc/100),2,',','.')}} </th>
          <th>{{ number_format($total*$iva->porc/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success'>BsS:  {{ number_format($total,2,',','.')}}</span></h4></th>
          <td></td>
          <th></th>
        </tr>
        </tfoot>
        
      </table>

       <br>
      <br>
      <center>
        <a href="/"><button class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver Al Catalogo</button></a>
        <a href="{{ route('cart.detail')}}">
        <button class="btn btn-primary"><i class="fa fa-chevron-right"></i> Procesar Pedido</button>
      </a>
      </center>
      @else
      <br>
      <br>
      <center>
        <i class="fa fa-warning fa-2x"></i>
      <h5><span class="label label-warning">No Hay Productos en El Carrito</span></h5>
      <br>
      
      <br>
       <a href="/"><button class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver Al Catalogo</button></a>
      </center>
      @endif
      <hr>
     
      
</div>
    </div>
    
    </div>

  </div>


  <script type="text/javascript">
    
    function update(id){
    
    var qty=$('#product_'+id).val();
    window.location.href = "/cart/update/"+id+"/"+qty+"";
    }
  </script>

@endsection


