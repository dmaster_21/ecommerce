@extends('layouts.portal')

@section('content')

<div class="container">
<hr>
  <div class="row">
  @include('layouts.navuser')
  <div class="col-sm-8">
    <div class="card">
      <div class="card-body"> 
        @include('alert.notificompras')
        
        

        <div class="col text-center">
          <div class="section_title new_arrivals_title">
            <h4>Tus Compras Realizadas</h4>
          </div>
          <hr>
          <button class="btn btn-primary btn-xs " title="Datos Bancarios" data-toggle="modal" data-target="#datosbanco"><i class="fa fa-check-circle"></i> Datos Bancarios</button>
         </div>
  <hr>

    
          <table class="table  table-striped">
            <thead>
              <tr>
                <th>#</th>
                <th>Nro Orden</th>
          
                <th>Fecha Compra</th>
                <th>Monto Total</th>
                <th>Estatus</th>
                <th></th>
              </tr>
            </thead>
        
            <tbody>

              @foreach($compra as $comp)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>
                  {{ str_pad($comp->nro_orden, 4, "0", STR_PAD_LEFT)}}


                 
                </td>
            
                <td>{{ date("d/m/Y", strtotime($comp->fecha_compra))}}</td>
                <td>{{number_format($comp->monto_compra + $comp->monto_envio,2,',','.')}}</td>
                <td>
                  @if($comp->status==1)
                  <span class="badge badge-primary">PENDIENTE</span>
                  @endif

                  @if($comp->status==2)
                  <span class="badge badge-warning">PAGO REPORTADO</span>
                  @endif

                  @if($comp->status==3)
                  <span class="badge badge-success">COMPRA FINALIZADA</span>
                  @endif

                  @if($comp->status==9)
                  <span class="badge badge-danger">COMPRA ANULADA</span>
                  @endif
                  

                </td>
                
                <td>
                  @if($comp->status!=9)

                   @if($comp->status==1)
                  <button class="btn btn-primary btn-xs " title="Reportar Pagos" data-toggle="modal" data-target="#data-pago{{$comp->id}}"><i class="fa fa-cc-mastercard"></i></button>
                  @endif
                  <button class="btn btn-success btn-xs " title="Ver Detalle"  data-toggle="modal" data-target="#data-vista{{$comp->id}}"><i class="fa fa-search"></i></button>
                   @if($comp->status==1)
                  <button class="btn btn-danger btn-xs " title="Anular Compra"  data-toggle="modal" data-target="#data-delete{{$comp->id}}"><i class="fa fa-trash"></i></button>
                  @endif
                @endif
                </td>
                
              </tr>
            
            @endforeach
            </tbody>
           
          </table>
          

        </div>    
        <hr> 
        <center>
         
        </center>
        <hr>
            
               
                  <!--end card body-->                 
                 </div>
               </div>

       
      </div>
    </div>
  </div>
</div>



</div>




<div class="modal fade" id="datosbanco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="datosbanco">Cuentas Bancarias </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body text-center">     
       <b>Nombre:</b> "ER ACCESORIOS & BOUTIQUE C.A"
       <br>                
        <b>Rif:</b> RIF:J-403591156
      <br>
      <b>Email:</b> ERACCESORIOS@GMAIL.COM
      <hr>
      
      <div class="text-center">
        <b>BANESCO BANCO UNIVERSAL</b>
        <br>
          CORRIENTE:0134-4567-7654-8897-0098
        <br>
        <b>BANCO DE VENEZUELA</b>
        <br>
        CORRIENTE:0102-2345-5646-8976
        <br>
        <b>BANCO FONDON COMÚN</b>
        <br>
        CORRIENTE:0102-2345-5646-8976
        <br>        
        <b>BANCO PROVICIAL</b>
        <br>
        CORRIENTE:0108-0989-6543-7656
        <br>
        <br>
        
      </div>

       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

@foreach($compra as $comp)
<!--modals de pago--->
<div class="modal fade" id="data-pago{{$comp->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Reportar Pago De La Compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form  action="{{action('ComprascartController@update',$comp->id)}}" method="POST">
        <input type="hidden" name="_method"value="PUT">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">
  <div class="form-group">
    <label for="exampleFormControlInput1">Monto Compra</label>
    <input type="text" class="form-control" name="monto" readonly="" value="{{number_format($comp->monto_compra + $comp->monto_envio ,'2',',','.')}}">
  </div>
  <div class="form-group">
    <label for="exampleFormControlSelect1">Forma de Pago</label>
    <select class="form-control {{ $errors->has('forma_pago') ? ' is-invalid' : '' }}" name="forma_pago">
      <option value="">-Indicar Forma de Pago-</option>
     @foreach($tipo as $tipos)
     <option value="{{$tipos->id}}">{{$tipos->name}}</option>

     @endforeach
    </select>

     @if ($errors->has('forma_pago'))
 <span class="invalid-feedback" role="alert">
       <strong>{{ $errors->first('forma_pago') }}</strong>
  
      @endif
  </div>
  <div class="form-group">
    <label for="example">Banco Receptor</label>
    <select class="form-control {{ $errors->has('banco') ? ' is-invalid' : '' }}" id="banco" name="banco">
      <option value="">-Seleccionar Pago-</option>
      @foreach($banco as $bancos)
     <option value="{{$bancos->id}}">{{$bancos->name}}</option>
     @endforeach
    </select>
     @if ($errors->has('banco'))
 <span class="invalid-feedback" role="alert">
       <strong>{{ $errors->first('banco') }}</strong>
  
      @endif
  </div>

  <div class="form-group">
    <label for="exampleFormControlSelect1">Nro De Comprobante</label>
    <input type="number" class="form-control {{ $errors->has('comprobante') ? ' is-invalid' : '' }}" name="comprobante" value="">
     @if ($errors->has('comprobante'))
 <span class="invalid-feedback" role="alert">
       <strong>{{ $errors->first('comprobante') }}</strong>

      @endif
  </div>
 <!--
  <div class="form-group">
    <label for="exampleFormControlTextarea1">Concepto de pago</label>
    <textarea class="form-control" name="concepto" rows="3"></textarea>
  </div>
-->
  <center>
  <a href="#" class="btn btn-secondary" data-dismiss="modal">Salir</a>
  <input type="submit" class="btn btn-primary" value="Reportar Pago">
</center>
</form>
    </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>


<!--modal de ver -->
<div class="modal fade" id="data-vista{{$comp->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalle de tu Compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >

         <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 15%" >
      </center>
        </div> 

     <div class="col-md-12">
                 
                      <center>
                     <p style="font-size: 20px"><strong>"ER ACCESORIOS & BOUTIQUE C.A"</strong></p> <hr>                
                      <p style="font-size: 15px"><strong>RIF:J-403591156</strong></p>
                     

                 </center>
               </div>
        <table class=" table table-striped">
          <thead>

            <tr>
              <th>Código</th>
              <td colspan="4">{{ str_pad($comp->nro_orden, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Nº Factura</th>
              <td colspan="4">{{ str_pad($comp->nro_factura, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Nº Control</th>
              <td colspan="4">{{ str_pad($comp->nro_control, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Fecha</th>
              <td colspan="4">{{ date("d/m/Y h:m:s", strtotime($comp->created_at))}}</td>
            </tr>
           
          </thead>
          
          
        </table>

             <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$iva->porc}}%</th>
          <th>TOTAL</th>
         

        </tr>
        <tbody>
          @foreach($comp->detalle as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$item->product->name }}</td>
          <td>
           {{ $item->qty }}
          </td>
         
          <td>{{ number_format(($item->price)-($item->price*$item->iva/100),2,',','') }}</td> 
          
          <td>{{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$item->iva/100),2,',','.') }}</td>
          <td>{{ number_format(($item->price * $item->qty)*($item->iva/100),2,',','.') }}</td>
          <td> {{ number_format(($item->price * $item->qty),2,',','.') }}</td>
          
          </th>
        </tr>

        
            
          @endforeach
          <!--envio-->
          <tr class="table-secondary">
            <th colspan="7">
          <center>  COSTO DE ENVIO</center>
          </th>
          </tr>

          <tr>
            <th colspan="4" >         
           
             ENVIO POR ({{ $comp->agency->name}})
            </th>
            <td id="tdsub">{{number_format(($comp->monto_envio)-($comp->monto_envio*$comp->iva/100),2,',','.')}}</td>
            <td id="tiva">{{number_format($comp->monto_envio*$comp->iva/100,2,',','.')}}</td>
            <td id="tot">{{number_format($comp->monto_envio,2,',','.')}}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th id="SUB">{{number_format(($comp->monto_compra + $comp->monto_envio)-(($comp->monto_compra + $comp->monto_envio)*$comp->iva/100),2,',','.')}}  </th>
          <th id="IV">{{number_format(($comp->monto_compra + $comp->monto_envio)*$comp->iva/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success' id="TT">
            BsS:  {{number_format(($comp->monto_compra + $comp->monto_envio),2,',','.')}}
          </span></h4>

         

          </th>
       
          <th></th>

        </tr>
        </tfoot>
        
      </table>

      <div class="text-center">
        <a href="/voucher/{{$comp->id}}" target="_black">
        <button class="btn btn-primary"><i class="fa fa-print"></i> Imprimir Comprobante</button>
      </a>
        
      </div>




       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>

<!--modals de anular-->
<div class="modal fade" id="data-delete{{$comp->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Anular Orden de Compra Nro ({{$comp->nro_orden}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar La Compra?
      </center>
        <form action="{{action('ComprascartController@destroy',$comp->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-secondary">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>
@endforeach
@endsection


