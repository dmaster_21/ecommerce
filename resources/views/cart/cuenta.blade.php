@extends('layouts.portal')

@section('content')

<div class="container">
<hr>
  <div class="row">
  @include('layouts.navuser')
  <div class="col-sm-8">


     <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 15%" >
      </center>
        </div> 

     <div class="col-md-12">
                 
                      <center>
                     <p style="font-size: 20px"><strong>"ER ACCESORIOS & BOUTIQUE C.A"</strong></p> <hr>                
                      <p style="font-size: 15px"><strong>RIF:J-403591156</strong></p>
                     

                 </center>
               </div>













    <div class="card">
      <div class="card-body">   
       @include('alert.notificompras') 
    <div class="alert alert-info alert-dismissible fade show" role="alert">

                <i class="fa fa-info-circle "></i> Sr(a) {{Auth::user()->name }} Bienvenid@ A Su Gestor De Compras en Linea
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                    </button>
                   </div>  

                   <hr>



                   <table class="table table-bordered">

                    <thead>
                      <tr>
                        <th colspan="4"><center><i class="fa fa-users"></i> Tus Datos Personales</center></th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <th> Nombres Y Apellido:</th>
                        <td colspan="3">{{Auth::user()->name }}</td>

                      
                      </tr>

                      <tr>
                        <th> Correo Electrónico:</th>
                        <td colspan="3">{{Auth::user()->email}}</td>

                      </tr>
                        <tr>
                        <th>Teléfono de Contacto:</th>
                        <td colspan="3">{{Auth::user()->telefono}}</td>
                      </tr>
                      <tr>
                        <th> Domicilio Fiscal :</th>
                        <td colspan="3">{{Auth::user()->direccion}}</td>

                        
                      </tr>
                    </tbody>
                     
                   </table>

                   <center>

                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                    <i class="fa fa-edit"></i>  
                    Editar Información                    
                    </button>
                     <button class="btn btn-danger" data-toggle="modal" data-target="#modalpassword">
                    <i class="fa fa-key "></i>  
                    Cambiar Contraseña                   
                    </button>


                   </center>
           
            
               
                  <!--end card body-->                 
                 </div>
               </div>

       
      </div>
    </div>
  </div>
</div>



</div>


<!--modal update-->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información De Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{action('UsersController@update',Auth::user()->id)}}" method="POST" novalidate="" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="PUT">

                        @csrf

        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}"> 
        <label>Nombre</label>      
        <input type="text" class="form-control " name="name" value="{{Auth::user()->name }}" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('name') }}</span>
      </div>


        <div class="form-group has-feedback {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <label>Teléfono</label>  
        <input type="text" class="form-control " name="telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask value="{{Auth::user()->telefono }}">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('telefono') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <label>Dirección</label>
        <textarea class="form-control  " name="direccion" >{{Auth::user()->direccion}}</textarea> 
          <span class="glyphicon glyphicon-home form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('direccion') }}</span>
      </div>
      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <label>Correo Electrónico</label>
        <input type="text" class="form-control " name="email"  value="{{Auth::user()->email }}" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
      </div>
   

       

      <div class="form-group has-feedback">
        <label>Imagen de Perfil</label>
        <input type="file" class="form-control " name="imagen"   >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  
      </div>
   
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit"  class="btn btn-primary" value="Guardar Cambios">

      </div>

    </form>
      </div>
    </div>
  </div>
</div>
<!--end modals-->



<div class="modal fade" id="modalpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cambiar Contraseña</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{action('UsersController@reset',Auth::user()->id)}}" novalidate="">
                        @csrf
             <input type="hidden" name="_method"value="GET">
            <input type="hidden" name="_token"value="{{ csrf_token()}}">


         <div class="form-group has-feedback">
          <label>Contraseña</label>
        <input type="password" class="form-control"  name="password" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         
      </div>

       

      <div class="form-group has-feedback " >
        <label>Confirmar Contraseña</label>
        <input type="password" class="form-control " name="password_confirmation">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
       

      </div>


       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
  </div>
</form>

      </div>
    </div>
  </div>
</div>
<!--end modals-->





@endsection


