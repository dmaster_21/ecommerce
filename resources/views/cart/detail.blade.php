@extends('layouts.portal')

@section('content')

<div class="container">

	<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h4>Procesar Pedido</h4>
					</div>
          <br>
          
				</div>

				<br>


  <div class="row">
   <div class="col-md-12 card">
     <div class="col-md-12 text-center">

          <div class="section_title new_arrivals_title">
            <h4>Datos del Cliente</h4>
          </div>
          <hr>

                <div class="col-md-12">

                 <div class="table-responsive">
                  <table class="table table-striped ">
                    <tr>
                      <td><i class="fa fa-user fa-2x"></i></td>
                      <th>Nombres Y Apellidos:</th>
                      <td>{{ Auth::user()->name }}</td>
                    </tr>
                    <tr>
                      <td><h5>@</h5></td>
                      <th>Correo Electrónico:</th>
                      <td>{{ Auth::user()->email }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-home fa-2x"></i></td>
                      <th>Domicilio Fiscal:</th>
                      <td>{{ Auth::user()->direccion }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-phone fa-2x"></i></td>
                      <th>Teléfono de Contacto:</th>
                      <td>{{ Auth::user()->telefono }}</td>
                    </tr>

                  </table>                    
                 </div>
               </div>


    <div class="col-md-12 text-center">
          <div class="section_title new_arrivals_title">
            <h4>Información del Pedido</h4>
          </div>
           <div class="col-md-12">
                 <div class="table-responsive">
     
      <hr>
      
      <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$iva->porc}}%</th>
          <th>TOTAL</th>
         

        </tr>
        <tbody>
          @foreach($cart as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{ $item->name }}</td>
          <td>
           {{ $item->qty }}
          </td>
         
          <td>{{ number_format(($item->price)-($item->price*$iva->porc/100),2,',','') }}</td> 
          
          <td>{{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$iva->porc/100),2,',','.') }}</td>
          <td> {{number_format(($item->price*$iva->porc/100)*$item->qty,2,',','')}}</td>
          <td> {{ number_format($item->price*$item->qty,2,',','') }}</td>
          
          </th>
        </tr>

        
            
          @endforeach
          <!--envio-->
          <tr class="table-secondary">
            <th colspan="7">Costo de Envio</th>
          </tr>

          <tr>
            <th colspan="4">
             <form action="{{action('CartController@proccess')}}" method="POST">

             <input type="hidden" name="_method"value="get">
             <input type="hidden" name="_token"value="{{ csrf_token()}}">


                   <select class="form-control {{ $errors->has('agente_envio') ? ' is-invalid' : '' }}" name="agente_envio" id="agente"
                    onchange="traeCosto()">
                    <option value="">Seleccione Agente De Envios</option>
                    @foreach($envio as $envios)
                    <option value="{{$envios->id}}">{{$envios->name}}</option>
                    @endforeach          
                   </select>

      @if ($errors->has('agente_envio'))
       <span class="invalid-feedback" role="alert">
       <strong>{{ $errors->first('agente_envio') }}</strong>
  
      @endif
  
    
            </th>
            <th id="tdsub">0,00</th>
            <th id="tiva">0,00</th>
            <th id="tot">0,00</th>
          </tr>
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th id="SUB">{{ number_format($total-($total*$iva->porc/100),2,',','.')}} </th>
          <th id="IV">{{ number_format($total*$iva->porc/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success' id="TT">BsS:  {{ number_format($total,2,',','.')}}</span></h4>

          <input type="hidden" id="subC" value="{{ number_format($total-($total*$iva->porc/100),2,',','')}}">
          <input type="hidden" id="ivC" value="{{ number_format($total*$iva->porc/100,2,',','')}}">
          <input type="hidden" id="ToC" value="{{ number_format($total,2,',','')}}">
          <input type="hidden" name="" id="iva" value="{{$iva->porc}}">

          </th>
       
          <th></th>

        </tr>
        </tfoot>
        
      </table>


       <br>
      <br>
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <center>
        <a href="/cart/show" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver Al Carrito</a>
        
        <button class="btn btn-success"><i class="fa fa-check-circle"></i> Confirmar Compra</button>
      
      </center>
      </form>
      <hr>


     
                            </div>
                        </div>
                   </div>    
               </div>
         </div>


  <script>
    
  function traeCosto(){
 
    agente=$('#agente').val();
subC=$('#subC').val();
ivC=$('#ivC').val();
ToC=$('#ToC').val();
iva=$('#iva').val();

 $.ajax({
                type: 'GET',//metoodo 
                url: '/envio/'+agente,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) { 
                sbc=parseFloat(data.price)-(parseFloat(data.price)*parseFloat(iva)/100);
                ivc=parseFloat(data.price)*parseFloat(iva)/100;
                tots=parseFloat(sbc)+parseFloat(ivc);
               // sb1=sbc.toFixed(2);
                 SUB=parseFloat(subC)+parseFloat(sbc);
                 IV=parseFloat(ivC)+parseFloat(ivc);

                 TOT=parseFloat(ToC)+parseFloat(data.price);
                
                $('#tdsub').text(sbc.toFixed(2)); 
                $('#tiva').text(ivc.toFixed(2)); 
                $('#tot').text(data.price.toFixed(2));   

                $('#SUB').text(SUB.toFixed(2)); 
                $('#IV').text(IV.toFixed(2)); 
                $('#TT').text('BsS:'+TOT.toFixed(2));              
             
                }
            });
}

  </script>

@endsection


