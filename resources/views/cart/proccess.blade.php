@extends('layouts.portal')

@section('content')

<div class="container">

	<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h4>Procesar Pedido</h4>
					</div>
          <br>
          
				</div>

				<br>


  <div class="row">
   <div class="col-md-12 card">
     <div class="col-md-12 text-center">

          <div class="section_title new_arrivals_title">
            <h4>Datos del Cliente</h4>
          </div>
          <hr>

                <div class="col-md-12">

                 <div class="table-responsive">
                  <table class="table table-striped ">
                    <tr>
                      <td><i class="fa fa-user fa-2x"></i></td>
                      <th>Nombres Y Apellidos:</th>
                      <td>{{ Auth::user()->name }}</td>
                    </tr>
                    <tr>
                      <td><h5>@</h5></td>
                      <th>Correo Electrónico:</th>
                      <td>{{ Auth::user()->email }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-home fa-2x"></i></td>
                      <th>Domicilio Fiscal:</th>
                      <td>{{ Auth::user()->direccion }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-phone fa-2x"></i></td>
                      <th>Teléfono de Contacto:</th>
                      <td>{{ Auth::user()->telefono }}</td>
                    </tr>

                  </table>                    
                 </div>
               </div>


    <div class="col-md-12 text-center">
          <div class="section_title new_arrivals_title">
            <h4>Información del Pedido</h4>
          </div>
           <div class="col-md-12">
                 <div class="table-responsive">
     
      <hr>
      
      <table class="table table-bordered">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>
          <th>Sub-Total</th>
        
        </tr>
        <tbody>
          @foreach($cart as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{ $item->name }}</td>
          <td>
            <h5><span class='badge badge-secondary'>
            {{ $item->qty }}
          </span></h5>
          </td>
          <td>BsS: {{ number_format($item->price,2,',','.') }}</td>
          <td>BsS: {{ number_format($item->price * $item->qty,2,',','.') }}</td>
         
        </tr>

        
            
          @endforeach
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th>Total Pedido:</th>
          <td><h4><span class='badge  badge-success'>BsS:  {{ number_format($total,2,',','.')}}</span></h4></td>
          
        </tr>
        </tfoot>
        
      </table>

       <br>
      <br>
      <center>
        <a href="/cart/show"><button class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver Al Carrito</button></a>
        <a href="{{ route('cart.show')}}">
        <button class="btn btn-success"><i class="fa fa-check-circle"></i> Confirmar Compra</button>
      </a>
      </center>
      
      <hr>
     
                            </div>
                        </div>
                   </div>    
               </div>
         </div>


  <script type="text/javascript">
    
    function update(id){
    alert('Su Solicitud se esta procesando Espere');
    var qty=$('#product_'+id).val();
    window.location.href = "/cart/update/"+id+"/"+qty+"";
    }
  </script>

@endsection


