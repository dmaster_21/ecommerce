@extends('layouts.portal')

@section('content')

<div class="container">
<hr>
  <div class="row">
  @include('layouts.navuser')
  <div class="col-sm-8">
    <div class="card">
      <div class="card-body">          
            
               <div class="col-md-12">
                 @include('alert.notification')
              
      
                 <div class="table-responsive">
                     <table class="table table-striped ">
                       <thead>
                      <tr class="table-danger">
                        <th colspan="4"><center>DATOS DE LA COMPRA</center></th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td><b>Nro Orden:</b> {{$compra->nro_orden}}</td>
                        <td><b>Nro Factura:</b>{{$compra->nro_factura}}</td>
                        <td><b>Nro Control:</b>{{$compra->nro_control}}</td>
                        <td><b>Fecha Compra:</b> {{ date("d/m/Y", strtotime($compra->fecha_compra))}}</td>
                      </tr>
                      
                    </tbody>

                     </table>


                  <table class="table table-striped ">
                    <thead>
                      <tr class="table-danger">
                        <th colspan="3"><center>DATOS DEL CLIENTE</center></th>
                      </tr>
                    </thead>
                    <tr>
                      <td><i class="fa fa-user "></i></td>
                      <th>Nombres Y Apellidos:</th>
                      <td>{{ Auth::user()->name }}</td>
                    </tr>
                    <tr>
                      <td><h5>@</h5></td>
                      <th>Correo Electrónico:</th>
                      <td>{{ Auth::user()->email }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-home"></i></td>
                      <th>Domicilio Fiscal:</th>
                      <td>{{ Auth::user()->direccion }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-phone"></i></td>
                      <th>Teléfono de Contacto:</th>
                      <td>{{ Auth::user()->telefono }}</td>
                    </tr>

                  </table>   


                   <table class="table table-bordered">
                     <thead>
                      <tr class="table-danger">
                        <th colspan="5"><center>DATOS DEL PEDIDO</center></th>
                      </tr>
                    </thead>
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>
          <th>Sub-Total</th>
        
        </tr>
        <tbody>
          @foreach($cart as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{ $item->name }}</td>
          <td>
            <h5><span class='badge badge-secondary'>
            {{ $item->qty }}
          </span></h5>
          </td>
          <td>BsS: {{ number_format($item->price,2,',','.') }}</td>
          <td>BsS: {{ number_format($item->price * $item->qty,2,',','.') }}</td>
         
        </tr>

        
            
          @endforeach
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th>Total Pedido:</th>
          <td><span class='badge  badge-success'>BsS:  {{ number_format($total,2,',','.')}}</span></td>
          
        </tr>
        </tfoot>
        
      </table>





                  <!--end card body-->                 
                 </div>
               </div>

       
      </div>
    </div>
  </div>
</div>



</div>



@endsection


