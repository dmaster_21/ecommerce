@extends('layouts.portal')

@section('content')

<div class="container">
<hr>
  <div class="row">
  @include('layouts.navuser')
  <div class="col-sm-8">
    <div class="card">
      <div class="card-body"> 
      @include('alert.notificompras')    

        <div class="col text-center">
          <div class="section_title new_arrivals_title">
            <h4>SUGERENCIAS | RECLAMOS</h4>
          </div>
         </div>
  <hr>
          
          <form action="{{action('SugerenciasController@store')}}" method="POST">

         <input type="hidden" name="_method"value="POST">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

              <div class="form-group">
               <label for="exampleFormControlInput1">Fecha</label> 
               <input type="text" class="form-control" name="fecha" readonly="" value="{{now()}}"> 
              </div>

              <div class="form-group">
               <label for="exampleFormControlInput1">Descripción</label> 
               <textarea class="form-control {{ $errors->has('descripcion') ? ' is-invalid' : '' }}" name="descripcion"></textarea>  
                @if ($errors->has('descripcion'))
 <span class="invalid-feedback" role="alert">
       <strong>{{ $errors->first('descripcion') }}</strong>
  
      @endif 
              </div>

              <hr>
              <center>

              
              <input type="submit" name="" class="btn btn-primary">
              <input type="reset" name="" class="btn btn-default">
            </center>
          </form>
                  <!--end card body-->    

            <hr>
            <table class="table table-hover">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Fecha</th>                 
                  <th>Descripcción</th>
                </tr>
              </thead>
              @if(count($suger)>0)
                <tbody>
                 @foreach($suger as $Suger)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{ date("d/m/Y", strtotime($Suger->created_at))}}</td>
                  <td>{{$Suger->description}}</td>
                </tr>



                 @endforeach
                 @else

                 
                  <div class="alert alert-info alert-dismissible fade show" role="alert">

                <i class="fa fa-info-circle "></i> Usted No Ha Generado Reclamos
                 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                     <span aria-hidden="true">&times;</span>
                    </button>
                   </div>



                 @endif
                </tbody>
                
            
              
            </table>             
                 </div>
               </div>

       
      </div>
    </div>
  </div>
</div>



</div>



@endsection


