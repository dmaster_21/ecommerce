
@extends('layouts.portal')

@section('content')


<!-- New Arrivals -->

	<div class="new_arrivals">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h2>Nuestro Catalogo</h2>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="col text-center">
					<div class="new_arrivals_sorting">
						<ul class="arrivals_grid_sorting clearfix button-group filters-button-group">

							<li class="grid_sorting_button button d-flex flex-column justify-content-center align-items-center active is-checked" data-filter="*">Todas</li>
							
							@foreach($category as $Cat)

							<li class="grid_sorting_button button d-flex flex-column justify-content-center align-items-center" data-filter=".{{$Cat->id}}">{{ $Cat->name }}</li>
							
							@endforeach

						</ul>
					</div>
				</div>
			</div>


<div class="row">
				<div class="col">
					<div class="product-grid" data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'>
						<center>

						@foreach($producto as $prod)
						
						
						<div class="product-item {{$prod->category_id}}" style="width: 20%;left: 10px;">
							<div class="product product_filter">
								<div class="product_image">
									<img src="{{ asset('images/productos/'.$prod->image) }}" alt="">
								</div>
								<div class="favorite"></div>
								<!--<div class="product_bubble product_bubble_left product_bubble_green d-flex flex-column align-items-center"><span>new</span></div>-->
								<div class="product_info">
									<h6 class="product_name">{{ $prod->name }}</h6>
									<div class="product_price">Precio: {{ number_format($prod->price,2,',','.') }}</div>
									<div class="product_qty">Cant: {{ $prod->qty }}</div>
									<div class="product_info">
										   <a href="{{ route('productos.detalle',$prod->id) }}"><button class="btn btn-primary"><i class="fa fa-eye"></i></button></a>
										<button class="btn btn-warning"><i class="fa fa-star"></i></button>
										<a href="{{ route('cart.add',$prod->id) }}">
										<button class="btn btn-success" title="Añadir a Carrito"><i class="fa fa-shopping-cart"></i></button>
									</a>
										


									</div>
								</div>
							</div>
							<!--<div class="red_button add_to_cart_button"><a href="#">Añadir</a></div>-->
						</div>

						@endforeach

						


                    </center>

					</div>
				</div>
			</div>
		</div>



	<div class="benefit">
		<div class="container">
			<div class="row benefit_row">
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>Envios Nacionales</h6>
							<p>Para Tu Comodidad estamos afiliados a Las diferentes, Empresas de envios.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>Modos de Pago</h6>
							<p>Contamos con Excelente Modalidad de Pago</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>Devolución de Mercancía</h6>
							<p>Para Tu Comodidad Podemos Gestionar Tus Devoluciones</p>
						</div>
					</div>
				</div>
				<div class="col-lg-3 benefit_col">
					<div class="benefit_item d-flex flex-row align-items-center">
						<div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
						<div class="benefit_content">
							<h6>Tienda Fisica </h6>
							<p>de Lunes A Sabado 9AM - 06PM</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@endsection