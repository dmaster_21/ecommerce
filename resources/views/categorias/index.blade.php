@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Categorias 
        <small>(Productos)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Categorias</li>
      </ol>
    </section>

     <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">


          <div class="col-md-6">
             <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               @if (session('message'))
                   <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif
            <form method="POST" action="categorias" novalidate="">
               @csrf
                 <div class="form-group {{ $errors->has('Categoria') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Definir Categoria</label>
                  <input type="text" class="form-control" name="Categoria" id="Categoria" value="{{ old('Categoria') }}" placeholder="">
                   <span class="help-block">{{ $errors->first('Categoria') }}</span>
                   
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 
                  
                </div>

                <hr>
                  <center>
                <button type="submit" class="btn btn-primary" ><i class="fa fa-check-circle"></i> Guardar Categoria</button>

                <button type="reset" class="btn btn-default" ><i class="fa fa-trash"></i>Limpiar</button>
                  </center>
              </form>
            </div>
          </div>
          </div>
      
    
        <!--end col-md-6-->

        <div class="col-md-6">
           <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">Categorias Creadas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th >#</th>
                  <th>Descripción</th>
                  <th>Status</th>
                  <th></th>
                </tr>
                @foreach ($Category as $Cat)
                  
                
                <tr>
                  <td>{{ $Cat->id }}</td>
                  <td>{{ $Cat->name }}</td>
                  <td>                    
                    @if($Cat->status==1)
                    <span class="badge bg-green">Habilitada</span>
                    @endif

                    @if($Cat->status==9)
                    <span class="badge bg-red">Inhabilitada</span>
                    @endif

                    
                  </td>
                  <td>
                    <center>
                      @if($Cat->status==1)
                   <a href="{{ route('categorias.edit',$Cat->id) }}" class="btn btn-default " >Inactivar</a>
                    @endif

                    @if($Cat->status==9)
                    <a href="{{ route('categorias.edit',$Cat->id) }}"> 
                    <button class=" btn btn-default  ">Activar</button>
                  </a>
                    @endif
                
                    <button class=" btn btn-info  " title="Editar" data-toggle="modal" data-target="#exampleModal{{$Cat->id}}" ><i class="fa fa-edit"></i></button>

                  <!--
                    <a href="{{ route('categorias.destroy',$Cat->id) }}" >
                      <button class=" btn btn-danger  " title="Eliminar Categoria" ><i class="fa fa-trash"></i></button>


                    </a>-->

                     <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#data-delete{{$Cat->id}}" ><i class="fa fa-trash"></i></a>
    
                    </center>
                  </td>
                </tr>
                
                @endforeach
                
              </table>

             
            </div>
            <!-- /.box-body -->
            <div class="box-footer clearfix">
              {{ $Category->links()}}
            </div>
          </div>
          <!-- /.box -->
          
        </div>
       
      </div>
      <!-- /.box -->

    </section>



<!-- Modal -->
@foreach ($Category as $Cat)
<div class="modal fade" id="exampleModal{{$Cat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Editar Categoria</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="POST" action="{{action('CategorysController@update',$Cat->id)}}" novalidate="">
               @csrf
          <div class="form-group {{ $errors->has('Categoria') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Nombre Categoria</label>
                  <input type="text" class="form-control" name="Categoria" id="categoria" value="{{$Cat->name}}" >

                   <span class="help-block">{{ $errors->first('Categoria') }}</span>
                   
                    <input type="hidden" name="_method"value="PUT">
                    <input type="hidden" name="_token"value="{{ csrf_token()}}">
                 
                  
                 
                  
                </div>

                <hr>
                  <center>
                <button type="submit" class="btn btn-primary" ><i class="fa fa-check-circle"></i> Guardar Cambios</button>

          
                  </center>
              </form>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="data-delete{{$Cat->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar La Categoria ({{$Cat->name}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar La Categoria ?
      </center>
        <form action="{{action('CategorysController@destroy',$Cat->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>


@endforeach
<!--cierro div-->    

<script type="text/javascript">

  function llenado(id,name){

    $('#categoria').val(name);
    $('#id_cat').val(id_cat);
  
  }
  
</script>

@endsection


