
@extends('layouts.portal')

@section('content')


<!-- New Arrivals -->

  <div class="container contact_container">
    <div class="row">
      <div class="col">

       

    <div class="row">
      <div class="col-sm-12">
        <div id="google_map">
          <div class="map_container">
            <div id="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7852.3154434481185!2d-67.57107025384522!3d10.24886055516729!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e803ca7eafdddc7%3A0x8e1e5a45ef5345ca!2sUrbanizaci%C3%B3n+Parque+Aragua%2C+Maracay+2101%2C+Aragua!5e0!3m2!1ses!2sve!4v1548902869268" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>



            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Contact Us -->

    <div class="row">

      <div class="col-lg-12 contact_col">
        <div class="contact_contents">
            <div class="section_title new_arrivals_title">
              <center>
          <h4>Contactanos</h4>
        </center>
        </div>
        <hr>
          <p>Para Mayor Información Y suscribirte A nuestro Catalogo Mensual. Puedes Contactarnos Y Te ayudaremos A emprender en El negocio de la moda Digital, Gracias Por Preferirnos</p>
          <div>
            <p>(0243) 661-34-54 / (0243) 456-78-98</p>
            <p>tuboutiqueenlinea.com</p>
          </div>
          <div>
            <p></p>
          </div>
          <div>
            <p>Horario Laboral: 09:00 AM - 06:00 PM</p>
            <p>LUNEAS A SABADO </p>
          </div>
        </div>

       
      </div>
    </div>


    

<!-- Benefit -->

  <div class="benefit">
    <div class="container">
      <div class="row benefit_row">
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-truck" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Emvios Nacionales</h6>
              <p>Para Tu Comodidad estamos afiliados a Las diferentes, Empresas de envios.</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-money" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Modos de Pago</h6>
              <p>Contamos con Excelente Modalidad de Pago</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-undo" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Devolución de Mercancía</h6>
              <p>Para Tu Comodidad Podemos Gestionar Tus Devoluciones</p>
            </div>
          </div>
        </div>
        <div class="col-lg-3 benefit_col">
          <div class="benefit_item d-flex flex-row align-items-center">
            <div class="benefit_icon"><i class="fa fa-clock-o" aria-hidden="true"></i></div>
            <div class="benefit_content">
              <h6>Tienda Fisica </h6>
              <p>de Lunes A Sabado 9AM - 06PM</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  @endsection