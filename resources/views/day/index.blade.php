@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Dia de Anulación
        <small>Compra</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Dia</li>
      </ol>
    </section>

     <section class="content">
      <div class="col-md-3"></div>
      <div class="col-md-6">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">


          <div class="col-md-12">
             <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
               @if (session('message'))
                   <div class="callout callout-success">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif
              <form action="{{action('dayController@update',$day->id)}}" method="POST">
                 <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token()}}">
                <table class="table table-hover">
                  <tr>                 
                    <th>Número de Días</th>
                  </tr>
                  <tr>                  
                    <th>
                      <div class="form-group {{ $errors->has('dia') ? 'has-error' : '' }}">

                      <input type="text" class="form-control" name="dia" value="{{$day->nroday}}" required="" onkeypress="return soloNumeros(event);">
                       <span class="help-block">{{ $errors->first('dia') }}</span>


                    </th>
                  </tr>
                </table>
                <center>
                  <button class="btn btn-primary"><i class="fa fa-check"></i> Modificar</button>
                </center>
              </form>
            
       

        
          
        </div>
       
      </div>
      <!-- /.box -->
    </div>

    </section>

@endsection


