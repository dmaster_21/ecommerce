@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        Despacho 
        <small>Historico Despacho</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Despacho</a></li>
        <li class="active">Historico Despacho</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif

            <form action="/procesabusqueda" method="GET">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('desde') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Desde:</label>
                  <input type="text" class="form-control datepicker" id="desde" name="desde" value="" readonly="">
                   <span class="help-block">{{ $errors->first('desde') }}</span>
                </div>
              </div>
               <div class="col-md-3">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Hasta:</label>
                  <input type="text" class="form-control datepicker" id="hasta" name="hasta" value="" readonly="">
                   <span class="help-block">{{ $errors->first('hasta') }}</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Agencia Envio:</label>
                  <select class="form-control" id="envio" name="envio" >
                    <option>-seleccione-</option>
                    @foreach($envio as $envios)
                    <option value="{{$envios->id}}">{{$envios->name}}</option>


                    @endforeach
                  </select>
                   <span class="help-block">{{ $errors->first('envio') }}</span>
                </div>
              </div>
              <hr>
                  <div class="col-md-12 text-center">
                    <button class="btn btn-primary"><i class='fa fa-check'></i> Buscar Despachos</button>
                  </div>
                </form>
              <hr>
              <hr>
              <br>
              <br>

           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>               
                  <th>Nota</th>
                </tr>
                </thead>
                <tbody>
                 </tbody>
                <tfoot>
                 <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>               
                  <th>Nota</th>
                </tr>
                </tfoot>
              </table>
      </div>
      <!-- /.box -->
        
    </section>






@endsection







