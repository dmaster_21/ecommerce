@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        Despacho 
        <small>Historico Despacho</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Despacho</a></li>
        <li class="active">Historico Despacho</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif


            <form action="/procesabusqueda" method="GET">
            <div class="col-md-3">
                <div class="form-group {{ $errors->has('desde') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Desde:</label>
                  <input type="text" class="form-control datepicker" id="desde" name="desde" value="{{$desde}}" readonly="">
                   <span class="help-block">{{ $errors->first('desde') }}</span>
                </div>
              </div>
               <div class="col-md-3">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Hasta:</label>
                  <input type="text" class="form-control datepicker" id="hasta" name="hasta" value="{{$hasta}}" readonly="">
                   <span class="help-block">{{ $errors->first('hasta') }}</span>
                </div>
              </div>

              <div class="col-md-6">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Agencia Envio:</label>
                  <select class="form-control" id="envio" name="envio" >
                    <option>-seleccione-</option>
                    @foreach($envio as $envios)
                    <option value="{{$envios->id}}">{{$envios->name}}</option>


                    @endforeach
                  </select>
                   <span class="help-block">{{ $errors->first('envio') }}</span>
                </div>
              </div>
              <hr>
                  <div class="col-md-12 text-center">
                    <button class="btn btn-primary"><i class='fa fa-check'></i> Buscar Despachos</button>
                  </div>
                </form>
              <hr>
              <hr>
              <br>
              <br>


           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>               
                  <th>Enviado Por</th>
                  <th>Nota</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                  @foreach($ventas as $venta)
                   <tr>
                  <td>#{{$venta->id}}</td>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{ date("d/m/Y", strtotime($venta->fecha_despacho))}}</td>
                  <td>{{$venta->user->name}}</td>
                  <td>{{$venta->agency->name}}</td>               
                  <td class="text-uparcase">{{$venta->nota_despacho}}</td>
                  <td><a href="#" class="btn btn-warning" data-toggle="modal" data-target="#data-despacho{{$venta->id}}" title="detalle"><i class="fa fa-eye"></i></a></td>
                </tr>
                  @endforeach
                 </tbody>
                <tfoot>
                 <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>  
                  <th>Enviado Por</th>             
                  <th>Nota</th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
      <form action="historicodespacho" method="GET">
        <input type="hidden" name="desde1" value="{{$desde}}">
        <input type="hidden" name="hasta1" value="{{$hasta}}">
        <input type="hidden" name="envio1" value="{{$enselect}}">
              <!-- /.box -->
      <div class="col-md-12 text-center">
      <button class="btn btn-primary"><i class="fa fa-print"></i> Imprimir Historico</button>
        </div>
      </div>
    </form>
      
    </section>






@endsection


<!-- Modal -->
  @foreach ($ventas as $venta)
<div class="modal fade" id="data-despacho{{$venta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Procesar Despacho</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 80%" >
        </div>
          </center>
                <div class="col-xs-8">
                 
                      <center>
                     <strong>"ER ACCESORIOS & BOUTIQUE C.A"</strong> <br>
                     <p style="font-size: 9px">CC. PARQUE ARAGUA PRIMER NIVEL MULTICENTRO  PISO 1 LOCAL 23-25 MARACAY,ESTADOR ARAGUA</p>
                      <p style="font-size: 12px"><strong>RIF:J-403591156</strong></p>
                     

                 </center>
              
                 
                                 
                </div>
          
 <hr>
     

        <table class="table table-bordered-hoverd">
          <thead>

            <tr>
             <th colspan="4"><center>DATOS DEL CLIENTE</center></th>
            </tr>
           
          </thead>
          <tbody>
            <tr>
             
              <td><b>Cédula | RIF </b>:</td>
              <td>{{$venta->cliente->nac_usr}}-{{$venta->cliente->ced_user}}</td>
              <td><b>Nombres:</b></td>
              <td>{{$venta->cliente->name}}</td>
            </tr>

             <tr>
              <td><b>Email:</b></td>
              <td>{{$venta->cliente->email}}</td>
              <td><b>Teléfono:</b></td>
              <td>{{$venta->cliente->telefono}}</td>
            </tr>
             <tr>
              <td><b>Domicilio Fiscal:</b></td>            
              <td colspan="3" >{{$venta->cliente->direccion}}</td>
            </tr>
          </tbody>
          
          
        </table>
        <table class=" table table-bordered">
          <thead>

            <tr>
              <th>Código</th>
              <td colspan="4">{{ str_pad($venta->nro_orden, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
           
          
          
        </table>

             <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$iva->porc}}%</th>
          <th>TOTAL</th>
         

        </tr>
        <tbody>
          @foreach($venta->detalle as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$item->product->name }}</td>
          <td>
           {{ $item->qty }}
          </td>
         
          <td>{{ number_format(($item->price)-($item->price*$item->iva/100),2,',','') }}</td> 
          
          <td>{{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$item->iva/100),2,',','.') }}</td>
          <td>{{ number_format(($item->price * $item->qty)*($item->iva/100),2,',','.') }}</td>
          <td> {{ number_format(($item->price * $item->qty),2,',','.') }}</td>
          
          </th>
        </tr>

        
            
          @endforeach
          <!--envio-->
          <tr class="table-secondary">
            <th colspan="7">
          <center>  COSTO DE ENVIO</center>
          </th>
          </tr>

          <tr>
            <th colspan="4">         
           <center>
             ENVIO POR({{ $venta->agency->name}})
           </center>
            </th>
            <td id="tdsub">{{number_format(($venta->monto_envio)-($venta->monto_envio*$venta->iva/100),2,',','.')}}</td>
            <td id="tiva">{{number_format($venta->monto_envio*$venta->iva/100,2,',','.')}}</td>
            <td id="tot">{{number_format($venta->monto_envio,2,',','.')}}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th id="SUB">{{number_format(($venta->monto_compra + $venta->monto_envio)-(($venta->monto_compra + $venta->monto_envio)*$venta->iva/100),2,',','.')}}  </th>
          <th id="IV">{{number_format(($venta->monto_compra + $venta->monto_envio)*$venta->iva/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success' id="TT">
            BsS:  {{number_format(($venta->monto_compra + $venta->monto_envio),2,',','.')}}
          </span></h4>

         

          </th>
       
          <th></th>

        </tr>
        </tfoot>
        
      </table>
      
      <div class="col-md-6">
        <label>Fecha Despacho</label>
       {{ date("d/m/Y", strtotime($venta->fecha_despacho))}}      
      </div>
      <div class="col-md-6">
        <label>Responsable</label>
        {{$venta->user->name}}
      </div>

      <div class="col-md-12">
        <label>Nota De Despacho</label>
        {{$venta->nota_despacho}} 
        
      </div>

       
       
      </div>
    
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        
      </div>
    </div>
  </div>
</div>





@endforeach





