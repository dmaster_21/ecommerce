@extends('layouts.portal')

@section('content')

<div class="container">

	<div class="col text-center">
					<div class="section_title new_arrivals_title">
						<h4>Detalle del Producto</h4>
					</div>
				</div>

				<br>

  <div class="row">

    <div id="gallery" class="col-md-3 col-sm-3">
       
         <a href="{{asset('images/dest/'.$Pdto->image)}}" title="Fujifilm FinePix S2950 Digital Camera">   
        <img class="img-thumbnail" style="border: 2px solid #b91f5a" src="{{asset('images/dest/'.$Pdto->image)}}" style="width:29%" alt=""/>
      </a>
            

       
      <div id="differentview" class="moreOptopm carousel slide">
        <br>
                <div class="carousel-inner">

                
                  <div class="item active">
                   @foreach($img as $images)

                      <center>
                   <img style="width:29%" src="{{asset('images/productos/'.$images->image)}}" alt="" class="img-thumbnail"/>
                  
                 </center>
                 
                    @endforeach
                  </div>
                 
                </div>
            <!--
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
              <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a> 
      -->
              </div>
               <div class="btn-toolbar">
                <center>
                  <!--
        <div class="btn-group">
        <span class="btn btn-default"><i class="icon-envelope"></i></span>
        <span class="btn btn-default" ><i class="icon-print"></i></span>
        <span class="btn btn-default" ><i class="icon-zoom-in"></i></span>
        <span class="btn btn-default" ><i class="icon-star"></i></span>
        <span class="btn btn-default" ><i class=" icon-thumbs-up"></i></span>
        <span class="btn btn-default" ><i class="icon-thumbs-down"></i></span>

        </div>
      -->
      </center>
      </div>
      </div>
   
      
  
      <div class="col-md-9 col-sm-9">
        <center>
        <h5>{{ $Pdto->name}}</h5>
      </center>
         <span class="title-divider"></span>
        <p align="justify"></p>
        <hr class="soft"/>
        <form class="form-horizontal qtyFrm">
          <div class="control-group">
          <label class="control-label"></label>
          <div class="controls">
          <b> Precio : {{ number_format($Pdto->price,2,',','.') }} </b>
          
            <br>
             <b> Stock: {{ $Pdto->qty }}</b>
             <br>
             <a href="{{ route('cart.add',$Pdto->id) }}">
               <span onclick="anadir()" class="btn btn-primary" >Añadir al Carrito<i class="fa fa-shopping-cart"></i></span>
             </a>
          </div>
          </div>
        </form>
        
        <hr class="soft"/>
        <h5>Características del Producto:</h5>
          <span class="title-divider"></span>
          <div class="col-md-12 col-sm-12" style="text-align: justify;">
           <p>
            {{ $Pdto->description }}
           </p>
            
          </div>
        
        <hr class="soft clr"/>
        <h5>Categoria:</h5> 
         <span class="title-divider"></span>
        <p><!--aqui va algo mas etenso --> 

        {{ $Pdto->category->name }}     
        </p>
    

        </div><!--cierro rows>-->
      </div><!--aqui cierra -->


    <hr>


    <!-- Best Sellers-->


  <div class="best_sellers">
    <div class="container">
      <div class="row">
        <div class="col text-center">
          <div class="section_title new_arrivals_title">
            <h2>Productos Relacionados</h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col">
          <div class="product_slider_container">
            <div class="owl-carousel owl-theme product_slider">

              @foreach($rel as $prod)
             
              <div class="owl-item product_slider_item">
                <div class="product-item">
                  <div class="product discount">
                    <div class="product_image">
                      <img src="{{ asset('images/productos/'.$prod->image) }}"" alt="">
                    </div> 
                    <div class="favorite favorite_left"></div>
                    
                    <div class="product_info">
                      <h6 class="product_name">{{ $prod->name}}</h6>
                      <div class="product_price">BsS:{{ number_format($prod->price,2,',','.') }}</div>



                       <a href="{{ route('productos.detalle',$prod->id) }}"><button class="btn btn-primary"><i class="fa fa-eye"></i></button></a>
                    <button class="btn btn-warning"><i class="fa fa-star"></i></button>
                    <a href="{{ route('cart.add',$prod->id) }}">
                    <button class="btn btn-success" title="Añadir a Carrito"><i class="fa fa-shopping-cart"></i></button>
                  </a>
                    


                
                    </div>
                  </div>
                </div>
              </div>
             
              @endforeach
            

              
              


            </div>

            <!-- Slider Navigation -->

            <div class="product_slider_nav_left product_slider_nav d-flex align-items-center justify-content-center flex-column">
              <i class="fa fa-chevron-left" aria-hidden="true"></i>
            </div>
            <div class="product_slider_nav_right product_slider_nav d-flex align-items-center justify-content-center flex-column">
              <i class="fa fa-chevron-right" aria-hidden="true"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


@endsection


