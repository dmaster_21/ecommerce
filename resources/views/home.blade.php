@extends('layouts.admin')

@section('content')

<section class="content-header">

      <!-- Small boxes (Stat box) -->
      <div class="row">

      @if(Auth::user()->admin==2)
         @if($top>0)
        
                   <div class="callout callout-danger">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>Estimado Usuario Verificar Su Stock de Productos Debido a Que estan llegando a su tope Minimo < 10 Items, Ingrese mediante sección Productos | Stock Minimo Para Mayor Información </p>
              </div>
                     
                @endif
                @endif

            <div class="box">
               <div class="box-header with-border">  
               <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 40%" >
      </center>
        </div>              
                <div class="col-xs-8">
                 
                      <center>
                     <p style="font-size: 20px"><strong>"ER ACCESORIOS & BOUTIQUE C.A"</strong></p> <br>                 
                      <p style="font-size: 15px"><strong>RIF:J-403591156</strong></p>
                     

                 </center>
               </div>

                    <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 40%" >
      </center>
        </div>  
             </div>
              
                 
                                 
                </div>
           


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{ $ventas_sn }}</h3>

              <p>Ventas Sin Verificar</p>
            </div>
            <div class="icon">
              <i class="fa fa-shopping-cart"></i>
            </div>
            <a href="#" class="small-box-footer">
              Mas Información <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ $ventas_vr }}<sup style="font-size: 20px"></sup></h3>

              <p>Ventas Concretadas</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="#" class="small-box-footer">
              Mas Información <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{ $user }}</h3>

              <p>Clientes Registrados</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="#" class="small-box-footer">
              Mas Información <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ $prod}}</h3>

              <p>Top De Productos</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">
              Mas Información <i class="fa fa-arrow-circle-right"></i>
            </a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </section>

    <!-- Main content -->
    <section class="content">
       @if (session('mensaje'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('mensaje') }}</p>
              </div>
                        
                @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Ventas Por Verificar</strong></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         <!--aqui va todo -->
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre Cliente</th>
                  <th>Telefóno</th>
                  <th>Email</th>
                  <th>Fecha de Venta</th>
                  <th>Fecha de Anulación</th>                  
                  <th>Monto</th>
                  <th>Estatus</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($ventas as $venta)
                <tr>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{$venta->cliente->telefono}}</td>
                  <td>{{$venta->cliente->email}}</td>
                  <td><span class='badge bg-green'>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</span></td>

                  <td>
                

                      @if($venta->status==1)
                    <span class='badge bg-red'>
                    {{ date("d/m/Y",strtotime ( '+'.$day->nroday.' day' , strtotime($venta->fecha_compra)))}}
                   @endif
                  </span></td>

                  <td><b>{{number_format($venta->monto_compra + $venta->monto_envio,2,',','.')}}</b></td>
                    <td>
                      @if($venta->status==1)
                      <span class='badge bg-info'>Pendiente</span>
                      @endif

                       @if($venta->status==2)
                      <span class='badge bg-warning'>Pago Procesado</span>
                      @endif

                       @if($venta->status==3)
                      <span class='badge bg-warning'>Pago Confirmado</span>
                      @endif

                      @if($venta->status==9)
                      <span class='badge bg-warning'>Compra Anulada</span>
                      @endif


                    </td>
                  <td>
                    <center>
                    
                      @if($venta->status==2)
                      <button class="btn btn-success " title="Autorizar Compra"  data-toggle="modal" data-target="#data-confirm{{$venta->id}}"><i class="fa fa-check-circle"></i></button>
                      @else
                      <button class="btn btn-success " title="Autorizar Compra" disabled=""><i class="fa fa-check-circle" ></i></button>
                      @endif
                     <button class="btn btn-peimary " title="ver detalle" data-toggle="modal" data-target="#data-vista{{$venta->id}}"><i class="fa fa-search"></i></button>

                    <a href="/voucher/{{$venta->id}}"><button class="btn btn-default " title="Imprimir Comprobante"><i class="fa fa-print"></i></button></a>
                     @if($venta->status==3 || $venta->status==9)
                   
                     
                     <button class="btn btn-danger " title="Anular Venta"  data-toggle="modal" data-target="#data-delete{{$venta->id}}" disabled=""><i class="fa fa-trash-o"></i></button> @else
                        @if($venta->status==1)
                        <button class="btn btn-danger " title="Anular Venta"  data-toggle="modal" data-target="#data-delete{{$venta->id}}"><i class="fa fa-trash-o"></i></button>
                        @endif
                     @endif
                  
                   </center>
                  </td>
                </tr>
                @endforeach
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
    
    <center><a href="/pdfVentaC"><button class="btn btn-primary"><i class="fa fa-print"></i> Imprimir Ventas Pendientes</button></a></center>

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->

@include('modals.verificarhome')
     
@endsection


