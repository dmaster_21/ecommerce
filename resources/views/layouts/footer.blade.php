  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="container">
      <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
      </div>
      <strong>Copyright &copy; 2018 Web Desarrollada Por Karla Castillo</strong> 
    </div>
    <!-- /.container -->
  </footer>