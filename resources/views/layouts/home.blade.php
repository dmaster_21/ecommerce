<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<title>{{ config('app.name', 'Laravel') }}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Colo Shop Template">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/styles/bootstrap4/bootstrap.min.css') }}">

<link href="{{ asset('portal/plugins/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/plugins/OwlCarousel2-2.2.1/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/plugins/OwlCarousel2-2.2.1/animate.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/styles/main_styles.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('portal/styles/responsive.css') }}">

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">




</head>

<body>

<div class="super_container">

	<!-- Header -->

	<header class="header trans_300">

	
		<!-- Main Navigation -->

		<div class="main_nav_container">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-right">
						<div class="logo_container">
							<img src="{{ asset('portal/images/logo.png') }}" style="width: 229%;
height: 47%;
margin-left: -617px;">
						</div>
						<nav class="navbar">
							<ul class="navbar_menu">
								<li><a href="/"><i class="fa fa-home " aria-hidden="true"></i> Inicio</a></li>
								
								
								<li><a href="/contacto"><i class="fa fa-phone" aria-hidden="true"></i> Contactanos</a></li>

			   @guest
			<li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          	<i class="fa fa-user" aria-hidden="true"></i>
          	Mi Perfil 

          	<span class="caret"></span></a>
                   <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('login') }}"><i class="fa fa-sign-in "></i> Login</a></li>
                 @if (Route::has('register'))
                   <li><a href="{{ route('register') }}"><i class="fa fa-users "></i> Registrarme</a></li>            
                  </ul>
             </li>
               @endif
                @else

                <li class="dropdown">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                 <i class="fa fa-user" aria-hidden="true"></i>
          	     {{ Auth::user()->name }}

          	<span class="caret"></span></a>
                   <ul class="dropdown-menu" role="menu">
                   	<li><a href="/compras/cuenta"><i class="fa fa-bookmark-o "></i> Mi Cuenta</a></li>
                   	<li><a href="/salir"><i class="fa fa-sign-out "></i> Cerrar Sesion</a></li>

                        
                  </ul>
             </li>



                     @endguest


							</ul>



										
									</ul>
							<ul class="navbar_user">
								<li><a href="#"><i class="fa fa-search" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-user" aria-hidden="true"></i></a></li>

								<li class="checkout">
									<a href="/cart/show" title="Ver Carro de Compras">
										<i class="fa fa-shopping-cart" aria-hidden="true"></i>
										
									</a>
								</li>
							</ul>
							<div class="hamburger_container">
								<i class="fa fa-bars" aria-hidden="true"></i>
							</div>
						</nav>
					</div>
				</div>
			</div>
		</div>

	</header>

	<div class="fs_menu_overlay"></div>
	<div class="hamburger_menu">
		<div class="hamburger_close"><i class="fa fa-times" aria-hidden="true"></i></div>
		<div class="hamburger_menu_content text-right">
			<ul class="menu_top_nav">
				<li class="menu_item has-children">
					<a href="#">
						usd
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#">cad</a></li>
						<li><a href="#">aud</a></li>
						<li><a href="#">eur</a></li>
						<li><a href="#">gbp</a></li>
					</ul>
				</li>
				<li class="menu_item has-children">
					<a href="#">
						English
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#">French</a></li>
						<li><a href="#">Italian</a></li>
						<li><a href="#">German</a></li>
						<li><a href="#">Spanish</a></li>
					</ul>
				</li>
				<li class="menu_item has-children">
					<a href="#">
						My Account
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="menu_selection">
						<li><a href="#"><i class="fa fa-sign-in" aria-hidden="true"></i>Sign In</a></li>
						<li><a href="#"><i class="fa fa-user-plus" aria-hidden="true"></i>Register</a></li>
					</ul>
				</li>
				<li class="menu_item"><a href="#">home</a></li>
				<li class="menu_item"><a href="#">shop</a></li>
				<li class="menu_item"><a href="#">promotion</a></li>
				<li class="menu_item"><a href="#">pages</a></li>
				<li class="menu_item"><a href="#">blog</a></li>
				<li class="menu_item"><a href="#">contact</a></li>
			</ul>
		</div>
	</div>

	<!-- Slider -->

	<div class="main_slider" style="background-image:url({{ asset('portal/images/slider_1.jpg')}})">
		<div class="container fill_height">
			<div class="row align-items-center fill_height">
				<div class="col">
					<div class="main_slider_content">
						<h4 class="text-center">"ER ACCESORIOS & BOUTIQUE C.A"</h4><br>
						<h4 class="text-center">RIF:J-403591156</h4>

						<h1>Traemos Para Ti La Comodidad de Comprar!!</h1>
						<div class="red_button shop_now_button"><a href="#">Hazlo Ya !</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
   @yield('content')
	

	<div class="newsletter">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="newsletter_text d-flex flex-column justify-content-center align-items-lg-start align-items-md-center text-center">
						<h4>Accesorios & boutique</h4>
						<p>Tu mejor Opción en Tiendas on Line</p>
					</div>
				</div>
				<div class="col-lg-6">
					
				</div>
			</div>
		</div>
	</div>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			<div class="row">
				<div class="col-lg-6">
					<div class="footer_nav_container d-flex flex-sm-row flex-column align-items-center justify-content-lg-start justify-content-center text-center">
						<ul class="footer_nav">
							
							<li><a href="#">Contactanos</a></li>
						</ul>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="footer_social d-flex flex-row align-items-center justify-content-lg-end justify-content-center">
						<ul>
							<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-skype" aria-hidden="true"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="footer_nav_container">
						<div class="cr">©2018 Derechos Reservados </div>
					</div>
				</div>
			</div>
		</div>
	</footer>

</div>

<script src="{{ asset('portal/js/jquery-3.2.1.min.js')}}"></script>
<script src="{{ asset('portal/styles/bootstrap4/popper.js')}}"></script>
<script src="{{ asset('portal/styles/bootstrap4/bootstrap.min.js')}}"></script>
<script src="{{ asset('portal/plugins/Isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{ asset('portal/plugins/OwlCarousel2-2.2.1/owl.carousel.js')}}"></script>
<script src="{{ asset('portal/plugins/easing/easing.js')}}"></script>
<script src="{{ asset('portal/js/custom.js')}}"></script>
<script src="{{ asset('portal/js/jquery.lightbox-0.5')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/js/bootstrap-notify.js"></script>



</body>

</html>
