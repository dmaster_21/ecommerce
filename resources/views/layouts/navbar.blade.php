
    <!-- Logo -->
    <a href="#" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">{{ config('app.name', 'Laravel') }}</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="{{ asset('images/avatar/user.png')}}" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('images/avatar/user.png')}}" class="img-circle" alt="User Image">

                <p>
                {{ Auth::user()->name }}
                  <small></small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="col-md-12 textt-center">
                  <a href="/perfil" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Perfil de Usuario</a>
                </div>
                <div class="col-md-12 textt-center">
                  <a  class="btn btn-default btn-flat" href="/salir"><i class="fa fa-sing-up"></i> Cerrar Sesion</a>
                </div>
                <hr>
                <div class="col-md-12 textt-center">
                  <a  class="btn btn-default btn-flat" href="{{ asset('manual/manual_uso.pdf')}}" target="_black"><i class="fa fa-book"></i> Manual de Usuario</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="{{ asset('images/avatar/user.png')}}" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> En Linea</a>
        </div>
      </div>
      <!-- search form
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"></li>
       
         
          <li> <a href="/home"> <i class="fa fa-home"></i> <span >Inicio</span></a></li>
     

        @if(Auth::user()->admin==2)
        <li class="treeview">
          <a href="#">
            <i class="fa fa-cogs" ></i> <span>Configuración</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/categorias"><i class="fa fa-circle-o"></i> Categorias</a></li>
            <li><a href="/envio"><i class="fa fa-circle-o"></i> Agencia De Envio</a></li>
            <li><a href="/banco"><i class="fa fa-circle-o"></i>Tus Receptor Bancario</a></li>
            <li><a href="/day"><i class="fa fa-circle-o"></i>Dia Anular</a></li>
            <!-- <li><a href="/valor"><i class="fa fa-circle-o"></i>Valor Inicial</a></li>-->
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/productos/create"><i class="fa fa-circle-o"></i>Ingreso</a></li>
            <li><a href="/productos"><i class="fa fa-circle-o"></i>Mis Productos</a></li>
            <li><a href="/stockmin"><i class="fa fa-circle-o"></i>Stock Minimo</a></li>
          
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa  fa-opencart ""></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/verificar"><i class="fa fa-circle-o"></i>Verificar </a></li>
            <li><a href="/factura"><i class="fa fa-circle-o"></i>Montar Factura Fiscal </a></li>
            
          </ul>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>Estádisticas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/Pventas"><i class="fa fa-circle-o"></i>Ventas</a></li>
            <li><a href="/Topventas"><i class="fa fa-circle-o"></i>Productos Vendidos</a></li>
             <li><a href="/graficos"><i class="fa fa-circle-o"></i>Información Gráfica</a></li>
          
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa fa-dropbox"></i> <span>Despacho Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/despacho"><i class="fa fa-circle-o"></i>Compras Despacho</a></li>
            <li><a href="/buscadespacho"><i class="fa fa-circle-o"></i>Historico Despacho</a></li>
            
          </ul>
        </li>
    
         <li class="treeview">
          <a href="#">
            <i class="fa fa-key"></i> <span>Auditoria</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/log"><i class="fa fa-circle-o"></i>Busqueda Avanzada</a></li>
            
          </ul>
        </li>
     

        <li class="treeview">
          <a href="#">
            <i class="fa fa-users"></i> <span>Gestón de Usuarios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/usuario"><i class="fa fa-circle-o"></i>Busqueda Avanzada</a></li>
            
          </ul>
        </li>

        <li> <a href="/inbox"> <i class="fa fa-inbox"></i> <span >Sugerencias Y Reclamos</span></a></li>
        
     <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Respaldos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{ asset('/respaldo/')}}"><i class="fa fa-database"></i>Busqueda Avanzada</a></li>
            
          </ul>
        </li>
        @endif


        @if(Auth::user()->admin==3)
       

        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
        
            <li><a href="/productos/create"><i class="fa fa-circle-o"></i>Ingreso</a></li>
            <li><a href="/productos"><i class="fa fa-circle-o"></i>Mis Productos</a></li>
            <li><a href="/stockmin"><i class="fa fa-circle-o"></i>Stock Minimo</a></li>       
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa  fa-opencart ""></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="/verificar"><i class="fa fa-circle-o"></i>Verificar </a></li>
           
            
          </ul>
        </li>

        
         

        @endif


        @if(Auth::user()->admin==4)
       
        <li class="treeview">
          <a href="#">
            <i class="fa fa-dashboard"></i> <span>Productos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
           
            <li><a href="/productos"><i class="fa fa-circle-o"></i>Inventario Existente</a></li>
        
          </ul>
        </li>

         <li class="treeview">
          <a href="#">
            <i class="fa  fa-opencart ""></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          
            <li><a href="/factura"><i class="fa fa-circle-o"></i>Montar Factura Fiscal </a></li>
            
          </ul>
        </li>

      
        @endif
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->
