
<div class="col-sm-4">
    <div class="card" style="width: 18rem;">
    	<center>
   @if(Auth::user()->photo=="")     
  <img src="{{ asset('images/avatar/userP.png')}}" style="width: 50%;" class="card-img-top" alt="...">
  @else
 <img src="{{ asset('images/avatar/'.Auth::user()->photo)}}" style="width: 50%;" class="card-img-top" alt="...">
  @endif
</center>
  <div class="card-body">
    <h5 class="card-title"><i class="fa fa-user"></i>{{ Auth::user()->name }}</h5>
    <p class="card-text"></p>
  </div>
  <ul class="list-group list-group-flush">
    <a href="/compras/cuenta">
    <li class="list-group-item"><i class="fa fa-cogs fa-2x"></i> Mi Cuenta</li>
  </a>
  <a href="/compras/cliente">
    <li class="list-group-item"><i class="fa fa-opencart fa-2x"></i>Compras</li>
  </a>
    <a href="/EnvioSug">
    <li class="list-group-item"><i class="fa fa-envelope-o fa-2x"></i> Sugerencias | Reclamos</li>
  </a>
  <a href="/cart/show">
    <li class="list-group-item"><i class="fa fa-shopping-cart fa-2x"></i>Carrito de Compras</li>
  </a>
  <a href="/cart/show">
    <li class="list-group-item"><i class="fa fa-home fa-2x"></i>Catalogo de Productos</li>
  </a>
  </ul>
  <div class="card-body">   
    <a href="/salir" class="card-link"><i class="fa fa-sign-out fa-2x"></i> Cerrar Sesion</a>
  </div>
</div>
  </div>