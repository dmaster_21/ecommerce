@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        Auditoria De Sucesos
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Auditoria</a></li>
        <li class="active">Bitacora de Eventos</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif


                <hr>
                <form action="logs/" method="GET">
      
        <input type="hidden" name="_token"value="{{ csrf_token()}}">
                <div class="col-md-3">
                <div class="form-group {{ $errors->has('user') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Usuario a Auditar</label>
                  <select class="form-control" id="user" name="user">
                    <option value="">Seleccione Usuario</option>
                    <option value="T">Todos Los Usuarios</option>
                    @foreach($user as $users)
                    <option value="{{$users->id}}">{{$users->name}}</option>


                    @endforeach
                    
                  </select>
                   <span class="help-block">{{ $errors->first('user') }}</span>
                </div>
              </div>

               <div class="col-md-3">
                <div class="form-group {{ $errors->has('desde') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Desde:</label>
                  <input type="text" class="form-control datepicker" name="desde" value="{{ old('desde') }}" readonly="">
                   <span class="help-block">{{ $errors->first('desde') }}</span>
                </div>
              </div>
               <div class="col-md-3">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Hasta:</label>
                  <input type="text" class="form-control datepicker" name="hasta" value="{{ old('hasta') }}" readonly="">
                   <span class="help-block">{{ $errors->first('hasta') }}</span>
                </div>
              </div>
              <div class="col-md-3">
                <div class="form-group {{ $errors->has('accion') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Acción:</label>
                  <select class="form-control"name="accion" required>
                    <option>-Seleccione-</option>
                    <option value='Crear'>Crear</option>
                    <option value='Editar'>Editar</option>
                    <option value="Eliminar">Eliminar</option>
                    
                  </select>
                </div>
              </div>
  <center> 
    <button class="btn btn-primary"><i class="fa fa-eye"></i> Solicitar Auditoria</button>
    <button class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button></center>
    </form>
              <hr>
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Usuario Relacionado</th>
                  <th>Rol del Usuario</th>
                  <th>Fecha</th>
                  <th>Detalle</th>
                  <th>Acción</th>
                  
               
                </tr>
                </thead>
                <tbody>  
                  @if($log)
                 @foreach($log as $logs) 
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   <td>{{$logs->user->name}}</td>
                    <td>
                      @if($logs->user->admin==1)
                      <b>CLIENTE</b>
                      @elseif($logs->user->admin==2)
                      <b>GERENTE DE GENERAL</b>
                      @elseif($logs->user->admin==3)
                      <b>GERENTE DE VENTAS</b>
                      @elseif($logs->user->admin==4)
                      <b>CAJERO</b>
                      @endif
                    </td>
                    <td>{{date('d/m/Y h:m:s', strtotime($logs->created_at))}}</td>                   
                    <td>{{$logs->accion}}</td>
                    <td>{{$logs->accion1}}</td>
                 </tr>  
               
                     
                
                @endforeach
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>Usuario Relacionado</th>
                  <th>Rol del Usuario</th>
                  <th>Fecha</th>
                  <th>Detalle</th>
                  <th>Acción</th>
                  
                 
                </tr>
                </tfoot>
              </table>
      </div>
      <!-- /.box -->
        
    </section>


 
@endsection







