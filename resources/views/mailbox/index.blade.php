@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        BUZÓN DE SUGERENCIAS Y RECLAMOS
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> sugerencias</a></li>
        <li class="active">Sugerencias y Reclamos</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
       
              <hr>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Usuario Emisor</th>
                  <th>Telefóno</th>
                  <th>fecha</th>
                  <th>Detalle</th>
                                
                </tr>
                </thead>
                <tbody>
                  @foreach($Sug as $sugs)
                  <tr>
                  <td>{{$sugs->user->name}}</td>
                  <td>{{$sugs->user->telefono}}</td>
                  <td>{{date_format($sugs->created_at,'d-m-Y')}}</td>
                  <td>{{$sugs->description}}</td>
                                
                </tr>



                  @endforeach
                
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  
                </tr>
                </tfoot>
              </table>


      </div>
      <!-- /.box -->
        
    </section>


 
@endsection







