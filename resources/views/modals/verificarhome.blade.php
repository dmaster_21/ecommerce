 @foreach($ventas as $venta)

      <div class="modal fade" id="data-vista{{$venta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detalle de tu Compra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >

        <div class="row">
        <div class="col-xs-2">
        <center>
        <img src="{{ asset('portal/images/logo.png') }}" style="width: 80%" >
        </div>
          </center>
                <div class="col-xs-8">
                 
                      <center>
                     <strong>"ER ACCESORIOS & BOUTIQUE C.A"</strong> <br>
                     <p style="font-size: 9px">CC. PARQUE ARAGUA PRIMER NIVEL MULTICENTRO  PISO 1 LOCAL 23-25 MARACAY,ESTADOR ARAGUA</p>
                      <p style="font-size: 12px"><strong>RIF:J-403591156</strong></p>
                     

                 </center>
              
                 
                                 
                </div>
          
 <hr>
     

        <table class="table table-bordered-hoverd">
          <thead>

            <tr>
             <th colspan="4"><center>DATOS DEL CLIENTE</center></th>
            </tr>
           
          </thead>
          <tbody>
            <tr>
             
              <td><b>Cédula | RIF </b>:</td>
              <td>{{$venta->cliente->nac_usr}}-{{$venta->cliente->ced_user}}</td>
              <td><b>Nombres:</b></td>
              <td>{{$venta->cliente->name}}</td>
            </tr>

             <tr>
              <td><b>Email:</b></td>
              <td>{{$venta->cliente->email}}</td>
              <td><b>Teléfono:</b></td>
              <td>{{$venta->cliente->telefono}}</td>
            </tr>
             <tr>
              <td><b>Domicilio Fiscal:</b></td>            
              <td colspan="3" >{{$venta->cliente->direccion}}</td>
            </tr>
          </tbody>
          
          
        </table>
        <table class=" table table-bordered">
          <thead>

            <tr>
              <th>Código</th>
              <td colspan="4">{{ str_pad($venta->nro_orden, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Nº Factura</th>
              <td colspan="4">{{ str_pad($venta->nro_factura, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Nº Control</th>
              <td colspan="4">{{ str_pad($venta->nro_control, 4, "0", STR_PAD_LEFT)}}</td>
            </tr>
            <tr>
              <th>Fecha</th>
              <td colspan="4">{{ date("d/m/Y h:m:s", strtotime($venta->created_at))}}</td>
            </tr>
           
          </thead>
          
          
        </table>

             <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$iva->porc}}%</th>
          <th>TOTAL</th>
         

        </tr>
        <tbody>
          @foreach($venta->detalle as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td>{{$item->product->name }}</td>
          <td>
           {{ $item->qty }}
          </td>
         
          <td>{{ number_format(($item->price)-($item->price*$item->iva/100),2,',','') }}</td> 
          
          <td>{{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$item->iva/100),2,',','.') }}</td>
          <td>{{ number_format(($item->price * $item->qty)*($item->iva/100),2,',','.') }}</td>
          <td> {{ number_format(($item->price * $item->qty),2,',','.') }}</td>
          
          </th>
        </tr>

        
            
          @endforeach
          <!--envio-->
          <tr class="table-secondary">
            <th colspan="7">
          <center>  COSTO DE ENVIO</center>
          </th>
          </tr>

          <tr>
            <th colspan="4">         
           <center>
             ENVIO POR({{ $venta->agency->name}})
           </center>
            </th>
            <td id="tdsub">{{number_format(($venta->monto_envio)-($venta->monto_envio*$venta->iva/100),2,',','.')}}</td>
            <td id="tiva">{{number_format($venta->monto_envio*$venta->iva/100,2,',','.')}}</td>
            <td id="tot">{{number_format($venta->monto_envio,2,',','.')}}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th id="SUB">{{number_format(($venta->monto_compra + $venta->monto_envio)-(($venta->monto_compra + $venta->monto_envio)*$venta->iva/100),2,',','.')}}  </th>
          <th id="IV">{{number_format(($venta->monto_compra + $venta->monto_envio)*$venta->iva/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success' id="TT">
            BsS:  {{number_format(($venta->monto_compra + $venta->monto_envio),2,',','.')}}
          </span></h4>

         

          </th>
       
          <th></th>

        </tr>
        </tfoot>
        
      </table>




       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>


      <div class="modal fade" id="data-delete{{$venta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Anular Orden de Compra Nro ({{$venta->nro_orden}}) del Cliente {{$venta->cliente->name}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar La Compra Del Cliente?
      </center>
        <form action="{{action('ComprascartController@destroy',$venta->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>               
</div>


 @if(!empty($venta->banco_id) && !empty($venta->tipo_id) )
 <div class="modal fade" id="data-confirm{{$venta->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Confirmar Pago de ({{$venta->cliente->name}}) NRO {{str_pad($venta->nro_orden, 4, "0", STR_PAD_LEFT)}}</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        <strong>El Cliente {{$venta->cliente->name}} Ha Reportado Un PAGO con las Siguientes Especificaciones:</strong>
        <hr>
        
        <table class="table table-bordered">
      
          <thead>
            <tr>
              <td>BANCO RECEPTOR</td><th>{{$venta->banco->name}}</th>
            </tr>
            <tr>
              <td>TIPO DE PAGO</td><th>{{$venta->pago->name}}</th>
            </tr>
            <tr>
               <td>MONTO CANCELADO</td><th>{{number_format($venta->monto_compra,2,',','.')}}</th>
             </tr>
             <tr>
                <td>NÚMERO DE OPERACIÓN</td><th>#{{$venta->ref_pago}}</th>
            </tr>
          </thead>
          
        </table>
        <p>Si los datos Suministrados Son Correctos y Han Sido Validados, Seleccione <b>Confirmar Pago</b></p>
 
    


      </center>
        <form action="{{action('ComprascartController@confirmar',$venta->id)}}" method="GET">

        <input type="hidden" name="_method" value="GET">
        <input type="hidden" name="_token" value="{{ csrf_token()}}">
        
        <label>Confirmar Nro Operacion</label>
        <input type="text" name="nro" class="form-control">

          <hr>

          <center>
       

            <input type="submit" name="" value="Confirmar Pago" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>    
   @endif           

      @endforeach
