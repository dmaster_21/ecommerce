@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">

       @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>RECEPTOR BANCARIO</strong></h3>
          <center><button class="btn btn-primary" data-toggle="modal" data-target="#data-register"><i class="fa fa-plus"></i>Nuevo Registro</button></center>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         <!--aqui va todo -->
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre de Banco</th>                  
                  <th>Estatus</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($bco as $banco)
                  
                   <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$banco->name}}</td>
                  <td>
                   @if($banco->status==1)
                    <span class="badge bg-green">Activo</span>
                   @endif

                   @if($banco->status==9)
                    <span class="badge bg-red">InActivo</span>
                   @endif
                  </td>
                  <td>
                  @if($banco->status==1)
                    <a href="/banco/status/{{$banco->id}}/9" class="btn btn-default" title="Inactivar">Inactivar</a>
                   @endif

                    @if($banco->status==9)
                    <a href="/banco/status/{{$banco->id}}/1" class="btn btn-default" title="Activar">Activar</a>
                   @endif

                   
                   <button class="btn btn-info" data-toggle="modal" data-target="#data-edit{{$banco->id}}"><i class="fa fa-edit"></i></button>

                   <button class="btn btn-danger" data-toggle="modal" data-target="#data-delete{{$banco->id}}"><i class="fa fa-trash"></i></button>
                   
                  </td>                 
                </tr>


                  @endforeach
                
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                 
                </tr>
                </tfoot>
              </table>
    

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->


     <div class="modal fade" id="data-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">NUEVO RECEPTOR BANCARIO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" action="{{action('BancosController@store')}}">
         <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Nombre del Banco</label>
                  <input type="text" class="form-control" name="nombre" id="name" value="{{ old('nombre') }}" placeholder="">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>
                   
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 <center>
                  <button class="btn btn-primary"> Enviar </button>

                 </center>
              </form>    
                </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>

 @foreach($bco as $banco)
<div class="modal fade" id="data-edit{{$banco->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">EDITAR RECEPTOR BANCARIO</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <form method="POST" action="{{action('BancosController@update',$banco->id)}}">
         <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label class="control-label" for="inputError"> Nombre del Banco</label>
                  <input type="text" class="form-control" name="nombre" id="name" value="{{ $banco->name }}" placeholder="">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>
                  <input type="hidden" name="_method" value="PUT">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 <center>
                  <button class="btn btn-primary"> Guardar Cambios </button>

                 </center>
              </form>    
                </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="data-delete{{$banco->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar Banco ({{$banco->name}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar el Banco?
      </center>
        <form action="{{action('BancosController@destroy',$banco->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>


 @endforeach

@endsection


