@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Productos
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>

     <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif

           <form  action="{{ url('productos') }}" method="POST" novalidate="" enctype="multipart/form-data">
          <!--info formulario-->
           @csrf
          <div class="col-md-5">
           <div class="col-md-12">
             <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
             
                  <label for="exampleInputPassword1">Categoria</label>
                  <select class="form-control" name="categoria">
                    <option value=""> Seleccione </option>
                    @foreach ($Category as $cat)
                  
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                   @endforeach
                  </select>
                   <span class="help-block">{{ $errors->first('categoria') }}</span>
                </div>
              </div>
              
                <div class="col-md-12">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nombre del Producto</label>
                  <input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>
                </div>
              </div>
                <div class="col-md-12">
                  <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Características</label>
                 <textarea class="form-control" name="descripcion" value="{{ old('descripcion') }}" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                           <span class="help-block">{{ $errors->first('descripcion') }}</span>
                  </div>
              </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('cantidad') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Cantidad</label>
                  <input type="text" class="form-control" onkeypress="return soloNumeros(event);" name="cantidad" value="{{ old('cantidad') }}">
                   <span class="help-block">{{ $errors->first('cantidad') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('precio') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Precio</label>
                  <input type="text" class="form-control" name="precio" onkeypress="return soloNumeros(event);"  placeholder="0.00" value="{{ old('precio') }}">
                   <span class="help-block">{{ $errors->first('precio') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('stock_min') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Stock Min</label>
                  <input type="text" name="stock_min"  onkeypress="return soloNumeros(event);" value="{{ old('stock_min') }}" class="form-control"  placeholder="0">
                   <span class="help-block">{{ $errors->first('stock_min') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('stock_max') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Stock Máx</label>
                  <input type="text" name="stock_max" value="{{ old('stock_max') }}" onkeypress="return soloNumeros(event);" class="form-control" id="exampleInputPassword1" placeholder="0">
                   <span class="help-block">{{ $errors->first('stock_max') }}</span>
                </div>
                </div>
               <input type="hidden" name="_token" value="{{ csrf_token() }}">

          </div>

          <div class="col-md-7">
            <label for="exampleInputPassword1">Imágen Destacada</label>
            <input type="file"  name="imagen">
          </div>
          <br>


          <hr>

          <!--Aqui va dropzone-->
          <div class="col-md-7">
            <label for="exampleInputPassword1">Galeria de Imágenes</label>
             <div class="file-loading">
              
                <input id="file-es" name="file[]" type="file" multiple>

                 {{ csrf_field() }}
            </div>
          </div>

         <input type="hidden" name="aci" value="1">
   
         
        </div>
        <hr>
          <center>
            <button type="submit" class="btn btn-default"><i class="fa fa-check-circle"></i> Crear Producto</button>
            <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
          </center>
          <hr>
        </form>
      </div>
      <!-- /.box -->

    </section>


<!--cierro div-->   



@endsection


