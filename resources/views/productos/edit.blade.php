@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Productos
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>

     <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif

           <form  action="{{action('ProductsController@update',$prod->id)}}" method="POST" novalidate="" enctype="multipart/form-data">
          <!--info formulario-->

          <input type="hidden" name="_method" value="PUT">
          <input type="hidden" name="_token" value="{{ csrf_token()}}">
           @csrf
          <div class="col-md-5">
           <div class="col-md-12">
             <div class="form-group {{ $errors->has('categoria') ? 'has-error' : '' }}">
             
                  <label for="exampleInputPassword1">Categoria</label>
                  <select class="form-control" id="categoria" name="categoria">
                    <option value=""> Seleccione </option>
                    @foreach ($Cat as $cat)
                  
                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                   @endforeach
                  </select>
                   <span class="help-block">{{ $errors->first('categoria') }}</span>
                </div>
              </div>
              
                <div class="col-md-12">
                <div class="form-group {{ $errors->has('nombre') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nombre del Producto</label>
                  <input type="text" class="form-control" id="nombre" name="nombre" value="{{$prod->nombre}}">
                   <span class="help-block">{{ $errors->first('nombre') }}</span>
                   <script>
                   
                   </script>
                </div>
              </div>
                <div class="col-md-12">
                  <div class="form-group {{ $errors->has('descripcion') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Características</label>
                 <textarea class="form-control" id="descripcion" name="descripcion" value="{{ old('descripcion') }}" 
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                           <span class="help-block">{{ $errors->first('descripcion') }}</span>
                  </div>
              </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('cantidad') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Cantidad</label>
                  <input type="text" class="form-control" id="cantidad" onkeypress="return soloNumeros(event);"  name="cantidad" value="{{ old('cantidad') }}">
                   <span class="help-block">{{ $errors->first('cantidad') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('precio') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Precio</label>
                  <input type="text" class="form-control" id="precio" name="precio" onkeypress="return soloNumeros(event);"   placeholder="0.00" value="{{ old('precio') }}">
                   <span class="help-block">{{ $errors->first('precio') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('stock_min') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Stock Min</label>
                  <input type="text" id="stock_min" name="stock_min" onkeypress="return soloNumeros(event);"  value="{{ old('stock_min') }}" class="form-control"  placeholder="0">
                   <span class="help-block">{{ $errors->first('stock_min') }}</span>
                </div>
                </div>

                <div class="col-md-3">
                  <div class="form-group {{ $errors->has('stock_max') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Stock Máx</label>
                  <input type="text" id="stock_max" name="stock_max" onkeypress="return soloNumeros(event);" value="{{ old('stock_max') }}" class="form-control" id="exampleInputPassword1" placeholder="0">
                   <span class="help-block">{{ $errors->first('stock_max') }}</span>
                </div>
                </div>
               <input type="hidden" name="_token" value="{{ csrf_token() }}">

          </div>
          <div class="col-md-7">
            <div class="alert alert-info" role="alert">
              <i class="fa fa-info-circle"></i>
              <b>Estimado Usuario:</b> Los Archivos deben Ser en Formato <b>JPG</b> y <b>PNG</b>
              Si en la actualización no se detentan cambios no se cambiaran las imagenes actuales.
            </div>
          </div>
    
          <div class="col-md-7">
            <label for="exampleInputPassword1">Imágen Destacada</label>
            <input type="file"  name="imagen">
          </div>
          <br>


          <hr>

          <!--Aqui va dropzone-->
          <div class="col-md-7">
            <label for="exampleInputPassword1">Galeria de Imágenes</label>
             <div class="file-loading">
              
                <input id="file-es" name="file[]" type="file" multiple>

                 {{ csrf_field() }}
            </div>
          </div>

        
         <input type="hidden" name="aci" value="1">
         <hr>
           <div class="col-md-7">
            <br>
            <br>
              <center>
                <a href="#" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
               <i class="fa fa-eye"></i> Ver Galeria
              </a>
              </center>
           </div>
         
        </div>


        <hr>

          <center>
            <button type="submit" class="btn btn-default"><i class="fa fa-check-circle"></i> Guardar Cambios</button>
            <button type="reset" class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button>
          </center>
          <hr>
        </form>
      </div>
      <!-- /.box -->

    </section>


<!--cierro div-->   

<!--modals estructure-->
<!-- Button trigger modal -->

<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Galeria de iamgen</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        @if(count($gal)<2)
             <div class="alert alert-warning" role="alert">
              <i class="fa fa-info-circle"></i>
              <b>Estimado Usuario:</b> La Opción Eliminar Imagen ha sido Inhabilitada, Debido a que la galeria debe contener al menos Una Imágen. Añada Una imagen nueva Y proceda a eliminar la que desea.
            </div>
           
            @endif
        <table class="table table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Imágen</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
        @foreach ($gal as $galery)
        <tr>
          <td>{{$loop->iteration}}</td>
          <td>
            <center>
            <img src="{{asset('images/productos/'.$galery->image)}}" width="30%" class="img-fluid img-thumbnail" >
          </center>
          </td>
          <td>
            @if(count($gal)>=2)
            <a href="{{ route('deleteIm', $galery->id) }}" class="btn btn-danger" title="Borrar Image">
              <i class="fa fa-trash"></i> 

            </a>
            @endif


          </td>
        </tr>

          
        @endforeach
      </tbody>
      </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
        
      </div>
    </div>
  </div>
</div>

<script>
   document.getElementById("categoria").value='{{$prod->category_id}}';
   document.getElementById("nombre").value='{{$prod->name}}';
   document.getElementById("descripcion").value='{{$prod->description}}';
   document.getElementById("cantidad").value='{{$prod->qty}}';
   document.getElementById("descripcion").value='{{$prod->description}}';
   document.getElementById("precio").value="{{number_format($prod->price,2,'.','')}}";
   document.getElementById("stock_min").value='{{$prod->stock_min}}';
   document.getElementById("stock_max").value='{{$prod->stock_max}}';



</script>
@endsection

@section('scripts')


@endsection




