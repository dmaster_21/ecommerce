@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        Productos
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif

                <center><a href="/pdf" class="btn btn-primary"><i class="fa fa-print"></i> Imprimir Inventario General</a></center>
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre Comercial</th>
                  <th>Categoria</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  
                  <th></th>
                </tr>
                </thead>
                <tbody>
            
                @foreach ($Productos as $prod)
                
                
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{ $prod->name }}</td>
                  <td>{{ $prod->category->name }}</td>
                  <td> {{ number_format($prod->price,2,'.',',' )}}</td>
                  <td>{{ $prod->qty }}</td>

                  <td>

                    @if(Auth::user()->admin==2 || Auth::user()->admin==3)
                    <center>

                      @if($prod->destacado==1)
                     <a href="{{ route('productos.destacar',[$prod->id, 0]) }}">
                      <button class="btn btn-default" title="Quitar Destacado" onclick="destacar('{{ $prod->id }}','0')"><i class="fa fa-star"></i></button>
                    </a>
                  
                      

                      @else
                      <a href="{{ route('productos.destacar',[$prod->id,1]) }}">                   
                      <button class="btn btn-warning" title="Destacar" onclick="destacar('{{ $prod->id}}','1')"><i class="fa fa-star-half-empty"></i></button>
                    </a>
                   
                    
                      @endif
                    
                    <a href="{{ route('productos.show', $prod->id) }}">
                    <button class="btn btn-default" title="Ver Información"><i class="fa fa-eye"></i></button>
                  </a>
                    <a href="{{ route('productos.edit', $prod->id) }}">
                    <button class="btn btn-info" title="Actualizar Productos"><i class="fa fa-edit"></i></button></a>
                    <a href="#" data-toggle="modal" data-target="#exampleModalLong{{$prod->id}}">
                    <button class="btn btn-danger" title="Anular Producto"><i class="fa fa-trash"></i></button>
                  </a>
                  </center>
                  @endif
                  </td>

                </tr>
                @endforeach 
                
                
                </tbody>
                <tfoot>
                <tr>
                   <th>#</th>
                  <th>Nombre Comercial</th>
                  <th>Categoria</th>
                  <th>Precio</th>
                  <th>Cantidad</th>
                  <th>
                    
                  </th>
                </tr>
                </tfoot>
              </table>
      </div>
      <!-- /.box -->
        
    </section>


<!--cierro div-->   

 @foreach ($Productos as $prod)
<div class="modal fade" id="exampleModalLong{{$prod->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <h5> <b>Eliminar el Producto ({{$prod->name}})</b></h5>
       
      </div>
      <div class="modal-body">
      <p>¿Está Segur@ que desea Eliminar este Producto?</p>     
      <form action="{{action('ProductsController@destroy',$prod->id)}}" method="POST" >

        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">
        
        
                    
        
      </div>
      <div class="modal-footer">
         <input type="submit" class="btn btn-primary" value="Aceptar">
        <a href="#" class="btn btn-danger" data-dismiss="modal">Cancelar</a> 
      </form>  
      </div>
    </div>
  </div>
</div>
@endforeach



@endsection







