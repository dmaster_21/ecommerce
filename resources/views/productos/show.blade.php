@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        

        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>

     <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">



        

        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b>Información del Producto</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
  <thead>
    
      

    <tr>
      <th scope="col">Nombre Comercial</th>
      <td scope="col" colspan="3" >{{ $Prod->name }}</td>
      
    </tr>
    <tr>
      <th scope="col">Categoria</th>
      <td scope="col" colspan="3">{{ $Prod->category->name }}</td>
      
    </tr>

    <tr>
      <th scope="col" colspan="4">Descripción | Carácteristicas</th>
     
      
    </tr>
    <tr>
      <td colspan="4">
        <p align="justify">{{ $Prod->description }}</p>
      </td>
    </tr>
  </thead>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Cantidad:</th>
      <td>{{ $Prod->qty }}</td>
      <th>Precio:</th>
      <td>{{ number_format($Prod->price,'2',',','.') }}</td>
    </tr>
    <tr>
      <th scope="row">Stock Min</th>
      <td>{{ $Prod->stock_min }}</td>
      <th>Stock Max</th>
      <td>{{ $Prod->stock_max }}</td>
    </tr>

    <tr>
      <th scope="col">Registrado:</th>
      <td scope="col" colspan="3">{{ date("d/m/Y h:m:s", strtotime($Prod->created_at))}}</td>
      
    </tr>

    <tr>
      <th scope="col">Actualizado:</th>
      <td scope="col" colspan="3">{{ date("d/m/Y h:m:s", strtotime($Prod->updated_at))}}</td>
      
    </tr>

  </tbody>
</table>





            </div>
            <center><a href="/productos"><button class="btn btn-primary"><i class="fa fa-chevron-left"></i> Volver</button></a></center>
             
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->



        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><b> Galeria de Imágenes</b></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                  @foreach ($img as $value)
                  <li data-target="#carousel-example-generic" data-slide-to="{{$loop->iteration}}" class=""></li>
                    @endforeach
                </ol>
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="{{ asset('images/dest/'.$value->image) }}" alt="First slide">

                    <div class="carousel-caption">
                      First Slide
                    </div>
                  </div>
                  @foreach ($img as $value)

                  <div class="item">
                    <img src="{{ asset('images/productos/'.$value->image) }}" alt="Second slide">

                    <div class="carousel-caption">
                      Second Slide
                    </div>
                  </div>
                  @endforeach
                 
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
              
               
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->

          
      </div>
      <!-- /.box -->

    </section>


<!--cierro div-->   



@endsection





