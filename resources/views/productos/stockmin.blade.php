@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">

       @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">
            <center>
            <strong>
          Stock Minimo de Productos 
        </strong>
      </center>
      </h3>
      
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">


            <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
              <th>#</th>
              <th>Nombre Del Producto</th>
              <th>Categoria</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th></th>
            </tr>
              
            </thead>
            <tbody>
              @foreach($prod as $prods)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$prods->name}}</td>
                <td>{{$prods->category->name}}</td>
                <td>{{$prods->qty}}</td>
                <td>{{number_format($prods->price,2,',','.')}}</td>
                <td>
                  <a href="{{ route('productos.show', $prods->id) }}">
                    <button class="btn btn-default" title="Ver Información"><i class="fa fa-eye"></i></button>
                  </a>
                    <a href="{{ route('productos.edit', $prods->id) }}">
                    <button class="btn btn-info" title="Actualizar Productos"><i class="fa fa-edit"></i></button></a>
                </td>                
              </tr>
              @endforeach
              
            </tbody>
            
          </table>
        
           <center><a href="/minpdf"><button class="btn btn-primary"><i class="fa fa-print"></i>Imprimir Listado</button></a></center>

        </div>

        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->




@endsection


