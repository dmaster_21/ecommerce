@extends('layouts.pdf')

@section('content')
<center><strong>Listado de Compras Despachadas</strong></center>
<hr>

    <table class="table">         
                <thead>
                <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>               
                  <th>Enviado Por</th>
                  <th>Nota</th>
                </tr>
                </thead>
                <tbody>
                  @foreach($ventas as $venta)
                   <tr>
                  <td>#{{$venta->id}}</td>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{ date("d/m/Y", strtotime($venta->fecha_despacho))}}</td>
                  <td>{{$venta->user->name}}</td>
                  <td>{{$venta->agency->name}}</td>               
                  <td class="text-uparcase">{{$venta->nota_despacho}}</td>
                </tr>
                  @endforeach
                 </tbody>
                <tfoot>
                 <tr>
                  <th>#Compra</th>
                  <th>Cliente</th>
                  <th>Fecha Despacho</th>
                  <th>Usuario Responsable</th>  
                  <th>Enviado Por</th>             
                  <th>Nota</th>
                </tr>
                </tfoot>
@endsection