@extends('layouts.pdf')

@section('content')
<center><strong>Listado de Compras Por Despachar</strong></center>
<hr>

    <table class="table">         
                <thead>
                 <tr>    
                  <th>#Compra</th>             
                  <th>Cliente</th>
                  <th>Fecha de Compra</th>
                  <th>Costo Total</th>
                  <th>Plataforma</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($ventas as $venta)
                  <tr>                
                  <td>{{$venta->id}}</td>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</td>
                  <td>{{number_format($venta->monto_compra+$venta->monto_envio,2,',','.')}}</td>
                  <td>{{$venta->plataforma}}</td>               
                 
                </tr>
                    
                  @endforeach 
            
               
                
                </tbody>
                
             
              </table>
@endsection