@extends('layouts.pdf')

@section('content')

<center><strong>INVENTARIO GENERAL EN DEFICIT</strong></center>
<hr>

    <table class="table table-hover table-striped">
       <thead>
              <tr>
              <th>#</th>
              <th>Nombre Del Producto</th>
              <th>Categoria</th>
              <th>Cantidad</th>
              <th>Precio</th>
              <th></th>
            </tr>
              
            </thead>
            <tbody>
              @foreach($prod as $prods)
              <tr>
                <td>{{$loop->iteration}}</td>
                <td>{{$prods->name}}</td>
                <td>{{$prods->category->name}}</td>
                <td>{{$prods->qty}}</td>
                <td>{{number_format($prods->price,2,',','.')}}</td>
                <td>
                 
                </td>                
              </tr>
          </tbody>
              @endforeach
    </table>
@endsection