@extends('layouts.pdf')

@section('content')

<center><strong>INVENTARIO GENERAL DE PRODUCTOS</strong></center>
<hr>

    <table class="table table-hover table-striped">
        <thead>
            <tr>
                 <th><center>ID</center></th>
                <th><center>Producto</center></th>
               
                <th><center>Costo</center></th>
                <th><center>Cantidad</center></th>
            </tr>                            
        </thead>
        <tbody>
            @foreach($products as $product)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $product->name }}</td>
             
                <td class="text-right">{{ number_format($product->price,2,',','.') }}</td>
                <td class="text-right">{{ $product->qty }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
@endsection