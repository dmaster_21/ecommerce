@extends('layouts.pdf')

@section('content')

<center><strong>Ventas Según Rango de Fecha</strong></center>
<hr>

    <table class="table table-hover table-striped">         
                 <thead>
                <tr>
                  <th>Nombre Cliente</th>               
                  <th>Fecha de Venta</th>                

                  <th>Monto</th>
                
                </tr>
                </thead>
                <tbody>
                  @if($ventas)
                    @foreach($ventas as $venta)
                <tr>
                  <td>{{$venta->cliente->name}}</td>
                
                  <td><span class='badge bg-green'>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</span></td>
                  
                  
                  <td>
                   {{number_format($venta->monto_compra,2,',','.')}}
                  </td>
                </tr>
                @endforeach
                @endif
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
@endsection