@extends('layouts.pdf')

@section('content')

<center><strong>Top de Productos Más Vendidos</strong></center>
<hr>

    <table class="table table-hover table-striped">         
                <thead>
                <tr>
                  <th>#</th>
                  <th>Producto</th>
                  <th>Cantidad Disponible</th>
                  <th>Cantidad Vendida</th>
                
                
                </tr>
                </thead>
                <tbody>
                  @if($top)
                   @foreach($top as $tops)

                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$tops->name}}</td>
                    <td>{{$tops->qty}}</td>
                    <td>{{$tops->total_qty}}</td>
                  </tr>


                  @endforeach
                  @endif
              
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
@endsection