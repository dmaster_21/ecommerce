@extends('layouts.pdf')

@section('content')

<center><strong>VENTAS EN ESPERA POR VERIFICAR</strong></center>
<hr>

    <table class="table table-hover table-striped">
         <thead>
                <tr>
                  <th>Nombre Cliente</th>
                  <th>Telefóno</th>
                  <th>Email</th>
                  <th>Fecha de Compra</th>
                  
                  <th>Monto</th>
                 
                </tr>
                </thead>
                <tbody>
                    @foreach($ventas as $venta)
                <tr>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{$venta->cliente->telefono}}</td>
                  <td>{{$venta->cliente->email}}</td>
                  <td><span class='badge bg-green'>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</span></td>
                

                  <td><b>{{number_format($venta->monto_compra,2,',','.')}}</b></td>
                   
                  <td>
                 
                  </td>
                </tr>
                @endforeach
                
    </table>
@endsection