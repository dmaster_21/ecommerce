@extends('layouts.pdf')

@section('content')

<center><strong>COMPROBANTE DE COMPRA</strong></center>
<hr>

    <table class=" table table-striped">
          <thead>

            <tr>
              <td><strong>Código:</strong></td>
              <td>{{ str_pad($comp->nro_orden, 4, "0", STR_PAD_LEFT)}}</td>
            
              <td><strong>Nº Factura:</strong></td>
              <td >{{ str_pad($comp->nro_factura, 4, "0", STR_PAD_LEFT)}}</td>
           
              <td><strong>Nº Control:</strong></td>
              <td >{{ str_pad($comp->nro_control, 4, "0", STR_PAD_LEFT)}}</td>
            
              <td><strong>Fecha:</strong></td>
              <td >{{ date("d/m/Y", strtotime($comp->created_at))}}</td>
            </tr>
           
          </thead>
          
          
        </table>

         <table class="table table-bordered-hoverd">
          <thead>

            <tr>
             <th colspan="4"><center>DATOS DEL CLIENTE</center></th>
            </tr>
           
          </thead>
          <tbody>
            <tr>
              <td><b>Cédula | RIF </b></td>             
              <td>{{$comp->cliente->nac_usr}}-{{$comp->cliente->ced_user}}</td>
              <td><b>Nombres:</b></td>
              <td>{{$comp->cliente->name}}</td>
            </tr>

             <tr>
              <td><b>Email:</b></td>
              <td>{{$comp->cliente->email}}</td>
              <td><b>Teléfono:</b></td>
              <td>{{$comp->cliente->telefono}}</td>
            </tr>
             <tr>
              <td><b>Domicilio Fiscal:</b></td>            
              <td colspan="3" style="font-size: 10px">{{$comp->cliente->direccion}}</td>
            </tr>
          </tbody>
          
          
        </table>

             <table class="table table-bordered-hover">
        <tr class="table-secondary">
          <th>#</th>
          <th>Descripción</th>
          <th>Cantidad</th>
          <th>Precio</th>          
          <th>Sub-Total</th>
          <th>I.V.A {{$comp->iva}}%</th>
          <th>TOTAL</th>
         

        </tr>
        <tbody>
          @foreach($comp->detalle as $item)

          <tr>
          <td>{{$loop->iteration}}</td>
          <td style="font-size: 10px">{{$item->product->name }}</td>
          <td>
           {{ $item->qty }}
          </td>
         
          <td>{{ number_format(($item->price)-($item->price*$item->iva/100),2,',','') }}</td> 
          
          <td>{{ number_format(($item->price * $item->qty)-(($item->price * $item->qty)*$item->iva/100),2,',','.') }}</td>
          <td>{{ number_format(($item->price * $item->qty)*($item->iva/100),2,',','.') }}</td>
          <td> {{ number_format(($item->price * $item->qty),2,',','.') }}</td>
          
          </th>
        </tr>

        
            
          @endforeach
          <!--envio-->
          <tr class="table-secondary">
            <th colspan="7">
          <center>  COSTO DE ENVIO</center>
          </th>
          </tr>

          <tr>
            <th colspan="4" >         
           
             ENVIO POR ({{ $comp->agency->name}})
            </th>
            <td id="tdsub">{{number_format(($comp->monto_envio)-($comp->monto_envio*$comp->iva/100),2,',','.')}}</td>
            <td id="tiva">{{number_format($comp->monto_envio*$comp->iva/100,2,',','.')}}</td>
            <td id="tot">{{number_format($comp->monto_envio,2,',','.')}}</td>
          </tr>
        </tbody>
        <tfoot>
          <tr class="table-secondary">
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th id="SUB">{{number_format(($comp->monto_compra + $comp->monto_envio)-(($comp->monto_compra + $comp->monto_envio)*$comp->iva/100),2,',','.')}}  </th>
          <th id="IV">{{number_format(($comp->monto_compra + $comp->monto_envio)*$comp->iva/100,2,',','.')}} </th>
          <th><h4><span class='badge  badge-success' id="TT">
            BsS:  {{number_format(($comp->monto_compra + $comp->monto_envio),2,',','.')}}
          </span></h4>

         

          </th>

              
         
        </tr>


          @if(!empty($comp->banco_id) && !empty($comp->tipo_id) )  
    
           
            <tr>
              <td colspan="4">BANCO RECEPTOR</td><th colspan="3">{{$comp->banco->name}}</th>
            </tr>
            <tr>
              <td colspan="4">TIPO DE PAGO</td><th colspan="3">{{$comp->pago->name}}</th>
            </tr>
            <tr>
               <td colspan="4">MONTO CANCELADO</td><th colspan="3">{{number_format($comp->monto_compra + $comp->monto_pago,2,',','.')}}</th>
             </tr>
             <tr>
                <td colspan="4">NÚMERO DE OPERACIÓN</td><th colspan="3">#{{$comp->ref_pago}}</th>
            </tr>
          
      @endif
        </tfoot>
        
      </table>
     
@endsection