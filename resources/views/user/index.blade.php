@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">

       @if (session('mensaje'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('mensaje') }}</p>
              </div>
                        
                @endif
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Gestión de Usuarios Internos</strong></h3>
          <center><button class="btn btn-primary" data-toggle="modal" data-target="#data-register"><i class="fa fa-plus"></i>Nuevo Registro</button></center>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         <!--aqui va todo -->
         <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Nacionalidad</th>
                  <th>Cédula</th>
                  <th>Usuario</th>                  
                  <th>Email</th>
                  <th>Teléfono</th>
                  <th>Rol</th>
                  <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($User as $user)
                <tr>
                  <td>{{$loop->iteration}}</td>
                  <td>{{$user->nac_usr}}</td> 
                  <td>{{$user->ced_user}}</td> 
                  <td>{{$user->name}}</td>                  
                  <td>{{$user->email}}</td>
                  <td>{{$user->telefono}}</td>
                  <td>
                    @if($user->admin==2)
                    Gerente General
                    @elseif($user->admin==3)
                    Gerente de Ventas
                    @elseif($user->admin==4)
                    Cajero
                    @endif
                  </td>
                  <td>
                   
                  <button class="btn btn-warning" data-toggle="modal" data-target="#data-clave{{$user->id}}"><i class="fa fa-key"></i></button>   
                  
      <button class="btn btn-danger" data-toggle="modal" data-target="#data-delete{{$user->id}}"><i class="fa fa-trash"></i></button>            

                  </td>
                </tr>



                @endforeach                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                 
                </tr>
                </tfoot>
              </table>
    
    

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->



@foreach($User as $user)

<div class="modal fade" id="data-clave{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Resetear Clave de El Usuario ({{$user->name}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form method="POST" action="{{action('UsersController@reset',$user->id)}}">
        
         <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }} ">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
         <span class="glyphicon glyphicon-lock form-control-feedback"></span>
     
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

       

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>        
     



                  <input type="hidden" name="_method" value="GET">
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                 <center>
                  <button class="btn btn-danger"> Cambiar Contraseña</button>

                 </center>
              </form>    
                </div>
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

<!--eliminar-->
<div class="modal fade" id="data-delete{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Eliminar El Usuario ({{$user->name}})</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <center>
        ¿Realmente desea Eliminar el Usuario?
      </center>
        <form action="{{action('UsersController@destroy',$user->id)}}" method="POST">
        <input type="hidden" name="_method"value="DELETE">
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

          <hr>

          <center>
            <input type="submit" name="" value="Confirmar" class="btn btn-danger">
            <a href="#" data-dismiss="modal" class="btn btn-default">
              Cancelar

            </a>
          </center>
        </form>
       
      </div>
      <div class="modal-footer">
       
      </div>
    </div>
  </div>
</div>

@endforeach






     <div class="modal fade" id="data-register" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Crear Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" >
        <div class="form-group has-feedback">
          <form method="POST" action="{{action('UsersController@store')}}" novalidate="">
                        @csrf
        <div class="form-group {{ $errors->has('nac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nacionalidad</label>
                 <select class="form-control" name="nac" id="nac" >
                  
                  <option value="V">Venezolano (V)</option>
                  <option value="E">Extranjero (E)</option>
               
                   
                 </select>
                  <span class="help-block">{{ $errors->first('nac') }}</span>
               
                </div>
                <script>               
                document.getElementById("nac").value="{{old('nac')}}";                
              </script>  
              </div>
        <div class="form-group has-feedback {{ $errors->has('cedula') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="cedula" placeholder="Cédula de Identidad" onkeypress="return soloNumeros(event);">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('cedula') }}</span>
      </div>

        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}">        
        <input type="text" class="form-control " name="name" placeholder="Nombre del Usuario">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('name') }}</span>
      </div>


        <div class="form-group has-feedback {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <input type="text" class="form-control " name="telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask>
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('telefono') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <textarea class="form-control  " name="direccion" placeholder="Domicilio Fiscal"></textarea> 
          <span class="glyphicon glyphicon-home form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('direccion') }}</span>
      </div>
      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <input type="text" class="form-control " name="email" placeholder="Correo Electrónico">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('rol') ? 'has-error' : '' }}">
     
          <select class="form-control" name="rol">
            <option value="">-Seleccione Rol-</option>
            <option value="2">Gerente General</option>
            <option value="3">Gerente de Ventas</option>
            <option value="4">Cajero</option>
            
          </select>
      
        
         <span class="help-block">{{ $errors->first('rol') }}</span>
      </div>


      <div class="form-group has-feedback {{ $errors->has('password') ? 'has-error' : '' }} ">
        <input type="password" class="form-control" name="password" placeholder="Contraseña">
     
         <span class="help-block">{{ $errors->first('password') }}</span>
      </div>

       

      <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? 'has-error' : '' }}" >
        <input type="password" class="form-control" name="password_confirmation" placeholder="Confirmar Contraseña">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
      </div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Salir</button>
        <button type="submit" class="btn btn-primary">Guardar Registro</button>
      </div>
    </div>
  </div>
</div>


<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>


<script type="text/javascript">
  $('[data-mask]').inputmask()

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
@endsection


