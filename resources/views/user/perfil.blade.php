@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">

       @if (session('mensaje'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('mensaje') }}</p>
              </div>
                        
                @endif
      
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       Perfil de Usuario Interno
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuario</a></li>
        <li class="active">Perfil de Usuario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="{{ asset('images/avatar/user.png')}}" alt="User profile picture">

              <h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>
                    @if(Auth::user()->admin==2)                    
                    <p class="text-muted text-center">Gerente General</p>
                    @elseif(Auth::user()->admin==3)
                    
                    <p class="text-muted text-center">Gerente de Ventas</p>
                    @elseif(Auth::user()->admin==4)
                   <p class="text-muted text-center">Cajero</p>
                    @endif
              

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Registrado:</b> <a class="pull-right">{{date('d/m/Y h:m:s', strtotime(Auth::user()->created_at))}}</a>
                </li>
                <li class="list-group-item">
                  <b>Actualizado</b> <a class="pull-right">{{date('d/m/Y h:m:s', strtotime(Auth::user()->updated_at))}}</a>
                </li>
              
              </ul>

              <a href="/salir" class="btn btn-primary btn-block"><b>Cerrar Sesión</b></a>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>


      <!-- Default box -->
      <div class="col-md-9">
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Tu Perfil En Linea</strong></h3>
         
        <div class="box-body">
        <table class="table table-bordered">

                    <thead>
                      <tr>
                        <th colspan="4"><center><i class="fa fa-users"></i> Tus Datos Personales</center></th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <th> Nombres Y Apellido:</th>
                        <td colspan="3">{{Auth::user()->name }}</td>

                      
                      </tr>

                      <tr>
                        <th> Correo Electrónico:</th>
                        <td colspan="3">{{Auth::user()->email}}</td>

                      </tr>
                        <tr>
                        <th>Teléfono de Contacto:</th>
                        <td colspan="3">{{Auth::user()->telefono}}</td>
                      </tr>
                      <tr>
                        <th> Domicilio Fiscal :</th>
                        <td colspan="3">{{Auth::user()->direccion}}</td>

                        
                      </tr>
                    </tbody>
                     
                   </table>

                   <hr>

                   <center>

                    <button class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                    <i class="fa fa-edit"></i>  
                    Editar Información                    
                    </button>
                     <button class="btn btn-danger" data-toggle="modal" data-target="#modalpassword">
                    <i class="fa fa-key "></i>  
                    Cambiar Contraseña                   
                    </button>


                   </center>
           

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </div>
</div>

 <div class="col-md-12">
   <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Ultimo Records De Auditoria</strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
        
           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>                 
                  <th>Fecha</th>
                  <th>Acción Realizada</th>
               
                </tr>
                </thead>
                <tbody>  
                  @if($log)
                 @foreach($log as $logs) 
                 <tr>
                   <td>{{$loop->iteration}}</td>
                   
                    <td>{{date('d/m/Y h:m:s', strtotime($logs->created_at))}}</td>                   
                    
                    <td>{{$logs->accion}}</td>
                 </tr>  
               
                     
                
                @endforeach
              @endif
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>                
                  <th>Fecha</th>
                  <th>Acción Realizada</th>
                  
                 
                </tr>
                </tfoot>
              </table>
      </div>
    </div>
    </section>
    <!-- /.content -->


<!--modal update-->
<!-- Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Información De Perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" enctype="multipart/form-data">
        <form method="POST" action="{{action('UsersController@update',Auth::user()->id)}}" method="POST" novalidate="">
          <input type="hidden" name="_method" value="PUT">

                        @csrf

        <div class="form-group has-feedback {{ $errors->has('name') ? 'has-error' : '' }}"> 
        <label>Nombre</label>      
        <input type="text" class="form-control " name="name" value="{{Auth::user()->name }}" >
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('name') }}</span>
      </div>


        <div class="form-group has-feedback {{ $errors->has('telefono') ? 'has-error' : '' }}">
        <label>Teléfono</label>  
        <input type="text" class="form-control " name="telefono" data-inputmask='"mask": "(9999) 999-9999"' data-mask value="{{Auth::user()->telefono }}">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('telefono') }}</span>
      </div>

      <div class="form-group has-feedback {{ $errors->has('direccion') ? 'has-error' : '' }}">
        <label>Dirección</label>
        <textarea class="form-control  " name="direccion" >{{Auth::user()->direccion}}</textarea> 
          <span class="glyphicon glyphicon-home form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('direccion') }}</span>
      </div>
      <div class="form-group has-feedback {{ $errors->has('email') ? 'has-error' : '' }}">
        <label>Correo Electrónico</label>
        <input type="text" class="form-control " name="email"  value="{{Auth::user()->email }}" >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
         <span class="help-block">{{ $errors->first('email') }}</span>
      </div>
   

       

      <div class="form-group has-feedback">
        <label>Imagen de Perfil</label>
        <input type="file"  name="imagen"   >
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
  
      </div>
   
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit"  class="btn btn-primary" value="Guardar Cambios">

      </div>

    </form>
      </div>
    </div>
  </div>
</div>
<!--end modals-->



<div class="modal fade" id="modalpassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Cambiar Contraseña</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="POST" action="{{action('UsersController@reset',Auth::user()->id)}}" novalidate="">
                        @csrf
             <input type="hidden" name="_method"value="GET">
            <input type="hidden" name="_token"value="{{ csrf_token()}}">


         <div class="form-group has-feedback">
          <label>Contraseña</label>
        <input type="password" class="form-control"  name="password" >
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
         
      </div>

       

      <div class="form-group has-feedback " >
        <label>Confirmar Contraseña</label>
        <input type="password" class="form-control " name="password_confirmation">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
       

      </div>


       </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Guardar Cambios</button>
  </div>
</form>

      </div>
    </div>
  </div>
</div>
<!--end modals-->


<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.js')}}"></script>
<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
<script src="{{ asset('admin/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>


<script type="text/javascript">
  $('[data-mask]').inputmask()

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
@endsection


