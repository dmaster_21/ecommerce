@extends('layouts.admin')

@section('content')

<section class="content-header">
      <h1>
        Productos
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> Configuraciones</a></li>
        <li class="active">Productos</li>
      </ol>
    </section>

     <section class="content">
      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif

        
          <!--info formulario-->
           @csrf
           <!--datos del cliente-->
           <form action="{{action('ComprascartController@ingreso')}}" method="POST">
         
        <input type="hidden" name="_token" value="{{ csrf_token()}}">
          <div class="col-md-5">

             <div class="col-md-4">
                <div class="form-group">
                  <label for="exampleInputPassword1">Fecha De Pedido</label>
                  <input type="text" class="form-control"  value="{{date('d/m/Y')}}" readonly="">
               
                </div>
              </div>

              <div class="col-md-8">
                <div class="form-group">
                  <label for="exampleInputPassword1">Cajero</label>
                  <input type="text" name="cajero" class="form-control"  value="{{ Auth::user()->name }}" readonly="">
               
                </div>
              </div>
              <hr>
              <center>
              <strong>Datos del Cliente</strong>
            </center>

            <div class="col-md-4">
                <div class="form-group {{ $errors->has('nac') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nacionalidad</label>
                 <select class="form-control" name="nac" id="nac"  onchange="busca_cli()">
                  
                  <option value="V">Venezolano (V)</option>
                  <option value="E">Extranjero (E)</option>
                  <option value="J">Juridico (J)</option>
                   
                 </select>
                  <span class="help-block">{{ $errors->first('nac') }}</span>
               
                </div>
                <script>               
                document.getElementById("nac").value="{{old('nac')}}";                
              </script>  
              </div>

            <div class="col-md-8">
                <div class="form-group {{ $errors->has('cedula') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Cédula </label>
                  <input type="text" id="cedula" name="cedula" class="form-control" value="{{old('cedula')}}" onchange="busca_cli()"  onkeypress="return soloNumeros(event);" >
                  <span class="help-block">{{ $errors->first('cedula') }}</span>
               
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nombres y Apellidos</label>
                  <input type="text" id="nombre" name="name" class="form-control"  value="{{old('name')}}" >
                  <span class="help-block">{{ $errors->first('name') }}</span>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group feedback {{ $errors->has('telefono') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Teléfono Contacto</label>
                  <input type="text" id="telefono" name="telefono" class="form-control" value="{{old('telefono')}}"  data-inputmask='"mask": "(9999) 999-9999"' data-mask >
                  <span class="help-block">{{ $errors->first('telefono') }}</span>
                </div>
              </div>
              <div class="col-md-8">
                <div class="form-group">
                  <label for="exampleInputPassword1">Correo Electrónico</label>
                  <input type="text" id="email" name="email" class="form-control"  value="{{old('email')}}"  >
                </div>
              </div>

              <div class="col-md-12">
                <div class="form-group {{ $errors->has('direccion') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Domicilio Fiscal</label>
                 <textarea id="direccion" name='direccion'  class="form-control">{{old('direccion')}}</textarea>
                  <span class="help-block">{{ $errors->first('direccion') }}</span>
                </div>
              </div>
              <hr>
               <center>
              <strong>Pago Del Pedido</strong>
            </center>
            <div class="col-md-12">
               


              </div> 
            <div class="col-md-6">
                <div class="form-group {{ $errors->has('pago') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Tipo Pago</label>
                 <select class="form-control" id="pago" name="pago">
                   <option value=''>-seleccione-</option>
                   @foreach($tipo as $tipos)
                    <option value='{{$tipos->id}}'>{{$tipos->name}}</option>
              
                   @endforeach
                 </select>
                  <span class="help-block">{{ $errors->first('pago') }}</span>
                </div>
              </div> 
                 <script>               
                document.getElementById("pago").value="{{old('pago')}}";                
              </script>  

              <div class="col-md-6">
                <div class="form-group {{ $errors->has('banco') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Banco Receptor</label>
                 <select class="form-control" id="banco" name="banco" >
                   <option value=''>-seleccione-</option>
                    @foreach($banco as $bancos)
                    <option value='{{$bancos->id}}'>{{$bancos->name}}</option>

                   @endforeach
                 </select>
                  <span class="help-block">{{ $errors->first('banco') }}</span>
                </div>
              </div>  
              <script>               
                document.getElementById("banco").value="{{old('banco')}}";                
              </script>       

         

           <div class="col-md-12">
                <div class="form-group {{ $errors->has('nro_pago') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Nro Transaccion</label>
                  <input type="text" name="nro_pago" class="form-control" value="{{old('nro_pago')}}" onkeypress="return soloNumeros(event);" >
                   <span class="help-block">{{ $errors->first('nro_pago') }}</span>
                 
                </div>
              </div>     



           </div>

          <div class="col-md-7">

            <div class="col-md-12">
              <div class="col-md-4">
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="fa fa-bookmark-o"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Subtotal</span>
              <span class="info-box-number" id="ST">0.00</span>
             

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>

         
        <div class="col-md-4">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-minus"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">I.V.A {{$iva->porc}}%</span>
              <span class="info-box-number" id="TI">0.00</span>


              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
               
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->


          <!-- /.col -->
        <div class="col-md-4">
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Total Compra</span>
              <span class="info-box-number" id="TC">0.00</span>
               <input type="hidden" id="ST1" name="monto_compra">

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
                 
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

              
            </div>

            <div class="col-md-12">          

            <center><a class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-folder"></i> Cargar Producto</a></center>
            <hr>

            <table class="table table-striped">
              <thead>
                <tr class="success">                 
                  <th>Items</th>
                  <th>Precio</th>
                  <th width="20px">Cantidad</th>
                  <th>Subtotal</th>
                  <th>Iva {{$iva->porc}}%</th>
                  <th>Total</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="Cped">
                
              </tbody>
            
              <tfoot>
                <tr>
                </tr>
                <tr>
                  <th colspan="3">
                     <div class="form-group {{ $errors->has('agente_envio') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Agente de Envio</label>
                   <select class="form-control {{ $errors->has('agente_envio') ? ' is-invalid' : '' }}" name="agente_envio" id="agente"
                    onchange="traeCosto()">
                    <option value="">Seleccione Agente De Envios</option>
                    @foreach($envio as $envios)
                    <option value="{{$envios->id}}">{{$envios->name}}</option>
                    @endforeach          
                   </select>
                   <span class="help-block">{{ $errors->first('agente_envio') }}</span>
                </div>
                 <script>               
                document.getElementById("agente").value="{{old('agente_envio')}}";                
              </script>   
               
                  <th id="costoE">0.00</th>
                  <th id="ivaE">0.00</th>
                  <th id="totE">0.00</th>
                  <th></th>
                </tr>
              </tfoot>
              
            </table>

          </div> 
          </div>


      </div>
      <!-- /.box -->
   
      <hr>
      <center>
        <button type="submit" class="btn btn-primary">Procesar Pedido</button>
        <a href="factura" class="btn btn-default" >Limpiar</a>
      </center>
      <hr>
 </form>
    </section>


<!--cierro div-->   

  
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Busca Productos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      
                <div class="form-group">
                  <label for="exampleInputPassword1">Nombre del Producto</label>
                 <input type="text" class="form-control" id="view"  onkeypress="busca()">
           </div>

          <hr>
          <hr>
        <table  class="table table-hover table-striped">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Precio</th>
              <th>Cantidad Disponible</th>
              <th></th>
            </tr>
          </thead>

          <tbody id="TbMuestra">
            
          </tbody>
          
        </table>
        <input type="hidden" name="_token"value="{{ csrf_token()}}">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

<script src="{{asset('js/fucionfactura.js')}}"></script>
<script >
  function busca(){
    name=$('#view').val();
    $('#TbMuestra').empty();
    $.ajax({    type: 'GET',//metoodo 
                url: '/productos/buscar/'+name,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) {            
    
        $.each(data, function(index, prod){
              //alert(prod.name);
      prise=prod.price;
       html='<tr><td>'+prod.name+'</center><td><center>'+prise.toFixed(2)+'</center></td><td><center>'+prod.qty+'</center></td><td><button class="btn btn-success" title="Seleccionar" onclick="carga('+prod.id+','+prod.qty+','+prise+')"><i class="fa fa-check-circle"></i></button><input type="hidden" value="'+prod.name+'" id="name'+prod.id+'"></td></tr>';
       $('#TbMuestra').append(html);
             
        });   

                }
            });
  }


  function carga(id,qty,precio){ 
  prod=$('#name'+id).val();
  iva="{{$iva->porc}}"/100;


  preProd=parseFloat(precio)-(parseFloat(precio)*parseFloat(iva));
  iva=parseFloat(precio)*parseFloat(iva);
  SubtProd=parseFloat(preProd);
  TotProd=parseFloat(SubtProd)+parseFloat(iva);

   html='<tr id="Items'+id+'"><td>'+prod+'<input type="hidden" id="nameP'+id+'" value="'+prod+'"></td><td>'+ preProd.toFixed(2) +'</td><td><input type="text" class="form-control" id="qty'+id+'" name="Prod[qty][]" value="1"  onChange="up('+id+','+precio+','+qty+')"></td><td>'+ SubtProd.toFixed(2) +'</td><td>'+ iva.toFixed(2) +'</td><td>'+TotProd.toFixed(2)+'</td><td><button class="btn btn-danger" onclick="quitar('+id+')" ><i class="fa fa-trash"></i></button><input type="hidden" name="Prod[id][]" value="'+id+'"><input type="hidden" name="sub[]" id="sub'+id+'" value="'+ SubtProd.toFixed(2) +'"><input type="hidden" name="iva[]" id="iva'+id+'" value="'+ iva.toFixed(2) +'"><input type="hidden" id="tot'+id+'" name="totC[]" value="'+TotProd.toFixed(2)+'"> </td></tr>';
       $('#Cped').append(html);
       sumatoria();
  }

  function quitar(id){
if(confirm('Realmente desea Quitar El Producto?')){
    $('#Items'+id).remove();
    sumatoria();
}
  }

  function sumatoria(){
    sub=0.00;
    iva=0.00;
    tot=0.00;

   $('input[name^="sub"]').each(function() {sub= parseFloat(sub) + parseFloat(this.value);});
   $('input[name^="iva"]').each(function() {iva= parseFloat(iva) + parseFloat(this.value)});
   $('input[name^="totC"]').each(function() {tot= parseFloat(tot) + parseFloat(this.value)});
  
  $('#ST').html(sub.toFixed(2));
  $('#TI').html(iva.toFixed(2));
  $('#TC').html(tot.toFixed(2));
  $('#ST1').val(tot.toFixed(2));

  }

  function up(id,precio,qt){


      iva="{{$iva->porc}}"/100;
      qty=$('#qty'+id).val();


  preProd=parseFloat(precio)-(parseFloat(precio)*parseFloat(iva));
  iva=(parseFloat(precio)*parseFloat(qty))*parseFloat(iva);
  SubtProd=parseFloat(preProd)*parseFloat(qty);
  TotProd=parseFloat(SubtProd)+parseFloat(iva);
  prod=$('#nameP'+id).val();
      //
     html='<td>'+prod+'<input type="hidden" id="nameP'+id+'" value="'+prod+'"></td><td>'+ preProd.toFixed(2) +'</td><td><input type="text" class="form-control" id="qty'+id+'" name="Prod[qty][]" value="'+qty+'"  onChange="up('+id+','+precio+')"></td><td>'+ SubtProd.toFixed(2) +'</td><td>'+ iva.toFixed(2) +'</td><td>'+TotProd.toFixed(2)+'</td><td><button class="btn btn-danger" onclick="quitar('+id+')" ><i class="fa fa-trash"></i></button><input type="hidden" name="Prod[id][]" value="'+id+'"><input type="hidden" name="sub[]" id="sub'+id+'" value="'+ SubtProd.toFixed(2) +'"><input type="hidden" name="iva[]" id="iva'+id+'" value="'+ iva.toFixed(2) +'"><input type="hidden" id="tot'+id+'" name="totC[]" value="'+TotProd.toFixed(2)+'"> </td>';
     if(parseInt(qty)>parseInt(qt)){
      alert('Disculpe No disponemos de esa Cantidad en nuestro almacen Verifique!');
      $('#Items'+id).remove();
      sumatoria();

     }else{
       $('#Items'+id).empty();
       $('#Items'+id).append(html);
       sumatoria();
     }

  }


  function busca_cli(){
    cedula=$('#cedula').val();
    nac=$('#nac').val();
    if(nac!=""){
    if(cedula!=""){
     $.ajax({    type: 'GET',//metoodo 
                url: '/busca/cliente/'+cedula+'/'+nac,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) { 
                  $.each(data, function(index, cli){
                  $('#nombre').val(cli.name);  
                  $('#telefono').val(cli.telefono);  
                  $('#email').val(cli.email); 
                  $('#direccion').val(cli.direccion);          

                     });
                                  }
            });
    }
  }else{
    alert('Por Favor Seleccione La Nacionalidad o Tipo de Cliente!')
  }
  

  }


 function traeCosto(){
 
    agente=$('#agente').val();

iva="{{$iva->porc}}"/100;

 $.ajax({
                type: 'GET',//metoodo 
                url: '/envio/'+agente,//id del delete
                data: {
                    '_token': $('input[name=_token]').val(),
                },
                success: function(data) { 
                sbc=parseFloat(data.price)-(parseFloat(data.price)*parseFloat(iva));
                ivc=parseFloat(data.price)*parseFloat(iva);
                tots=parseFloat(sbc)+parseFloat(ivc);
     
              $('#costoE').html(sbc.toFixed(2)+ "<input type='hidden' name='sub[]' value='"+sbc.toFixed(2)+"'>");
              $('#ivaE').html(ivc.toFixed(2)+ "<input type='hidden' name='iva[]' value='"+ivc.toFixed(2)+"'>"); 
              $('#totE').html(data.price.toFixed(2)+ "<input type='hidden' name='totC[]' value='"+data.price.toFixed(2)+"'>");                

                sumatoria();
          
             
                }
            });
}

</script>
@endsection


