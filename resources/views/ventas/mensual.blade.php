@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        VENTAS EJECUTADAS
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> ventas</a></li>
        <li class="active">Ventas Realizadas</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif


                <hr>
                <form action="/compraMes" method="POST">
      
        <input type="hidden" name="_token"value="{{ csrf_token()}}">
           

               <div class="col-md-6">
                <div class="form-group {{ $errors->has('desde') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Desde:</label>
                  <input type="text" class="form-control datepicker" id="desde" name="desde" value="@if($f1){{$f1}}@endif" readonly="">
                   <span class="help-block">{{ $errors->first('desde') }}</span>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Hasta:</label>
                  <input type="text" class="form-control datepicker" id="hasta" name="hasta" value="@if($f2){{$f2}}@endif" readonly="">
                   <span class="help-block">{{ $errors->first('hasta') }}</span>
                </div>
              </div>

              <br>

  <center> 
    <button class="btn btn-primary"><i class="fa fa-eye"></i> Consultar Ventas</button>
    <button class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button></center>
    </form>
              <hr>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre Cliente</th>
                  <th>Telefóno</th>
                  <th>Email</th>
                  <th>Fecha de Venta</th>
                  <th>Plataforma</th>
                  <th>Monto</th>
                
                </tr>
                </thead>
                <tbody>
                  @if($ventas)
                    @foreach($ventas as $venta)
                <tr>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{$venta->cliente->telefono}}</td>
                  <td>{{$venta->cliente->email}}</td>
                  <td><span class='badge bg-green'>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</span></td>
                   <td>{{$venta->plataforma}}</td>
                  
                  <td>
                   {{number_format($venta->monto_compra,2,',','.')}}
                  </td>
                </tr>
                @endforeach
                @endif
                
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
    
    <center>
      @if($ventas)
      <button class="btn btn-primary" onclick="print()"><i class="fa fa-print"></i> Imprimir Records De Ventas</button>
      @else
      <button class="btn btn-primary" disabled=""><i class="fa fa-print"></i> Imprimir Records De Ventas</button>

      @endif



    </center>

      </div>
      <!-- /.box -->
        
    </section>
<script type="text/javascript">
  function print(){
    desde=$('#desde').val();
    hasta=$('#hasta').val();
    if(desde!="" && hasta!=""){

    if(confirm('Realmente desea Generar El Reporte')){
    //location.href('/pdfTopMes/');
    window.location.href = "/pdfcompraMes?desde="+desde+"&hasta="+hasta+"";
    }
  }
   
  }
</script>


 
@endsection







