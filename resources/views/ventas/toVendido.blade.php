@extends('layouts.admin')

@section('content')
 <meta name="_token" content="{{ csrf_token() }}"/>
<section class="content-header">
      <h1>
        TOP DE PRODUCTOS MAS VENDIDOS
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="home"><i class="fa fa-home"></i> Inicio</a></li>
        <li><a href="#"><i class="fa fa-cogs"></i> ventas</a></li>
        <li class="active">Top Más Vendido</li>
      </ol>
    </section>

     <section class="content">

      
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>
         

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
           @if (session('message'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('message') }}</p>
              </div>
                        
                @endif


                <hr>
                <form action="/TopMes" method="GET">
      
        <input type="hidden" name="_token"value="{{ csrf_token()}}">
           

               <div class="col-md-6">
                <div class="form-group {{ $errors->has('desde') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Desde:</label>
                  <input type="text" class="form-control datepicker" id="desde" name="desde" value="{{ $f1 }}" readonly="">
                   <span class="help-block">{{ $errors->first('desde') }}</span>
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group {{ $errors->has('hasta') ? 'has-error' : '' }}">
                  <label for="exampleInputPassword1">Hasta:</label>
                  <input type="text" class="form-control datepicker" id="hasta" name="hasta" value="{{ $f2 }}" readonly="">
                   <span class="help-block">{{ $errors->first('hasta') }}</span>
                </div>
              </div>

              <br>

  <center> 
    <button class="btn btn-primary"><i class="fa fa-eye"></i> Consultar Top Mas Vendido</button>
    <button class="btn btn-danger"><i class="fa fa-trash"></i> Limpiar</button></center>
    </form>
              <hr>
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>#</th>
                  <th>Producto</th>
                  <th>Cantidad Disponible</th>
                  <th>Cantidad Vendida</th>
                
                
                </tr>
                </thead>
                <tbody>
                  @if($top)
                   @foreach($top as $tops)

                  <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$tops->name}}</td>
                    <td>{{$tops->qty}}</td>
                    <td>{{$tops->total_qty}}</td>
                  </tr>


                  @endforeach
                  @endif
              
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
    
    <center>
      @if($top)
      <button class="btn btn-primary" onclick="print()"><i class="fa fa-print"></i> Imprimir Tops De Ventas</button>
      @else
      <button class="btn btn-primary" disabled=""><i class="fa fa-print"></i> Imprimir Tops De Ventas</button>
      @endif

    </center>

      </div>
      <!-- /.box -->
        
    </section>


<script type="text/javascript">
  function print(){
    desde=$('#desde').val();
    hasta=$('#hasta').val();
    if(desde!="" && hasta!=""){

    if(confirm('Realmente desea Generar El Reporte')){
    //location.href('/pdfTopMes/');
    window.location.href = "/pdfTopMes?desde="+desde+"&hasta="+hasta+"";
    }
  }
   
  }
</script>

 
@endsection







