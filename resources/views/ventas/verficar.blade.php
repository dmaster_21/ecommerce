@extends('layouts.admin')

@section('content')


    <!-- Main content -->
    <section class="content">
       @if (session('mensaje'))
                   <div class="callout callout-{{ session('class') }}">
                <h4><i class="icon fa fa-check"></i> Notificación !</h4>

                <p>{{ session('mensaje') }}</p>
              </div>
                        
                @endif
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>Verificar Ventas</strong></h3>
        </div>
        <div class="box-body">
         <!--aqui va todo -->


              <div class="col-md-6">

      <form action="{{action('ComprascartController@verificar')}}" method="POST">

              <div class="input-group">

        <input type="hidden" name="_method" value="GET">
        <input type="hidden" name="_token" value="{{ csrf_token()}}">

     <input type="text" class="form-control text-uppercase" name="nombre" placeholder="Nombre del cliente"  >
 

                    <span class="input-group-btn">
                      <button type="submit" class="btn btn-info btn-flat">Buscar</button>
                    </span>
              </div>

            </div>
           
          </form>
        
            
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><strong></strong></h3>


         
        <div class="box-body">
         <!--aqui va todo -->


           <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nombre Cliente</th>
                  <th>Telefóno</th>
                  <th>Email</th>
                  <th>Fecha de Venta</th>
                  <th>Fecha de Anulación</th>                  
                  <th>Monto</th>
                  <th>Estatus</th>
                  <th>Acción</th>
                </tr>
                </thead>
                <tbody>
                  @if($user)
                @foreach($user as $users)
                   @foreach($users->clientes as $venta)
                <tr>
                  <td>{{$venta->cliente->name}}</td>
                  <td>{{$venta->cliente->telefono}}</td>
                  <td>{{$venta->cliente->email}}</td>
                  <td><span class='badge bg-green'>{{ date("d/m/Y", strtotime($venta->fecha_compra))}}</span></td>
                  <td>
                    @if($venta->status==1)
                    <span class='badge bg-red'>
                    {{ date("d/m/Y",strtotime ( '+'.$day->nroday.' day' , strtotime($venta->fecha_compra)))}}
                  </span>
                  @endif
                </td>

                  <td><b>{{number_format($venta->monto_compra + $venta->monto_envio ,2,',','.')}}</b></td>
                    <td>
                    
                      @if($venta->status==1)
                      <span class='badge bg-info'>Pendiente</span>
                      @endif

                       @if($venta->status==2)
                      <span class='badge bg-warning'>Pago Procesado</span>
                      @endif

                       @if($venta->status==3)
                      <span class='badge bg-success'>Pago Confirmado</span>
                      @endif

                      @if($venta->status==9)
                      <span class='badge bg-warning'>Compra Anulada</span>
                      @endif


                    </td>
                  <td>
                    <center>
                      @if($venta->status==2)
                       <button class="btn btn-success " title="Autorizar Compra"  data-toggle="modal" data-target="#data-confirm{{$venta->id}}"><i class="fa fa-check-circle"></i></button>
                      @else
                      <button class="btn btn-success " title="Autorizar Compra" disabled=""><i class="fa fa-check-circle" ></i></button>
                      @endif
                     <button class="btn btn-peimary " title="ver detalle" data-toggle="modal" data-target="#data-vista{{$venta->id}}"><i class="fa fa-search"></i></button>

                     <a href="/voucher/{{$venta->id}}"><button class="btn btn-default " title="Imprimir Comprobante"><i class="fa fa-print"></i></button></a>
                     @if($venta->status==3 || $venta->status==9)
                   
                     
                     <button class="btn btn-danger " title="Anular Venta"  data-toggle="modal" data-target="#data-delete{{$venta->id}}" disabled=""><i class="fa fa-trash-o"></i></button> @else

                        <button class="btn btn-danger " title="Anular Venta"  data-toggle="modal" data-target="#data-delete{{$venta->id}}"><i class="fa fa-trash-o"></i></button>
                     @endif
                   </center>
                  </td>
                </tr>
                @endforeach
                
               
                   
                @endforeach
                @endif
                </tbody>
                <tfoot>
                <tr>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                  <th></th>
                </tr>
                </tfoot>
              </table>
    
   

        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
 @include('modals.verificar')


      
@endsection


