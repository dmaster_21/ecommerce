<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

	$Category=App\Category::where('status', 1)->get();
    $prod=App\Product::where('qty','>','0')->where('status', 1)->get();
     return view('welcome',['category'=>$Category,'producto' => $prod]);
});

Route::get('catalogo/', function () {

    $Category=App\Category::where('status', 1)->get();
     $prod=App\Product::where('qty','>','0')->where('status', 1)->get();
     return view('catalogo',['category'=>$Category,'producto' => $prod]);
});

Route::get('contacto/', function () {

     return view('contacto');
});


//inyeccion de dependecnias
Route::bind('product',function($id){
return App\Product::where('id',$id)->first();
});

Route::get('detalle/{id}', 'ProductsController@detalle')->name('productos.detalle');
Route::get('productos/deleteIm/{id}', 'ProductsController@deleteIm')->name('deleteIm');
Route::get('productos/buscar/{name}', 'ProductsController@buscar')->name('buscarIm');
Auth::routes();
Route::get('/home', 'HomeController@index')->middleware('auth')->name('home');

//Route::get('/categorias','CategorysController@index')->name('categorias');
//Route::apiResource('/categorias','CategorysController@index');
Route::Resource('categorias','CategorysController');
Route::Resource('productos','ProductsController');
Route::get('/compras/cuenta','ComprascartController@cuenta')->middleware('auth')->name('compras.cuenta');
Route::get('/compras/cliente','ComprascartController@cliente')->middleware('auth')->name('compras.cliente');
Route::Resource('compras','ComprascartController');
Route::Resource('Cdetalle','ComprasdetalleController');
Route::get('verificarCliente/','ComprascartController@verificar');

Route::post('/compraMes','ComprascartController@compraMes');
Route::get('/TopMes','ComprascartController@TopMes');
Route::get('/graficos','ComprascartController@graficos');



//
Route::Resource('Sugerencias','SugerenciasController');
Route::get('EnvioSug','SugerenciasController@envio')->middleware('auth')->name('sugerencia.cliente');
/*Funcionalidad del carrito*/
Route::get('/cart/show', 'CartController@show')->name('cart.show');//ver Carrito d compras
Route::get('/cart/add/{product}', 'CartController@add')->name('cart.add');//añadir items
Route::get('/cart/delete/{product}', 'CartController@delete')->name('cart.delete');//quitar items
Route::get('/cart/trash', 'CartController@trash')->name('cart.trash');//vaciar Carritos
Route::get('/cart/update/{product}/{qty}', 'CartController@update')->name('cart.update');//actualizar cantidad
Route::get('/cart/detail', 'CartController@Detail')->middleware('auth')->name('cart.detail');//detalle del carrito
Route::get('/cart/proccess','CartController@proccess')->name('cart.proccess');
Route::get('/productos/destacar/{id}/{vl}', 'ProductsController@destacar')->name('productos.destacar');//ver Carrito d compras
/////////////////////////////////////////////////////////////
Route::get('pdf','ProductsController@pdf')->name('pdf.producto');
Route::get('minpdf','ProductsController@minpdf');
Route::get('pdfVentaC','ComprascartController@pdfVentaC');
Route::get('voucher/{id}','ComprascartController@voucher');


////////////////////////////////////////////////////////////////

Route::Resource('banco','BancosController');
Route::Resource('tipo-pago','TipopagoController');
Route::get('/banco/status/{id}/{st}','BancosController@status')->name('banco.status');
Route::Resource('envio','EnviosController');
Route::get('/envios/status/{id}/{st}','EnviosController@status')->name('envio.status');


Route::get('/salir', function () {
    Auth::logout();
    return redirect()->intended('/');
})->middleware('auth');

//vista min max
Route::get('/stockmin', function () { $prod=App\Product::where('qty','<=','10')->get();
     return view('productos.stockmin',compact('prod'));
})->middleware('auth');

Route::get('/top', function () {
 $prod=App\Product::where('qty','<=','50')->get();
/*$prod=App\Compras_cart_detalle::select('products_id', DB::raw('SUM(qty) as total_sales'))
                ->groupBy('products_id')
                ->havingRaw('SUM(qty) > 10')
                ->get();*/
                return $prod;


     return view('productos.stockmax',compact('prod'));
})->middleware('auth');


Route::get('/verificar', function () {
   $user="";
   $iva="";

     return view('ventas.verficar',compact('user','iva'));
})->middleware('auth');
Route::get('confirmar/{id}','ComprascartController@confirmar');
Route::get('verificarCliente/','ComprascartController@verificar');
Route::post('ingreso/','ComprascartController@ingreso');
Route::get('despacho','ComprascartController@verdespacho');
Route::post('Procesadespacho','ComprascartController@procesardespacho');
Route::get('buscadespacho','ComprascartController@buscadespacho');
Route::get('procesabusqueda','ComprascartController@procesabusqueda');


Route::Resource('usuario','UsersController');
Route::get('reset/{id}','UsersController@reset');
Route::get('logs','UsersController@logs');
Route::get('perfil', function () {

    $log=App\Log::where('user_id','=',auth()->id())->orderBy('created_at', 'desc')->take(15)->get();
return view('user.perfil',compact('log'));
 
})->middleware('auth');

Route::get('busca/cliente/{cedula}/{nac}','UsersController@busc_cliente');
//factura fiscal 
Route::get('/factura', function () {
$iva=App\Iva::find(1);
  $banco=App\Banco::where('status',1)->get();
        $tipo=App\Tipo_pago::where('status',1)->get();
         $envio=App\Envio::where('status',1)->get();
     return view('ventas.factura',compact('iva','banco','tipo','envio'));
})->middleware('auth');


//log
Route::get('/log', function () {
$log="";
$user=App\User::where('admin','<>','1')->get();
 
     return view('log.index',compact('log','user'));
})->middleware('auth');


Route::get('/Pventas', function () {
$ventas=""; 
$f1="";
$f2="";
     return view('ventas.mensual',compact('ventas','f1','f2'));
})->middleware('auth');

Route::get('/pdfcompraMes','ComprascartController@pdfcompraMes')->middleware('auth');
Route::get('/pdfTopMes','ComprascartController@pdfTopMes')->middleware('auth');
Route::get('/listadespacho','ComprascartController@listadespacho')->middleware('auth');
Route::get('/historicodespacho','ComprascartController@historicodespacho')->middleware('auth');



Route::get('/Topventas', function () {
$top=""; 
$f1="";
$f2="";
     return view('ventas.toVendido',compact('top','f1','f2'));
})->middleware('auth');

Route::get('/inbox', function () {
$Sug=App\Sugerencia::all();
    return view('mailbox.index',compact('Sug'));
})->middleware('auth');

Route::Resource('day','dayController');